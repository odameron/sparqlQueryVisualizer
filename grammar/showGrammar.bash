#! /usr/bin/env bash

antlr4JarPath="/usr/local/lib/antlr-4.10.1-complete.jar"

export CLASSPATH=".:${antlr4JarPath}:$CLASSPATH"
antlr4="java -jar ${antlr4JarPath}"
grun='java org.antlr.v4.gui.TestRig'

$antlr4 Sparql.g4
javac Sparql*.java
#$grun Sparql statement -gui ../testQuery.rq
#$grun Sparql statement -gui ../testQuery-alternateProperty.rq
#$grun Sparql statement -gui ../testQuery-sequenceProperty.rq
#$grun Sparql statement -gui ../testQuery-sameSubject.rq
#$grun Sparql statement -gui ../testQuery-sameSubjectSameProperty.rq
#$grun Sparql statement -gui ../testQuery-sameSubjectPropertyAlternativePropertySequence.rq
#$grun Sparql statement -gui ../testQuery-datatypeValue.rq
#$grun Sparql statement -gui ../biopax-invalid-complexes.rq
#$grun Sparql statement -gui ../query-redundancy-heterodimeres-getRedundantCurationEvents.rq
$grun Sparql statement -gui ../testQuery-filterNotExists.rq
