// Generated from Sparql.g4 by ANTLR 4.10.1
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class SparqlParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.10.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, T__28=29, T__29=30, T__30=31, 
		T__31=32, T__32=33, T__33=34, T__34=35, T__35=36, T__36=37, T__37=38, 
		T__38=39, T__39=40, T__40=41, T__41=42, T__42=43, T__43=44, T__44=45, 
		T__45=46, T__46=47, T__47=48, T__48=49, T__49=50, T__50=51, T__51=52, 
		T__52=53, T__53=54, T__54=55, T__55=56, T__56=57, T__57=58, T__58=59, 
		T__59=60, T__60=61, T__61=62, T__62=63, T__63=64, T__64=65, T__65=66, 
		T__66=67, T__67=68, T__68=69, T__69=70, T__70=71, T__71=72, T__72=73, 
		T__73=74, T__74=75, T__75=76, T__76=77, T__77=78, T__78=79, T__79=80, 
		T__80=81, T__81=82, T__82=83, T__83=84, T__84=85, T__85=86, T__86=87, 
		T__87=88, T__88=89, T__89=90, T__90=91, T__91=92, T__92=93, T__93=94, 
		T__94=95, T__95=96, T__96=97, T__97=98, T__98=99, T__99=100, T__100=101, 
		T__101=102, T__102=103, T__103=104, T__104=105, T__105=106, T__106=107, 
		T__107=108, T__108=109, T__109=110, T__110=111, T__111=112, T__112=113, 
		T__113=114, T__114=115, T__115=116, T__116=117, T__117=118, T__118=119, 
		T__119=120, T__120=121, T__121=122, T__122=123, T__123=124, T__124=125, 
		T__125=126, T__126=127, T__127=128, T__128=129, T__129=130, T__130=131, 
		T__131=132, T__132=133, T__133=134, T__134=135, T__135=136, T__136=137, 
		T__137=138, T__138=139, T__139=140, T__140=141, T__141=142, T__142=143, 
		IRIREF=144, PNAME_NS=145, PNAME_LN=146, BLANK_NODE_LABEL=147, VAR1=148, 
		VAR2=149, LANGTAG=150, INTEGER=151, DECIMAL=152, DOUBLE=153, INTEGER_POSITIVE=154, 
		DECIMAL_POSITIVE=155, DOUBLE_POSITIVE=156, INTEGER_NEGATIVE=157, DECIMAL_NEGATIVE=158, 
		DOUBLE_NEGATIVE=159, EXPONENT=160, STRING_LITERAL1=161, STRING_LITERAL2=162, 
		STRING_LITERAL_LONG1=163, STRING_LITERAL_LONG2=164, ECHAR=165, NIL=166, 
		WS=167, COMMENT=168, ANON=169, PN_CHARS_BASE=170, PN_CHARS_U=171, VARNAME=172, 
		PN_CHARS=173, PN_PREFIX=174, PN_LOCAL=175, PLX=176, PERCENT=177, HEX=178, 
		PN_LOCAL_ESC=179;
	public static final int
		RULE_statement = 0, RULE_query = 1, RULE_prologue = 2, RULE_baseDecl = 3, 
		RULE_prefixDecl = 4, RULE_selectQuery = 5, RULE_subSelect = 6, RULE_selectClause = 7, 
		RULE_constructQuery = 8, RULE_describeQuery = 9, RULE_askQuery = 10, RULE_datasetClause = 11, 
		RULE_defaultGraphClause = 12, RULE_namedGraphClause = 13, RULE_sourceSelector = 14, 
		RULE_whereClause = 15, RULE_solutionModifier = 16, RULE_groupClause = 17, 
		RULE_groupCondition = 18, RULE_havingClause = 19, RULE_havingCondition = 20, 
		RULE_orderClause = 21, RULE_orderCondition = 22, RULE_limitOffsetClauses = 23, 
		RULE_limitClause = 24, RULE_offsetClause = 25, RULE_valuesClause = 26, 
		RULE_update = 27, RULE_update1 = 28, RULE_load = 29, RULE_clear = 30, 
		RULE_drop = 31, RULE_create = 32, RULE_add = 33, RULE_move = 34, RULE_copy = 35, 
		RULE_insertData = 36, RULE_deleteData = 37, RULE_deleteWhere = 38, RULE_modify = 39, 
		RULE_deleteClause = 40, RULE_insertClause = 41, RULE_usingClause = 42, 
		RULE_graphOrDefault = 43, RULE_graphRef = 44, RULE_graphRefAll = 45, RULE_quadPattern = 46, 
		RULE_quadData = 47, RULE_quads = 48, RULE_quadsNotTriples = 49, RULE_triplesTemplate = 50, 
		RULE_groupGraphPattern = 51, RULE_groupGraphPatternSub = 52, RULE_triplesBlock = 53, 
		RULE_graphPatternNotTriples = 54, RULE_optionalGraphPattern = 55, RULE_graphGraphPattern = 56, 
		RULE_serviceGraphPattern = 57, RULE_bind = 58, RULE_inlineData = 59, RULE_dataBlock = 60, 
		RULE_inlineDataOneVar = 61, RULE_inlineDataFull = 62, RULE_dataBlockValue = 63, 
		RULE_minusGraphPattern = 64, RULE_groupOrUnionGraphPattern = 65, RULE_filterClause = 66, 
		RULE_constraint = 67, RULE_functionCall = 68, RULE_argList = 69, RULE_expressionList = 70, 
		RULE_constructTemplate = 71, RULE_constructTriples = 72, RULE_triplesSameSubject = 73, 
		RULE_propertyList = 74, RULE_propertyListNotEmpty = 75, RULE_verb = 76, 
		RULE_objectList = 77, RULE_objectClause = 78, RULE_triplesSameSubjectPath = 79, 
		RULE_propertyListPath = 80, RULE_propertyListPathNotEmpty = 81, RULE_verbPath = 82, 
		RULE_verbSimple = 83, RULE_objectListPath = 84, RULE_objectPath = 85, 
		RULE_path = 86, RULE_pathAlternative = 87, RULE_pathSequence = 88, RULE_pathElt = 89, 
		RULE_pathEltOrInverse = 90, RULE_pathMod = 91, RULE_pathPrimary = 92, 
		RULE_pathNegatedPropertySet = 93, RULE_pathOneInPropertySet = 94, RULE_triplesNode = 95, 
		RULE_blankNodePropertyList = 96, RULE_triplesNodePath = 97, RULE_blankNodePropertyListPath = 98, 
		RULE_collection = 99, RULE_collectionPath = 100, RULE_graphNode = 101, 
		RULE_graphNodePath = 102, RULE_varOrTerm = 103, RULE_varOrIri = 104, RULE_var = 105, 
		RULE_graphTerm = 106, RULE_expression = 107, RULE_conditionalOrExpression = 108, 
		RULE_conditionalAndExpression = 109, RULE_valueLogical = 110, RULE_relationalExpression = 111, 
		RULE_numericExpression = 112, RULE_additiveExpression = 113, RULE_multiplicativeExpression = 114, 
		RULE_unaryExpression = 115, RULE_primaryExpression = 116, RULE_brackettedExpression = 117, 
		RULE_builtInCall = 118, RULE_regexExpression = 119, RULE_substringExpression = 120, 
		RULE_strReplaceExpression = 121, RULE_existsFunc = 122, RULE_notExistsFunc = 123, 
		RULE_aggregate = 124, RULE_iriOrFunction = 125, RULE_rdfLiteral = 126, 
		RULE_numericLiteral = 127, RULE_numericLiteralUnsigned = 128, RULE_numericLiteralPositive = 129, 
		RULE_numericLiteralNegative = 130, RULE_booleanLiteral = 131, RULE_string = 132, 
		RULE_iri = 133, RULE_prefixedName = 134, RULE_blankNode = 135;
	private static String[] makeRuleNames() {
		return new String[] {
			"statement", "query", "prologue", "baseDecl", "prefixDecl", "selectQuery", 
			"subSelect", "selectClause", "constructQuery", "describeQuery", "askQuery", 
			"datasetClause", "defaultGraphClause", "namedGraphClause", "sourceSelector", 
			"whereClause", "solutionModifier", "groupClause", "groupCondition", "havingClause", 
			"havingCondition", "orderClause", "orderCondition", "limitOffsetClauses", 
			"limitClause", "offsetClause", "valuesClause", "update", "update1", "load", 
			"clear", "drop", "create", "add", "move", "copy", "insertData", "deleteData", 
			"deleteWhere", "modify", "deleteClause", "insertClause", "usingClause", 
			"graphOrDefault", "graphRef", "graphRefAll", "quadPattern", "quadData", 
			"quads", "quadsNotTriples", "triplesTemplate", "groupGraphPattern", "groupGraphPatternSub", 
			"triplesBlock", "graphPatternNotTriples", "optionalGraphPattern", "graphGraphPattern", 
			"serviceGraphPattern", "bind", "inlineData", "dataBlock", "inlineDataOneVar", 
			"inlineDataFull", "dataBlockValue", "minusGraphPattern", "groupOrUnionGraphPattern", 
			"filterClause", "constraint", "functionCall", "argList", "expressionList", 
			"constructTemplate", "constructTriples", "triplesSameSubject", "propertyList", 
			"propertyListNotEmpty", "verb", "objectList", "objectClause", "triplesSameSubjectPath", 
			"propertyListPath", "propertyListPathNotEmpty", "verbPath", "verbSimple", 
			"objectListPath", "objectPath", "path", "pathAlternative", "pathSequence", 
			"pathElt", "pathEltOrInverse", "pathMod", "pathPrimary", "pathNegatedPropertySet", 
			"pathOneInPropertySet", "triplesNode", "blankNodePropertyList", "triplesNodePath", 
			"blankNodePropertyListPath", "collection", "collectionPath", "graphNode", 
			"graphNodePath", "varOrTerm", "varOrIri", "var", "graphTerm", "expression", 
			"conditionalOrExpression", "conditionalAndExpression", "valueLogical", 
			"relationalExpression", "numericExpression", "additiveExpression", "multiplicativeExpression", 
			"unaryExpression", "primaryExpression", "brackettedExpression", "builtInCall", 
			"regexExpression", "substringExpression", "strReplaceExpression", "existsFunc", 
			"notExistsFunc", "aggregate", "iriOrFunction", "rdfLiteral", "numericLiteral", 
			"numericLiteralUnsigned", "numericLiteralPositive", "numericLiteralNegative", 
			"booleanLiteral", "string", "iri", "prefixedName", "blankNode"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'BASE'", "'PREFIX'", "'SELECT'", "'DISTINCT'", "'REDUCED'", "'('", 
			"'AS'", "')'", "'*'", "'CONSTRUCT'", "'WHERE'", "'{'", "'}'", "'DESCRIBE'", 
			"'ASK'", "'FROM'", "'NAMED'", "'GROUP'", "'BY'", "'HAVING'", "'ORDER'", 
			"'ASC'", "'DESC'", "'LIMIT'", "'OFFSET'", "'VALUES'", "';'", "'LOAD'", 
			"'SILENT'", "'INTO'", "'CLEAR'", "'DROP'", "'CREATE'", "'ADD'", "'TO'", 
			"'MOVE'", "'COPY'", "'INSERT DATA'", "'DELETE DATA'", "'DELETE WHERE'", 
			"'WITH'", "'DELETE'", "'INSERT'", "'USING'", "'DEFAULT'", "'GRAPH'", 
			"'ALL'", "'.'", "'OPTIONAL'", "'SERVICE'", "'BIND'", "'UNDEF'", "'MINUS'", 
			"'UNION'", "'FILTER'", "','", "'a'", "'A'", "'|'", "'/'", "'^'", "'?'", 
			"'+'", "'!'", "'['", "']'", "'||'", "'&&'", "'='", "'!='", "'<'", "'>'", 
			"'<='", "'>='", "'IN'", "'NOT'", "'-'", "'STR'", "'LANG'", "'LANGMATCHES'", 
			"'DATATYPE'", "'BOUND'", "'IRI'", "'URI'", "'BNODE'", "'RAND'", "'ABS'", 
			"'CEIL'", "'FLOOR'", "'ROUND'", "'CONCAT'", "'STRLEN'", "'UCASE'", "'LCASE'", 
			"'ENCODE_FOR_URI'", "'CONTAINS'", "'STRSTARTS'", "'STRENDS'", "'STRBEFORE'", 
			"'STRAFTER'", "'YEAR'", "'MONTH'", "'DAY'", "'HOURS'", "'MINUTES'", "'SECONDS'", 
			"'TIMEZONE'", "'TZ'", "'NOW'", "'UUID'", "'STRUUID'", "'MD5'", "'SHA1'", 
			"'SHA256'", "'SHA384'", "'SHA512'", "'COALESCE'", "'IF'", "'STRLANG'", 
			"'STRDT'", "'SAMETERM'", "'ISIRI'", "'ISURI'", "'ISBLANK'", "'ISLITERAL'", 
			"'ISNUMERIC'", "'REGEX'", "'SUBSTR'", "'REPLACE'", "'EXISTS'", "'COUNT'", 
			"'SUM'", "'MIN'", "'MAX'", "'AVG'", "'SAMPLE'", "'GROUP_CONCAT'", "'SEPARATOR'", 
			"'^^'", "'true'", "'false'", "'TRUE'", "'FALSE'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			null, null, null, null, null, null, null, null, null, null, null, null, 
			"IRIREF", "PNAME_NS", "PNAME_LN", "BLANK_NODE_LABEL", "VAR1", "VAR2", 
			"LANGTAG", "INTEGER", "DECIMAL", "DOUBLE", "INTEGER_POSITIVE", "DECIMAL_POSITIVE", 
			"DOUBLE_POSITIVE", "INTEGER_NEGATIVE", "DECIMAL_NEGATIVE", "DOUBLE_NEGATIVE", 
			"EXPONENT", "STRING_LITERAL1", "STRING_LITERAL2", "STRING_LITERAL_LONG1", 
			"STRING_LITERAL_LONG2", "ECHAR", "NIL", "WS", "COMMENT", "ANON", "PN_CHARS_BASE", 
			"PN_CHARS_U", "VARNAME", "PN_CHARS", "PN_PREFIX", "PN_LOCAL", "PLX", 
			"PERCENT", "HEX", "PN_LOCAL_ESC"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Sparql.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public static boolean allowsBlankNodes = true;
	public SparqlParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class StatementContext extends ParserRuleContext {
		public QueryContext query() {
			return getRuleContext(QueryContext.class,0);
		}
		public UpdateContext update() {
			return getRuleContext(UpdateContext.class,0);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitStatement(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_statement);
		try {
			setState(274);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(272);
				query();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(273);
				update();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class QueryContext extends ParserRuleContext {
		public PrologueContext prologue() {
			return getRuleContext(PrologueContext.class,0);
		}
		public ValuesClauseContext valuesClause() {
			return getRuleContext(ValuesClauseContext.class,0);
		}
		public SelectQueryContext selectQuery() {
			return getRuleContext(SelectQueryContext.class,0);
		}
		public ConstructQueryContext constructQuery() {
			return getRuleContext(ConstructQueryContext.class,0);
		}
		public DescribeQueryContext describeQuery() {
			return getRuleContext(DescribeQueryContext.class,0);
		}
		public AskQueryContext askQuery() {
			return getRuleContext(AskQueryContext.class,0);
		}
		public QueryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_query; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterQuery(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitQuery(this);
		}
	}

	public final QueryContext query() throws RecognitionException {
		QueryContext _localctx = new QueryContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_query);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(276);
			prologue();
			setState(281);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__2:
				{
				setState(277);
				selectQuery();
				}
				break;
			case T__9:
				{
				setState(278);
				constructQuery();
				}
				break;
			case T__13:
				{
				setState(279);
				describeQuery();
				}
				break;
			case T__14:
				{
				setState(280);
				askQuery();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(283);
			valuesClause();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrologueContext extends ParserRuleContext {
		public List<BaseDeclContext> baseDecl() {
			return getRuleContexts(BaseDeclContext.class);
		}
		public BaseDeclContext baseDecl(int i) {
			return getRuleContext(BaseDeclContext.class,i);
		}
		public List<PrefixDeclContext> prefixDecl() {
			return getRuleContexts(PrefixDeclContext.class);
		}
		public PrefixDeclContext prefixDecl(int i) {
			return getRuleContext(PrefixDeclContext.class,i);
		}
		public PrologueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prologue; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterPrologue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitPrologue(this);
		}
	}

	public final PrologueContext prologue() throws RecognitionException {
		PrologueContext _localctx = new PrologueContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_prologue);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(289);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__0 || _la==T__1) {
				{
				setState(287);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__0:
					{
					setState(285);
					baseDecl();
					}
					break;
				case T__1:
					{
					setState(286);
					prefixDecl();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(291);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BaseDeclContext extends ParserRuleContext {
		public TerminalNode IRIREF() { return getToken(SparqlParser.IRIREF, 0); }
		public BaseDeclContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_baseDecl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterBaseDecl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitBaseDecl(this);
		}
	}

	public final BaseDeclContext baseDecl() throws RecognitionException {
		BaseDeclContext _localctx = new BaseDeclContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_baseDecl);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(292);
			match(T__0);
			setState(293);
			match(IRIREF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrefixDeclContext extends ParserRuleContext {
		public TerminalNode PNAME_NS() { return getToken(SparqlParser.PNAME_NS, 0); }
		public TerminalNode IRIREF() { return getToken(SparqlParser.IRIREF, 0); }
		public PrefixDeclContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prefixDecl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterPrefixDecl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitPrefixDecl(this);
		}
	}

	public final PrefixDeclContext prefixDecl() throws RecognitionException {
		PrefixDeclContext _localctx = new PrefixDeclContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_prefixDecl);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(295);
			match(T__1);
			setState(296);
			match(PNAME_NS);
			setState(297);
			match(IRIREF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SelectQueryContext extends ParserRuleContext {
		public SelectClauseContext selectClause() {
			return getRuleContext(SelectClauseContext.class,0);
		}
		public WhereClauseContext whereClause() {
			return getRuleContext(WhereClauseContext.class,0);
		}
		public SolutionModifierContext solutionModifier() {
			return getRuleContext(SolutionModifierContext.class,0);
		}
		public List<DatasetClauseContext> datasetClause() {
			return getRuleContexts(DatasetClauseContext.class);
		}
		public DatasetClauseContext datasetClause(int i) {
			return getRuleContext(DatasetClauseContext.class,i);
		}
		public SelectQueryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_selectQuery; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterSelectQuery(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitSelectQuery(this);
		}
	}

	public final SelectQueryContext selectQuery() throws RecognitionException {
		SelectQueryContext _localctx = new SelectQueryContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_selectQuery);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(299);
			selectClause();
			setState(303);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__15) {
				{
				{
				setState(300);
				datasetClause();
				}
				}
				setState(305);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(306);
			whereClause();
			setState(307);
			solutionModifier();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SubSelectContext extends ParserRuleContext {
		public SelectClauseContext selectClause() {
			return getRuleContext(SelectClauseContext.class,0);
		}
		public WhereClauseContext whereClause() {
			return getRuleContext(WhereClauseContext.class,0);
		}
		public SolutionModifierContext solutionModifier() {
			return getRuleContext(SolutionModifierContext.class,0);
		}
		public ValuesClauseContext valuesClause() {
			return getRuleContext(ValuesClauseContext.class,0);
		}
		public SubSelectContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_subSelect; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterSubSelect(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitSubSelect(this);
		}
	}

	public final SubSelectContext subSelect() throws RecognitionException {
		SubSelectContext _localctx = new SubSelectContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_subSelect);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(309);
			selectClause();
			setState(310);
			whereClause();
			setState(311);
			solutionModifier();
			setState(312);
			valuesClause();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SelectClauseContext extends ParserRuleContext {
		public List<VarContext> var() {
			return getRuleContexts(VarContext.class);
		}
		public VarContext var(int i) {
			return getRuleContext(VarContext.class,i);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public SelectClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_selectClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterSelectClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitSelectClause(this);
		}
	}

	public final SelectClauseContext selectClause() throws RecognitionException {
		SelectClauseContext _localctx = new SelectClauseContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_selectClause);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(314);
			match(T__2);
			setState(316);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__3 || _la==T__4) {
				{
				setState(315);
				_la = _input.LA(1);
				if ( !(_la==T__3 || _la==T__4) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				}
			}

			setState(330);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__5:
			case VAR1:
			case VAR2:
				{
				setState(325); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					setState(325);
					_errHandler.sync(this);
					switch (_input.LA(1)) {
					case VAR1:
					case VAR2:
						{
						setState(318);
						var();
						}
						break;
					case T__5:
						{
						{
						setState(319);
						match(T__5);
						setState(320);
						expression();
						setState(321);
						match(T__6);
						setState(322);
						var();
						setState(323);
						match(T__7);
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					}
					setState(327); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==T__5 || _la==VAR1 || _la==VAR2 );
				}
				break;
			case T__8:
				{
				setState(329);
				match(T__8);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstructQueryContext extends ParserRuleContext {
		public ConstructTemplateContext constructTemplate() {
			return getRuleContext(ConstructTemplateContext.class,0);
		}
		public WhereClauseContext whereClause() {
			return getRuleContext(WhereClauseContext.class,0);
		}
		public SolutionModifierContext solutionModifier() {
			return getRuleContext(SolutionModifierContext.class,0);
		}
		public List<DatasetClauseContext> datasetClause() {
			return getRuleContexts(DatasetClauseContext.class);
		}
		public DatasetClauseContext datasetClause(int i) {
			return getRuleContext(DatasetClauseContext.class,i);
		}
		public TriplesTemplateContext triplesTemplate() {
			return getRuleContext(TriplesTemplateContext.class,0);
		}
		public ConstructQueryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constructQuery; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterConstructQuery(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitConstructQuery(this);
		}
	}

	public final ConstructQueryContext constructQuery() throws RecognitionException {
		ConstructQueryContext _localctx = new ConstructQueryContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_constructQuery);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(332);
			match(T__9);
			setState(356);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__11:
				{
				setState(333);
				constructTemplate();
				setState(337);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__15) {
					{
					{
					setState(334);
					datasetClause();
					}
					}
					setState(339);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(340);
				whereClause();
				setState(341);
				solutionModifier();
				}
				break;
			case T__10:
			case T__15:
				{
				setState(346);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__15) {
					{
					{
					setState(343);
					datasetClause();
					}
					}
					setState(348);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(349);
				match(T__10);
				setState(350);
				match(T__11);
				setState(352);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,11,_ctx) ) {
				case 1:
					{
					setState(351);
					triplesTemplate();
					}
					break;
				}
				setState(354);
				match(T__12);
				setState(355);
				solutionModifier();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DescribeQueryContext extends ParserRuleContext {
		public SolutionModifierContext solutionModifier() {
			return getRuleContext(SolutionModifierContext.class,0);
		}
		public List<DatasetClauseContext> datasetClause() {
			return getRuleContexts(DatasetClauseContext.class);
		}
		public DatasetClauseContext datasetClause(int i) {
			return getRuleContext(DatasetClauseContext.class,i);
		}
		public WhereClauseContext whereClause() {
			return getRuleContext(WhereClauseContext.class,0);
		}
		public List<VarOrIriContext> varOrIri() {
			return getRuleContexts(VarOrIriContext.class);
		}
		public VarOrIriContext varOrIri(int i) {
			return getRuleContext(VarOrIriContext.class,i);
		}
		public DescribeQueryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_describeQuery; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterDescribeQuery(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitDescribeQuery(this);
		}
	}

	public final DescribeQueryContext describeQuery() throws RecognitionException {
		DescribeQueryContext _localctx = new DescribeQueryContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_describeQuery);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(358);
			match(T__13);
			setState(365);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case IRIREF:
			case PNAME_NS:
			case PNAME_LN:
			case VAR1:
			case VAR2:
				{
				setState(360); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(359);
					varOrIri();
					}
					}
					setState(362); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( ((((_la - 144)) & ~0x3f) == 0 && ((1L << (_la - 144)) & ((1L << (IRIREF - 144)) | (1L << (PNAME_NS - 144)) | (1L << (PNAME_LN - 144)) | (1L << (VAR1 - 144)) | (1L << (VAR2 - 144)))) != 0) );
				}
				break;
			case T__8:
				{
				setState(364);
				match(T__8);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(370);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__15) {
				{
				{
				setState(367);
				datasetClause();
				}
				}
				setState(372);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(374);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__10 || _la==T__11) {
				{
				setState(373);
				whereClause();
				}
			}

			setState(376);
			solutionModifier();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AskQueryContext extends ParserRuleContext {
		public WhereClauseContext whereClause() {
			return getRuleContext(WhereClauseContext.class,0);
		}
		public SolutionModifierContext solutionModifier() {
			return getRuleContext(SolutionModifierContext.class,0);
		}
		public List<DatasetClauseContext> datasetClause() {
			return getRuleContexts(DatasetClauseContext.class);
		}
		public DatasetClauseContext datasetClause(int i) {
			return getRuleContext(DatasetClauseContext.class,i);
		}
		public AskQueryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_askQuery; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterAskQuery(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitAskQuery(this);
		}
	}

	public final AskQueryContext askQuery() throws RecognitionException {
		AskQueryContext _localctx = new AskQueryContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_askQuery);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(378);
			match(T__14);
			setState(382);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__15) {
				{
				{
				setState(379);
				datasetClause();
				}
				}
				setState(384);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(385);
			whereClause();
			setState(386);
			solutionModifier();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DatasetClauseContext extends ParserRuleContext {
		public DefaultGraphClauseContext defaultGraphClause() {
			return getRuleContext(DefaultGraphClauseContext.class,0);
		}
		public NamedGraphClauseContext namedGraphClause() {
			return getRuleContext(NamedGraphClauseContext.class,0);
		}
		public DatasetClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_datasetClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterDatasetClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitDatasetClause(this);
		}
	}

	public final DatasetClauseContext datasetClause() throws RecognitionException {
		DatasetClauseContext _localctx = new DatasetClauseContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_datasetClause);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(388);
			match(T__15);
			setState(391);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case IRIREF:
			case PNAME_NS:
			case PNAME_LN:
				{
				setState(389);
				defaultGraphClause();
				}
				break;
			case T__16:
				{
				setState(390);
				namedGraphClause();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DefaultGraphClauseContext extends ParserRuleContext {
		public SourceSelectorContext sourceSelector() {
			return getRuleContext(SourceSelectorContext.class,0);
		}
		public DefaultGraphClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_defaultGraphClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterDefaultGraphClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitDefaultGraphClause(this);
		}
	}

	public final DefaultGraphClauseContext defaultGraphClause() throws RecognitionException {
		DefaultGraphClauseContext _localctx = new DefaultGraphClauseContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_defaultGraphClause);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(393);
			sourceSelector();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NamedGraphClauseContext extends ParserRuleContext {
		public SourceSelectorContext sourceSelector() {
			return getRuleContext(SourceSelectorContext.class,0);
		}
		public NamedGraphClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_namedGraphClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterNamedGraphClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitNamedGraphClause(this);
		}
	}

	public final NamedGraphClauseContext namedGraphClause() throws RecognitionException {
		NamedGraphClauseContext _localctx = new NamedGraphClauseContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_namedGraphClause);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(395);
			match(T__16);
			setState(396);
			sourceSelector();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SourceSelectorContext extends ParserRuleContext {
		public IriContext iri() {
			return getRuleContext(IriContext.class,0);
		}
		public SourceSelectorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sourceSelector; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterSourceSelector(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitSourceSelector(this);
		}
	}

	public final SourceSelectorContext sourceSelector() throws RecognitionException {
		SourceSelectorContext _localctx = new SourceSelectorContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_sourceSelector);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(398);
			iri();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WhereClauseContext extends ParserRuleContext {
		public GroupGraphPatternContext groupGraphPattern() {
			return getRuleContext(GroupGraphPatternContext.class,0);
		}
		public WhereClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_whereClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterWhereClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitWhereClause(this);
		}
	}

	public final WhereClauseContext whereClause() throws RecognitionException {
		WhereClauseContext _localctx = new WhereClauseContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_whereClause);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(401);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__10) {
				{
				setState(400);
				match(T__10);
				}
			}

			setState(403);
			groupGraphPattern();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SolutionModifierContext extends ParserRuleContext {
		public GroupClauseContext groupClause() {
			return getRuleContext(GroupClauseContext.class,0);
		}
		public HavingClauseContext havingClause() {
			return getRuleContext(HavingClauseContext.class,0);
		}
		public OrderClauseContext orderClause() {
			return getRuleContext(OrderClauseContext.class,0);
		}
		public LimitOffsetClausesContext limitOffsetClauses() {
			return getRuleContext(LimitOffsetClausesContext.class,0);
		}
		public SolutionModifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_solutionModifier; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterSolutionModifier(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitSolutionModifier(this);
		}
	}

	public final SolutionModifierContext solutionModifier() throws RecognitionException {
		SolutionModifierContext _localctx = new SolutionModifierContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_solutionModifier);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(406);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__17) {
				{
				setState(405);
				groupClause();
				}
			}

			setState(409);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__19) {
				{
				setState(408);
				havingClause();
				}
			}

			setState(412);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__20) {
				{
				setState(411);
				orderClause();
				}
			}

			setState(415);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__23 || _la==T__24) {
				{
				setState(414);
				limitOffsetClauses();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GroupClauseContext extends ParserRuleContext {
		public List<GroupConditionContext> groupCondition() {
			return getRuleContexts(GroupConditionContext.class);
		}
		public GroupConditionContext groupCondition(int i) {
			return getRuleContext(GroupConditionContext.class,i);
		}
		public GroupClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_groupClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterGroupClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitGroupClause(this);
		}
	}

	public final GroupClauseContext groupClause() throws RecognitionException {
		GroupClauseContext _localctx = new GroupClauseContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_groupClause);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(417);
			match(T__17);
			setState(418);
			match(T__18);
			setState(420); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(419);
				groupCondition();
				}
				}
				setState(422); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==T__5 || ((((_la - 76)) & ~0x3f) == 0 && ((1L << (_la - 76)) & ((1L << (T__75 - 76)) | (1L << (T__77 - 76)) | (1L << (T__78 - 76)) | (1L << (T__79 - 76)) | (1L << (T__80 - 76)) | (1L << (T__81 - 76)) | (1L << (T__82 - 76)) | (1L << (T__83 - 76)) | (1L << (T__84 - 76)) | (1L << (T__85 - 76)) | (1L << (T__86 - 76)) | (1L << (T__87 - 76)) | (1L << (T__88 - 76)) | (1L << (T__89 - 76)) | (1L << (T__90 - 76)) | (1L << (T__91 - 76)) | (1L << (T__92 - 76)) | (1L << (T__93 - 76)) | (1L << (T__94 - 76)) | (1L << (T__95 - 76)) | (1L << (T__96 - 76)) | (1L << (T__97 - 76)) | (1L << (T__98 - 76)) | (1L << (T__99 - 76)) | (1L << (T__100 - 76)) | (1L << (T__101 - 76)) | (1L << (T__102 - 76)) | (1L << (T__103 - 76)) | (1L << (T__104 - 76)) | (1L << (T__105 - 76)) | (1L << (T__106 - 76)) | (1L << (T__107 - 76)) | (1L << (T__108 - 76)) | (1L << (T__109 - 76)) | (1L << (T__110 - 76)) | (1L << (T__111 - 76)) | (1L << (T__112 - 76)) | (1L << (T__113 - 76)) | (1L << (T__114 - 76)) | (1L << (T__115 - 76)) | (1L << (T__116 - 76)) | (1L << (T__117 - 76)) | (1L << (T__118 - 76)) | (1L << (T__119 - 76)) | (1L << (T__120 - 76)) | (1L << (T__121 - 76)) | (1L << (T__122 - 76)) | (1L << (T__123 - 76)) | (1L << (T__124 - 76)) | (1L << (T__125 - 76)) | (1L << (T__126 - 76)) | (1L << (T__127 - 76)) | (1L << (T__128 - 76)) | (1L << (T__129 - 76)) | (1L << (T__130 - 76)) | (1L << (T__131 - 76)) | (1L << (T__132 - 76)) | (1L << (T__133 - 76)) | (1L << (T__134 - 76)) | (1L << (T__135 - 76)) | (1L << (T__136 - 76)))) != 0) || ((((_la - 144)) & ~0x3f) == 0 && ((1L << (_la - 144)) & ((1L << (IRIREF - 144)) | (1L << (PNAME_NS - 144)) | (1L << (PNAME_LN - 144)) | (1L << (VAR1 - 144)) | (1L << (VAR2 - 144)))) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GroupConditionContext extends ParserRuleContext {
		public BuiltInCallContext builtInCall() {
			return getRuleContext(BuiltInCallContext.class,0);
		}
		public FunctionCallContext functionCall() {
			return getRuleContext(FunctionCallContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public VarContext var() {
			return getRuleContext(VarContext.class,0);
		}
		public GroupConditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_groupCondition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterGroupCondition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitGroupCondition(this);
		}
	}

	public final GroupConditionContext groupCondition() throws RecognitionException {
		GroupConditionContext _localctx = new GroupConditionContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_groupCondition);
		int _la;
		try {
			setState(435);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__75:
			case T__77:
			case T__78:
			case T__79:
			case T__80:
			case T__81:
			case T__82:
			case T__83:
			case T__84:
			case T__85:
			case T__86:
			case T__87:
			case T__88:
			case T__89:
			case T__90:
			case T__91:
			case T__92:
			case T__93:
			case T__94:
			case T__95:
			case T__96:
			case T__97:
			case T__98:
			case T__99:
			case T__100:
			case T__101:
			case T__102:
			case T__103:
			case T__104:
			case T__105:
			case T__106:
			case T__107:
			case T__108:
			case T__109:
			case T__110:
			case T__111:
			case T__112:
			case T__113:
			case T__114:
			case T__115:
			case T__116:
			case T__117:
			case T__118:
			case T__119:
			case T__120:
			case T__121:
			case T__122:
			case T__123:
			case T__124:
			case T__125:
			case T__126:
			case T__127:
			case T__128:
			case T__129:
			case T__130:
			case T__131:
			case T__132:
			case T__133:
			case T__134:
			case T__135:
			case T__136:
				enterOuterAlt(_localctx, 1);
				{
				setState(424);
				builtInCall();
				}
				break;
			case IRIREF:
			case PNAME_NS:
			case PNAME_LN:
				enterOuterAlt(_localctx, 2);
				{
				setState(425);
				functionCall();
				}
				break;
			case T__5:
				enterOuterAlt(_localctx, 3);
				{
				setState(426);
				match(T__5);
				setState(427);
				expression();
				setState(430);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__6) {
					{
					setState(428);
					match(T__6);
					setState(429);
					var();
					}
				}

				setState(432);
				match(T__7);
				}
				break;
			case VAR1:
			case VAR2:
				enterOuterAlt(_localctx, 4);
				{
				setState(434);
				var();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class HavingClauseContext extends ParserRuleContext {
		public List<HavingConditionContext> havingCondition() {
			return getRuleContexts(HavingConditionContext.class);
		}
		public HavingConditionContext havingCondition(int i) {
			return getRuleContext(HavingConditionContext.class,i);
		}
		public HavingClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_havingClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterHavingClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitHavingClause(this);
		}
	}

	public final HavingClauseContext havingClause() throws RecognitionException {
		HavingClauseContext _localctx = new HavingClauseContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_havingClause);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(437);
			match(T__19);
			setState(439); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(438);
				havingCondition();
				}
				}
				setState(441); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==T__5 || ((((_la - 76)) & ~0x3f) == 0 && ((1L << (_la - 76)) & ((1L << (T__75 - 76)) | (1L << (T__77 - 76)) | (1L << (T__78 - 76)) | (1L << (T__79 - 76)) | (1L << (T__80 - 76)) | (1L << (T__81 - 76)) | (1L << (T__82 - 76)) | (1L << (T__83 - 76)) | (1L << (T__84 - 76)) | (1L << (T__85 - 76)) | (1L << (T__86 - 76)) | (1L << (T__87 - 76)) | (1L << (T__88 - 76)) | (1L << (T__89 - 76)) | (1L << (T__90 - 76)) | (1L << (T__91 - 76)) | (1L << (T__92 - 76)) | (1L << (T__93 - 76)) | (1L << (T__94 - 76)) | (1L << (T__95 - 76)) | (1L << (T__96 - 76)) | (1L << (T__97 - 76)) | (1L << (T__98 - 76)) | (1L << (T__99 - 76)) | (1L << (T__100 - 76)) | (1L << (T__101 - 76)) | (1L << (T__102 - 76)) | (1L << (T__103 - 76)) | (1L << (T__104 - 76)) | (1L << (T__105 - 76)) | (1L << (T__106 - 76)) | (1L << (T__107 - 76)) | (1L << (T__108 - 76)) | (1L << (T__109 - 76)) | (1L << (T__110 - 76)) | (1L << (T__111 - 76)) | (1L << (T__112 - 76)) | (1L << (T__113 - 76)) | (1L << (T__114 - 76)) | (1L << (T__115 - 76)) | (1L << (T__116 - 76)) | (1L << (T__117 - 76)) | (1L << (T__118 - 76)) | (1L << (T__119 - 76)) | (1L << (T__120 - 76)) | (1L << (T__121 - 76)) | (1L << (T__122 - 76)) | (1L << (T__123 - 76)) | (1L << (T__124 - 76)) | (1L << (T__125 - 76)) | (1L << (T__126 - 76)) | (1L << (T__127 - 76)) | (1L << (T__128 - 76)) | (1L << (T__129 - 76)) | (1L << (T__130 - 76)) | (1L << (T__131 - 76)) | (1L << (T__132 - 76)) | (1L << (T__133 - 76)) | (1L << (T__134 - 76)) | (1L << (T__135 - 76)) | (1L << (T__136 - 76)))) != 0) || ((((_la - 144)) & ~0x3f) == 0 && ((1L << (_la - 144)) & ((1L << (IRIREF - 144)) | (1L << (PNAME_NS - 144)) | (1L << (PNAME_LN - 144)))) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class HavingConditionContext extends ParserRuleContext {
		public ConstraintContext constraint() {
			return getRuleContext(ConstraintContext.class,0);
		}
		public HavingConditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_havingCondition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterHavingCondition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitHavingCondition(this);
		}
	}

	public final HavingConditionContext havingCondition() throws RecognitionException {
		HavingConditionContext _localctx = new HavingConditionContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_havingCondition);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(443);
			constraint();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OrderClauseContext extends ParserRuleContext {
		public List<OrderConditionContext> orderCondition() {
			return getRuleContexts(OrderConditionContext.class);
		}
		public OrderConditionContext orderCondition(int i) {
			return getRuleContext(OrderConditionContext.class,i);
		}
		public OrderClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_orderClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterOrderClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitOrderClause(this);
		}
	}

	public final OrderClauseContext orderClause() throws RecognitionException {
		OrderClauseContext _localctx = new OrderClauseContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_orderClause);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(445);
			match(T__20);
			setState(446);
			match(T__18);
			setState(448); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(447);
				orderCondition();
				}
				}
				setState(450); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__5) | (1L << T__21) | (1L << T__22))) != 0) || ((((_la - 76)) & ~0x3f) == 0 && ((1L << (_la - 76)) & ((1L << (T__75 - 76)) | (1L << (T__77 - 76)) | (1L << (T__78 - 76)) | (1L << (T__79 - 76)) | (1L << (T__80 - 76)) | (1L << (T__81 - 76)) | (1L << (T__82 - 76)) | (1L << (T__83 - 76)) | (1L << (T__84 - 76)) | (1L << (T__85 - 76)) | (1L << (T__86 - 76)) | (1L << (T__87 - 76)) | (1L << (T__88 - 76)) | (1L << (T__89 - 76)) | (1L << (T__90 - 76)) | (1L << (T__91 - 76)) | (1L << (T__92 - 76)) | (1L << (T__93 - 76)) | (1L << (T__94 - 76)) | (1L << (T__95 - 76)) | (1L << (T__96 - 76)) | (1L << (T__97 - 76)) | (1L << (T__98 - 76)) | (1L << (T__99 - 76)) | (1L << (T__100 - 76)) | (1L << (T__101 - 76)) | (1L << (T__102 - 76)) | (1L << (T__103 - 76)) | (1L << (T__104 - 76)) | (1L << (T__105 - 76)) | (1L << (T__106 - 76)) | (1L << (T__107 - 76)) | (1L << (T__108 - 76)) | (1L << (T__109 - 76)) | (1L << (T__110 - 76)) | (1L << (T__111 - 76)) | (1L << (T__112 - 76)) | (1L << (T__113 - 76)) | (1L << (T__114 - 76)) | (1L << (T__115 - 76)) | (1L << (T__116 - 76)) | (1L << (T__117 - 76)) | (1L << (T__118 - 76)) | (1L << (T__119 - 76)) | (1L << (T__120 - 76)) | (1L << (T__121 - 76)) | (1L << (T__122 - 76)) | (1L << (T__123 - 76)) | (1L << (T__124 - 76)) | (1L << (T__125 - 76)) | (1L << (T__126 - 76)) | (1L << (T__127 - 76)) | (1L << (T__128 - 76)) | (1L << (T__129 - 76)) | (1L << (T__130 - 76)) | (1L << (T__131 - 76)) | (1L << (T__132 - 76)) | (1L << (T__133 - 76)) | (1L << (T__134 - 76)) | (1L << (T__135 - 76)) | (1L << (T__136 - 76)))) != 0) || ((((_la - 144)) & ~0x3f) == 0 && ((1L << (_la - 144)) & ((1L << (IRIREF - 144)) | (1L << (PNAME_NS - 144)) | (1L << (PNAME_LN - 144)) | (1L << (VAR1 - 144)) | (1L << (VAR2 - 144)))) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OrderConditionContext extends ParserRuleContext {
		public BrackettedExpressionContext brackettedExpression() {
			return getRuleContext(BrackettedExpressionContext.class,0);
		}
		public ConstraintContext constraint() {
			return getRuleContext(ConstraintContext.class,0);
		}
		public VarContext var() {
			return getRuleContext(VarContext.class,0);
		}
		public OrderConditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_orderCondition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterOrderCondition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitOrderCondition(this);
		}
	}

	public final OrderConditionContext orderCondition() throws RecognitionException {
		OrderConditionContext _localctx = new OrderConditionContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_orderCondition);
		int _la;
		try {
			setState(458);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__21:
			case T__22:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(452);
				_la = _input.LA(1);
				if ( !(_la==T__21 || _la==T__22) ) {
				_errHandler.recoverInline(this);
				}
				else {
					if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(453);
				brackettedExpression();
				}
				}
				break;
			case T__5:
			case T__75:
			case T__77:
			case T__78:
			case T__79:
			case T__80:
			case T__81:
			case T__82:
			case T__83:
			case T__84:
			case T__85:
			case T__86:
			case T__87:
			case T__88:
			case T__89:
			case T__90:
			case T__91:
			case T__92:
			case T__93:
			case T__94:
			case T__95:
			case T__96:
			case T__97:
			case T__98:
			case T__99:
			case T__100:
			case T__101:
			case T__102:
			case T__103:
			case T__104:
			case T__105:
			case T__106:
			case T__107:
			case T__108:
			case T__109:
			case T__110:
			case T__111:
			case T__112:
			case T__113:
			case T__114:
			case T__115:
			case T__116:
			case T__117:
			case T__118:
			case T__119:
			case T__120:
			case T__121:
			case T__122:
			case T__123:
			case T__124:
			case T__125:
			case T__126:
			case T__127:
			case T__128:
			case T__129:
			case T__130:
			case T__131:
			case T__132:
			case T__133:
			case T__134:
			case T__135:
			case T__136:
			case IRIREF:
			case PNAME_NS:
			case PNAME_LN:
			case VAR1:
			case VAR2:
				enterOuterAlt(_localctx, 2);
				{
				setState(456);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__5:
				case T__75:
				case T__77:
				case T__78:
				case T__79:
				case T__80:
				case T__81:
				case T__82:
				case T__83:
				case T__84:
				case T__85:
				case T__86:
				case T__87:
				case T__88:
				case T__89:
				case T__90:
				case T__91:
				case T__92:
				case T__93:
				case T__94:
				case T__95:
				case T__96:
				case T__97:
				case T__98:
				case T__99:
				case T__100:
				case T__101:
				case T__102:
				case T__103:
				case T__104:
				case T__105:
				case T__106:
				case T__107:
				case T__108:
				case T__109:
				case T__110:
				case T__111:
				case T__112:
				case T__113:
				case T__114:
				case T__115:
				case T__116:
				case T__117:
				case T__118:
				case T__119:
				case T__120:
				case T__121:
				case T__122:
				case T__123:
				case T__124:
				case T__125:
				case T__126:
				case T__127:
				case T__128:
				case T__129:
				case T__130:
				case T__131:
				case T__132:
				case T__133:
				case T__134:
				case T__135:
				case T__136:
				case IRIREF:
				case PNAME_NS:
				case PNAME_LN:
					{
					setState(454);
					constraint();
					}
					break;
				case VAR1:
				case VAR2:
					{
					setState(455);
					var();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LimitOffsetClausesContext extends ParserRuleContext {
		public LimitClauseContext limitClause() {
			return getRuleContext(LimitClauseContext.class,0);
		}
		public OffsetClauseContext offsetClause() {
			return getRuleContext(OffsetClauseContext.class,0);
		}
		public LimitOffsetClausesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_limitOffsetClauses; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterLimitOffsetClauses(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitLimitOffsetClauses(this);
		}
	}

	public final LimitOffsetClausesContext limitOffsetClauses() throws RecognitionException {
		LimitOffsetClausesContext _localctx = new LimitOffsetClausesContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_limitOffsetClauses);
		int _la;
		try {
			setState(468);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__23:
				enterOuterAlt(_localctx, 1);
				{
				setState(460);
				limitClause();
				setState(462);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__24) {
					{
					setState(461);
					offsetClause();
					}
				}

				}
				break;
			case T__24:
				enterOuterAlt(_localctx, 2);
				{
				setState(464);
				offsetClause();
				setState(466);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__23) {
					{
					setState(465);
					limitClause();
					}
				}

				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LimitClauseContext extends ParserRuleContext {
		public TerminalNode INTEGER() { return getToken(SparqlParser.INTEGER, 0); }
		public LimitClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_limitClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterLimitClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitLimitClause(this);
		}
	}

	public final LimitClauseContext limitClause() throws RecognitionException {
		LimitClauseContext _localctx = new LimitClauseContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_limitClause);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(470);
			match(T__23);
			setState(471);
			match(INTEGER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OffsetClauseContext extends ParserRuleContext {
		public TerminalNode INTEGER() { return getToken(SparqlParser.INTEGER, 0); }
		public OffsetClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_offsetClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterOffsetClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitOffsetClause(this);
		}
	}

	public final OffsetClauseContext offsetClause() throws RecognitionException {
		OffsetClauseContext _localctx = new OffsetClauseContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_offsetClause);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(473);
			match(T__24);
			setState(474);
			match(INTEGER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValuesClauseContext extends ParserRuleContext {
		public DataBlockContext dataBlock() {
			return getRuleContext(DataBlockContext.class,0);
		}
		public ValuesClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_valuesClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterValuesClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitValuesClause(this);
		}
	}

	public final ValuesClauseContext valuesClause() throws RecognitionException {
		ValuesClauseContext _localctx = new ValuesClauseContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_valuesClause);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(478);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__25) {
				{
				setState(476);
				match(T__25);
				setState(477);
				dataBlock();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UpdateContext extends ParserRuleContext {
		public PrologueContext prologue() {
			return getRuleContext(PrologueContext.class,0);
		}
		public Update1Context update1() {
			return getRuleContext(Update1Context.class,0);
		}
		public UpdateContext update() {
			return getRuleContext(UpdateContext.class,0);
		}
		public UpdateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_update; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterUpdate(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitUpdate(this);
		}
	}

	public final UpdateContext update() throws RecognitionException {
		UpdateContext _localctx = new UpdateContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_update);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(480);
			prologue();
			setState(486);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__27) | (1L << T__30) | (1L << T__31) | (1L << T__32) | (1L << T__33) | (1L << T__35) | (1L << T__36) | (1L << T__37) | (1L << T__38) | (1L << T__39) | (1L << T__40) | (1L << T__41) | (1L << T__42))) != 0)) {
				{
				setState(481);
				update1();
				setState(484);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__26) {
					{
					setState(482);
					match(T__26);
					setState(483);
					update();
					}
				}

				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Update1Context extends ParserRuleContext {
		public LoadContext load() {
			return getRuleContext(LoadContext.class,0);
		}
		public ClearContext clear() {
			return getRuleContext(ClearContext.class,0);
		}
		public DropContext drop() {
			return getRuleContext(DropContext.class,0);
		}
		public AddContext add() {
			return getRuleContext(AddContext.class,0);
		}
		public MoveContext move() {
			return getRuleContext(MoveContext.class,0);
		}
		public CopyContext copy() {
			return getRuleContext(CopyContext.class,0);
		}
		public CreateContext create() {
			return getRuleContext(CreateContext.class,0);
		}
		public InsertDataContext insertData() {
			return getRuleContext(InsertDataContext.class,0);
		}
		public DeleteDataContext deleteData() {
			return getRuleContext(DeleteDataContext.class,0);
		}
		public DeleteWhereContext deleteWhere() {
			return getRuleContext(DeleteWhereContext.class,0);
		}
		public ModifyContext modify() {
			return getRuleContext(ModifyContext.class,0);
		}
		public Update1Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_update1; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterUpdate1(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitUpdate1(this);
		}
	}

	public final Update1Context update1() throws RecognitionException {
		Update1Context _localctx = new Update1Context(_ctx, getState());
		enterRule(_localctx, 56, RULE_update1);
		try {
			setState(499);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__27:
				enterOuterAlt(_localctx, 1);
				{
				setState(488);
				load();
				}
				break;
			case T__30:
				enterOuterAlt(_localctx, 2);
				{
				setState(489);
				clear();
				}
				break;
			case T__31:
				enterOuterAlt(_localctx, 3);
				{
				setState(490);
				drop();
				}
				break;
			case T__33:
				enterOuterAlt(_localctx, 4);
				{
				setState(491);
				add();
				}
				break;
			case T__35:
				enterOuterAlt(_localctx, 5);
				{
				setState(492);
				move();
				}
				break;
			case T__36:
				enterOuterAlt(_localctx, 6);
				{
				setState(493);
				copy();
				}
				break;
			case T__32:
				enterOuterAlt(_localctx, 7);
				{
				setState(494);
				create();
				}
				break;
			case T__37:
				enterOuterAlt(_localctx, 8);
				{
				setState(495);
				insertData();
				}
				break;
			case T__38:
				enterOuterAlt(_localctx, 9);
				{
				setState(496);
				deleteData();
				}
				break;
			case T__39:
				enterOuterAlt(_localctx, 10);
				{
				setState(497);
				deleteWhere();
				}
				break;
			case T__40:
			case T__41:
			case T__42:
				enterOuterAlt(_localctx, 11);
				{
				setState(498);
				modify();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LoadContext extends ParserRuleContext {
		public IriContext iri() {
			return getRuleContext(IriContext.class,0);
		}
		public GraphRefContext graphRef() {
			return getRuleContext(GraphRefContext.class,0);
		}
		public LoadContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_load; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterLoad(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitLoad(this);
		}
	}

	public final LoadContext load() throws RecognitionException {
		LoadContext _localctx = new LoadContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_load);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(501);
			match(T__27);
			setState(503);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__28) {
				{
				setState(502);
				match(T__28);
				}
			}

			setState(505);
			iri();
			setState(508);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__29) {
				{
				setState(506);
				match(T__29);
				setState(507);
				graphRef();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ClearContext extends ParserRuleContext {
		public GraphRefAllContext graphRefAll() {
			return getRuleContext(GraphRefAllContext.class,0);
		}
		public ClearContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_clear; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterClear(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitClear(this);
		}
	}

	public final ClearContext clear() throws RecognitionException {
		ClearContext _localctx = new ClearContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_clear);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(510);
			match(T__30);
			setState(512);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__28) {
				{
				setState(511);
				match(T__28);
				}
			}

			setState(514);
			graphRefAll();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DropContext extends ParserRuleContext {
		public GraphRefAllContext graphRefAll() {
			return getRuleContext(GraphRefAllContext.class,0);
		}
		public DropContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_drop; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterDrop(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitDrop(this);
		}
	}

	public final DropContext drop() throws RecognitionException {
		DropContext _localctx = new DropContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_drop);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(516);
			match(T__31);
			setState(518);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__28) {
				{
				setState(517);
				match(T__28);
				}
			}

			setState(520);
			graphRefAll();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CreateContext extends ParserRuleContext {
		public GraphRefContext graphRef() {
			return getRuleContext(GraphRefContext.class,0);
		}
		public CreateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_create; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterCreate(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitCreate(this);
		}
	}

	public final CreateContext create() throws RecognitionException {
		CreateContext _localctx = new CreateContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_create);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(522);
			match(T__32);
			setState(524);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__28) {
				{
				setState(523);
				match(T__28);
				}
			}

			setState(526);
			graphRef();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AddContext extends ParserRuleContext {
		public List<GraphOrDefaultContext> graphOrDefault() {
			return getRuleContexts(GraphOrDefaultContext.class);
		}
		public GraphOrDefaultContext graphOrDefault(int i) {
			return getRuleContext(GraphOrDefaultContext.class,i);
		}
		public AddContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_add; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterAdd(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitAdd(this);
		}
	}

	public final AddContext add() throws RecognitionException {
		AddContext _localctx = new AddContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_add);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(528);
			match(T__33);
			setState(530);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__28) {
				{
				setState(529);
				match(T__28);
				}
			}

			setState(532);
			graphOrDefault();
			setState(533);
			match(T__34);
			setState(534);
			graphOrDefault();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MoveContext extends ParserRuleContext {
		public List<GraphOrDefaultContext> graphOrDefault() {
			return getRuleContexts(GraphOrDefaultContext.class);
		}
		public GraphOrDefaultContext graphOrDefault(int i) {
			return getRuleContext(GraphOrDefaultContext.class,i);
		}
		public MoveContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_move; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterMove(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitMove(this);
		}
	}

	public final MoveContext move() throws RecognitionException {
		MoveContext _localctx = new MoveContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_move);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(536);
			match(T__35);
			setState(538);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__28) {
				{
				setState(537);
				match(T__28);
				}
			}

			setState(540);
			graphOrDefault();
			setState(541);
			match(T__34);
			setState(542);
			graphOrDefault();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CopyContext extends ParserRuleContext {
		public List<GraphOrDefaultContext> graphOrDefault() {
			return getRuleContexts(GraphOrDefaultContext.class);
		}
		public GraphOrDefaultContext graphOrDefault(int i) {
			return getRuleContext(GraphOrDefaultContext.class,i);
		}
		public CopyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_copy; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterCopy(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitCopy(this);
		}
	}

	public final CopyContext copy() throws RecognitionException {
		CopyContext _localctx = new CopyContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_copy);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(544);
			match(T__36);
			setState(546);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__28) {
				{
				setState(545);
				match(T__28);
				}
			}

			setState(548);
			graphOrDefault();
			setState(549);
			match(T__34);
			setState(550);
			graphOrDefault();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InsertDataContext extends ParserRuleContext {
		public QuadDataContext quadData() {
			return getRuleContext(QuadDataContext.class,0);
		}
		public InsertDataContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_insertData; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterInsertData(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitInsertData(this);
		}
	}

	public final InsertDataContext insertData() throws RecognitionException {
		InsertDataContext _localctx = new InsertDataContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_insertData);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(552);
			match(T__37);
			setState(553);
			quadData();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeleteDataContext extends ParserRuleContext {
		public QuadDataContext quadData() {
			return getRuleContext(QuadDataContext.class,0);
		}
		public DeleteDataContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_deleteData; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterDeleteData(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitDeleteData(this);
		}
	}

	public final DeleteDataContext deleteData() throws RecognitionException {
		DeleteDataContext _localctx = new DeleteDataContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_deleteData);

		allowsBlankNodes = false;

		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(555);
			match(T__38);
			setState(556);
			quadData();
			}
			_ctx.stop = _input.LT(-1);

			allowsBlankNodes = true;

		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeleteWhereContext extends ParserRuleContext {
		public QuadPatternContext quadPattern() {
			return getRuleContext(QuadPatternContext.class,0);
		}
		public DeleteWhereContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_deleteWhere; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterDeleteWhere(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitDeleteWhere(this);
		}
	}

	public final DeleteWhereContext deleteWhere() throws RecognitionException {
		DeleteWhereContext _localctx = new DeleteWhereContext(_ctx, getState());
		enterRule(_localctx, 76, RULE_deleteWhere);

		allowsBlankNodes = false;

		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(558);
			match(T__39);
			setState(559);
			quadPattern();
			}
			_ctx.stop = _input.LT(-1);

			allowsBlankNodes = true;

		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ModifyContext extends ParserRuleContext {
		public GroupGraphPatternContext groupGraphPattern() {
			return getRuleContext(GroupGraphPatternContext.class,0);
		}
		public DeleteClauseContext deleteClause() {
			return getRuleContext(DeleteClauseContext.class,0);
		}
		public InsertClauseContext insertClause() {
			return getRuleContext(InsertClauseContext.class,0);
		}
		public IriContext iri() {
			return getRuleContext(IriContext.class,0);
		}
		public List<UsingClauseContext> usingClause() {
			return getRuleContexts(UsingClauseContext.class);
		}
		public UsingClauseContext usingClause(int i) {
			return getRuleContext(UsingClauseContext.class,i);
		}
		public ModifyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_modify; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterModify(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitModify(this);
		}
	}

	public final ModifyContext modify() throws RecognitionException {
		ModifyContext _localctx = new ModifyContext(_ctx, getState());
		enterRule(_localctx, 78, RULE_modify);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(563);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__40) {
				{
				setState(561);
				match(T__40);
				setState(562);
				iri();
				}
			}

			setState(570);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__41:
				{
				setState(565);
				deleteClause();
				setState(567);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__42) {
					{
					setState(566);
					insertClause();
					}
				}

				}
				break;
			case T__42:
				{
				setState(569);
				insertClause();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(575);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__43) {
				{
				{
				setState(572);
				usingClause();
				}
				}
				setState(577);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(578);
			match(T__10);
			setState(579);
			groupGraphPattern();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeleteClauseContext extends ParserRuleContext {
		public QuadPatternContext quadPattern() {
			return getRuleContext(QuadPatternContext.class,0);
		}
		public DeleteClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_deleteClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterDeleteClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitDeleteClause(this);
		}
	}

	public final DeleteClauseContext deleteClause() throws RecognitionException {
		DeleteClauseContext _localctx = new DeleteClauseContext(_ctx, getState());
		enterRule(_localctx, 80, RULE_deleteClause);

		allowsBlankNodes = false;

		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(581);
			match(T__41);
			setState(582);
			quadPattern();
			}
			_ctx.stop = _input.LT(-1);

			allowsBlankNodes = true;

		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InsertClauseContext extends ParserRuleContext {
		public QuadPatternContext quadPattern() {
			return getRuleContext(QuadPatternContext.class,0);
		}
		public InsertClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_insertClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterInsertClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitInsertClause(this);
		}
	}

	public final InsertClauseContext insertClause() throws RecognitionException {
		InsertClauseContext _localctx = new InsertClauseContext(_ctx, getState());
		enterRule(_localctx, 82, RULE_insertClause);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(584);
			match(T__42);
			setState(585);
			quadPattern();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UsingClauseContext extends ParserRuleContext {
		public IriContext iri() {
			return getRuleContext(IriContext.class,0);
		}
		public UsingClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_usingClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterUsingClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitUsingClause(this);
		}
	}

	public final UsingClauseContext usingClause() throws RecognitionException {
		UsingClauseContext _localctx = new UsingClauseContext(_ctx, getState());
		enterRule(_localctx, 84, RULE_usingClause);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(587);
			match(T__43);
			setState(591);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case IRIREF:
			case PNAME_NS:
			case PNAME_LN:
				{
				setState(588);
				iri();
				}
				break;
			case T__16:
				{
				setState(589);
				match(T__16);
				setState(590);
				iri();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GraphOrDefaultContext extends ParserRuleContext {
		public IriContext iri() {
			return getRuleContext(IriContext.class,0);
		}
		public GraphOrDefaultContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_graphOrDefault; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterGraphOrDefault(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitGraphOrDefault(this);
		}
	}

	public final GraphOrDefaultContext graphOrDefault() throws RecognitionException {
		GraphOrDefaultContext _localctx = new GraphOrDefaultContext(_ctx, getState());
		enterRule(_localctx, 86, RULE_graphOrDefault);
		int _la;
		try {
			setState(598);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__44:
				enterOuterAlt(_localctx, 1);
				{
				setState(593);
				match(T__44);
				}
				break;
			case T__45:
			case IRIREF:
			case PNAME_NS:
			case PNAME_LN:
				enterOuterAlt(_localctx, 2);
				{
				setState(595);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__45) {
					{
					setState(594);
					match(T__45);
					}
				}

				setState(597);
				iri();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GraphRefContext extends ParserRuleContext {
		public IriContext iri() {
			return getRuleContext(IriContext.class,0);
		}
		public GraphRefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_graphRef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterGraphRef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitGraphRef(this);
		}
	}

	public final GraphRefContext graphRef() throws RecognitionException {
		GraphRefContext _localctx = new GraphRefContext(_ctx, getState());
		enterRule(_localctx, 88, RULE_graphRef);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(600);
			match(T__45);
			setState(601);
			iri();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GraphRefAllContext extends ParserRuleContext {
		public GraphRefContext graphRef() {
			return getRuleContext(GraphRefContext.class,0);
		}
		public GraphRefAllContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_graphRefAll; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterGraphRefAll(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitGraphRefAll(this);
		}
	}

	public final GraphRefAllContext graphRefAll() throws RecognitionException {
		GraphRefAllContext _localctx = new GraphRefAllContext(_ctx, getState());
		enterRule(_localctx, 90, RULE_graphRefAll);
		try {
			setState(607);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__45:
				enterOuterAlt(_localctx, 1);
				{
				setState(603);
				graphRef();
				}
				break;
			case T__44:
				enterOuterAlt(_localctx, 2);
				{
				setState(604);
				match(T__44);
				}
				break;
			case T__16:
				enterOuterAlt(_localctx, 3);
				{
				setState(605);
				match(T__16);
				}
				break;
			case T__46:
				enterOuterAlt(_localctx, 4);
				{
				setState(606);
				match(T__46);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class QuadPatternContext extends ParserRuleContext {
		public QuadsContext quads() {
			return getRuleContext(QuadsContext.class,0);
		}
		public QuadPatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_quadPattern; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterQuadPattern(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitQuadPattern(this);
		}
	}

	public final QuadPatternContext quadPattern() throws RecognitionException {
		QuadPatternContext _localctx = new QuadPatternContext(_ctx, getState());
		enterRule(_localctx, 92, RULE_quadPattern);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(609);
			match(T__11);
			setState(610);
			quads();
			setState(611);
			match(T__12);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class QuadDataContext extends ParserRuleContext {
		public QuadsContext quads() {
			return getRuleContext(QuadsContext.class,0);
		}
		public QuadDataContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_quadData; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterQuadData(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitQuadData(this);
		}
	}

	public final QuadDataContext quadData() throws RecognitionException {
		QuadDataContext _localctx = new QuadDataContext(_ctx, getState());
		enterRule(_localctx, 94, RULE_quadData);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(613);
			match(T__11);
			setState(614);
			quads();
			setState(615);
			match(T__12);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class QuadsContext extends ParserRuleContext {
		public List<TriplesTemplateContext> triplesTemplate() {
			return getRuleContexts(TriplesTemplateContext.class);
		}
		public TriplesTemplateContext triplesTemplate(int i) {
			return getRuleContext(TriplesTemplateContext.class,i);
		}
		public List<QuadsNotTriplesContext> quadsNotTriples() {
			return getRuleContexts(QuadsNotTriplesContext.class);
		}
		public QuadsNotTriplesContext quadsNotTriples(int i) {
			return getRuleContext(QuadsNotTriplesContext.class,i);
		}
		public QuadsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_quads; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterQuads(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitQuads(this);
		}
	}

	public final QuadsContext quads() throws RecognitionException {
		QuadsContext _localctx = new QuadsContext(_ctx, getState());
		enterRule(_localctx, 96, RULE_quads);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(618);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,54,_ctx) ) {
			case 1:
				{
				setState(617);
				triplesTemplate();
				}
				break;
			}
			setState(629);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__45) {
				{
				{
				setState(620);
				quadsNotTriples();
				setState(622);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,55,_ctx) ) {
				case 1:
					{
					setState(621);
					match(T__47);
					}
					break;
				}
				setState(625);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,56,_ctx) ) {
				case 1:
					{
					setState(624);
					triplesTemplate();
					}
					break;
				}
				}
				}
				setState(631);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class QuadsNotTriplesContext extends ParserRuleContext {
		public VarOrIriContext varOrIri() {
			return getRuleContext(VarOrIriContext.class,0);
		}
		public GroupGraphPatternContext groupGraphPattern() {
			return getRuleContext(GroupGraphPatternContext.class,0);
		}
		public QuadsNotTriplesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_quadsNotTriples; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterQuadsNotTriples(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitQuadsNotTriples(this);
		}
	}

	public final QuadsNotTriplesContext quadsNotTriples() throws RecognitionException {
		QuadsNotTriplesContext _localctx = new QuadsNotTriplesContext(_ctx, getState());
		enterRule(_localctx, 98, RULE_quadsNotTriples);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(632);
			match(T__45);
			setState(633);
			varOrIri();
			setState(634);
			groupGraphPattern();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TriplesTemplateContext extends ParserRuleContext {
		public TriplesSameSubjectContext triplesSameSubject() {
			return getRuleContext(TriplesSameSubjectContext.class,0);
		}
		public TriplesTemplateContext triplesTemplate() {
			return getRuleContext(TriplesTemplateContext.class,0);
		}
		public TriplesTemplateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_triplesTemplate; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterTriplesTemplate(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitTriplesTemplate(this);
		}
	}

	public final TriplesTemplateContext triplesTemplate() throws RecognitionException {
		TriplesTemplateContext _localctx = new TriplesTemplateContext(_ctx, getState());
		enterRule(_localctx, 100, RULE_triplesTemplate);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(636);
			triplesSameSubject();
			setState(641);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__47) {
				{
				setState(637);
				match(T__47);
				setState(639);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,58,_ctx) ) {
				case 1:
					{
					setState(638);
					triplesTemplate();
					}
					break;
				}
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GroupGraphPatternContext extends ParserRuleContext {
		public SubSelectContext subSelect() {
			return getRuleContext(SubSelectContext.class,0);
		}
		public GroupGraphPatternSubContext groupGraphPatternSub() {
			return getRuleContext(GroupGraphPatternSubContext.class,0);
		}
		public GroupGraphPatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_groupGraphPattern; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterGroupGraphPattern(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitGroupGraphPattern(this);
		}
	}

	public final GroupGraphPatternContext groupGraphPattern() throws RecognitionException {
		GroupGraphPatternContext _localctx = new GroupGraphPatternContext(_ctx, getState());
		enterRule(_localctx, 102, RULE_groupGraphPattern);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(643);
			match(T__11);
			setState(646);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,60,_ctx) ) {
			case 1:
				{
				setState(644);
				subSelect();
				}
				break;
			case 2:
				{
				setState(645);
				groupGraphPatternSub();
				}
				break;
			}
			setState(648);
			match(T__12);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GroupGraphPatternSubContext extends ParserRuleContext {
		public List<TriplesBlockContext> triplesBlock() {
			return getRuleContexts(TriplesBlockContext.class);
		}
		public TriplesBlockContext triplesBlock(int i) {
			return getRuleContext(TriplesBlockContext.class,i);
		}
		public List<GraphPatternNotTriplesContext> graphPatternNotTriples() {
			return getRuleContexts(GraphPatternNotTriplesContext.class);
		}
		public GraphPatternNotTriplesContext graphPatternNotTriples(int i) {
			return getRuleContext(GraphPatternNotTriplesContext.class,i);
		}
		public GroupGraphPatternSubContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_groupGraphPatternSub; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterGroupGraphPatternSub(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitGroupGraphPatternSub(this);
		}
	}

	public final GroupGraphPatternSubContext groupGraphPatternSub() throws RecognitionException {
		GroupGraphPatternSubContext _localctx = new GroupGraphPatternSubContext(_ctx, getState());
		enterRule(_localctx, 104, RULE_groupGraphPatternSub);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(651);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,61,_ctx) ) {
			case 1:
				{
				setState(650);
				triplesBlock();
				}
				break;
			}
			setState(662);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__11) | (1L << T__25) | (1L << T__45) | (1L << T__48) | (1L << T__49) | (1L << T__50) | (1L << T__52) | (1L << T__54))) != 0)) {
				{
				{
				setState(653);
				graphPatternNotTriples();
				setState(655);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,62,_ctx) ) {
				case 1:
					{
					setState(654);
					match(T__47);
					}
					break;
				}
				setState(658);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,63,_ctx) ) {
				case 1:
					{
					setState(657);
					triplesBlock();
					}
					break;
				}
				}
				}
				setState(664);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TriplesBlockContext extends ParserRuleContext {
		public TriplesSameSubjectPathContext triplesSameSubjectPath() {
			return getRuleContext(TriplesSameSubjectPathContext.class,0);
		}
		public TriplesBlockContext triplesBlock() {
			return getRuleContext(TriplesBlockContext.class,0);
		}
		public TriplesBlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_triplesBlock; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterTriplesBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitTriplesBlock(this);
		}
	}

	public final TriplesBlockContext triplesBlock() throws RecognitionException {
		TriplesBlockContext _localctx = new TriplesBlockContext(_ctx, getState());
		enterRule(_localctx, 106, RULE_triplesBlock);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(665);
			triplesSameSubjectPath();
			setState(670);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__47) {
				{
				setState(666);
				match(T__47);
				setState(668);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,65,_ctx) ) {
				case 1:
					{
					setState(667);
					triplesBlock();
					}
					break;
				}
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GraphPatternNotTriplesContext extends ParserRuleContext {
		public GroupOrUnionGraphPatternContext groupOrUnionGraphPattern() {
			return getRuleContext(GroupOrUnionGraphPatternContext.class,0);
		}
		public OptionalGraphPatternContext optionalGraphPattern() {
			return getRuleContext(OptionalGraphPatternContext.class,0);
		}
		public MinusGraphPatternContext minusGraphPattern() {
			return getRuleContext(MinusGraphPatternContext.class,0);
		}
		public GraphGraphPatternContext graphGraphPattern() {
			return getRuleContext(GraphGraphPatternContext.class,0);
		}
		public ServiceGraphPatternContext serviceGraphPattern() {
			return getRuleContext(ServiceGraphPatternContext.class,0);
		}
		public FilterClauseContext filterClause() {
			return getRuleContext(FilterClauseContext.class,0);
		}
		public BindContext bind() {
			return getRuleContext(BindContext.class,0);
		}
		public InlineDataContext inlineData() {
			return getRuleContext(InlineDataContext.class,0);
		}
		public GraphPatternNotTriplesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_graphPatternNotTriples; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterGraphPatternNotTriples(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitGraphPatternNotTriples(this);
		}
	}

	public final GraphPatternNotTriplesContext graphPatternNotTriples() throws RecognitionException {
		GraphPatternNotTriplesContext _localctx = new GraphPatternNotTriplesContext(_ctx, getState());
		enterRule(_localctx, 108, RULE_graphPatternNotTriples);
		try {
			setState(680);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__11:
				enterOuterAlt(_localctx, 1);
				{
				setState(672);
				groupOrUnionGraphPattern();
				}
				break;
			case T__48:
				enterOuterAlt(_localctx, 2);
				{
				setState(673);
				optionalGraphPattern();
				}
				break;
			case T__52:
				enterOuterAlt(_localctx, 3);
				{
				setState(674);
				minusGraphPattern();
				}
				break;
			case T__45:
				enterOuterAlt(_localctx, 4);
				{
				setState(675);
				graphGraphPattern();
				}
				break;
			case T__49:
				enterOuterAlt(_localctx, 5);
				{
				setState(676);
				serviceGraphPattern();
				}
				break;
			case T__54:
				enterOuterAlt(_localctx, 6);
				{
				setState(677);
				filterClause();
				}
				break;
			case T__50:
				enterOuterAlt(_localctx, 7);
				{
				setState(678);
				bind();
				}
				break;
			case T__25:
				enterOuterAlt(_localctx, 8);
				{
				setState(679);
				inlineData();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OptionalGraphPatternContext extends ParserRuleContext {
		public GroupGraphPatternContext groupGraphPattern() {
			return getRuleContext(GroupGraphPatternContext.class,0);
		}
		public OptionalGraphPatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_optionalGraphPattern; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterOptionalGraphPattern(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitOptionalGraphPattern(this);
		}
	}

	public final OptionalGraphPatternContext optionalGraphPattern() throws RecognitionException {
		OptionalGraphPatternContext _localctx = new OptionalGraphPatternContext(_ctx, getState());
		enterRule(_localctx, 110, RULE_optionalGraphPattern);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(682);
			match(T__48);
			setState(683);
			groupGraphPattern();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GraphGraphPatternContext extends ParserRuleContext {
		public VarOrIriContext varOrIri() {
			return getRuleContext(VarOrIriContext.class,0);
		}
		public GroupGraphPatternContext groupGraphPattern() {
			return getRuleContext(GroupGraphPatternContext.class,0);
		}
		public GraphGraphPatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_graphGraphPattern; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterGraphGraphPattern(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitGraphGraphPattern(this);
		}
	}

	public final GraphGraphPatternContext graphGraphPattern() throws RecognitionException {
		GraphGraphPatternContext _localctx = new GraphGraphPatternContext(_ctx, getState());
		enterRule(_localctx, 112, RULE_graphGraphPattern);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(685);
			match(T__45);
			setState(686);
			varOrIri();
			setState(687);
			groupGraphPattern();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ServiceGraphPatternContext extends ParserRuleContext {
		public VarOrIriContext varOrIri() {
			return getRuleContext(VarOrIriContext.class,0);
		}
		public GroupGraphPatternContext groupGraphPattern() {
			return getRuleContext(GroupGraphPatternContext.class,0);
		}
		public ServiceGraphPatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_serviceGraphPattern; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterServiceGraphPattern(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitServiceGraphPattern(this);
		}
	}

	public final ServiceGraphPatternContext serviceGraphPattern() throws RecognitionException {
		ServiceGraphPatternContext _localctx = new ServiceGraphPatternContext(_ctx, getState());
		enterRule(_localctx, 114, RULE_serviceGraphPattern);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(689);
			match(T__49);
			setState(691);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__28) {
				{
				setState(690);
				match(T__28);
				}
			}

			setState(693);
			varOrIri();
			setState(694);
			groupGraphPattern();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BindContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public VarContext var() {
			return getRuleContext(VarContext.class,0);
		}
		public BindContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bind; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterBind(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitBind(this);
		}
	}

	public final BindContext bind() throws RecognitionException {
		BindContext _localctx = new BindContext(_ctx, getState());
		enterRule(_localctx, 116, RULE_bind);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(696);
			match(T__50);
			setState(697);
			match(T__5);
			setState(698);
			expression();
			setState(699);
			match(T__6);
			setState(700);
			var();
			setState(701);
			match(T__7);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InlineDataContext extends ParserRuleContext {
		public DataBlockContext dataBlock() {
			return getRuleContext(DataBlockContext.class,0);
		}
		public InlineDataContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_inlineData; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterInlineData(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitInlineData(this);
		}
	}

	public final InlineDataContext inlineData() throws RecognitionException {
		InlineDataContext _localctx = new InlineDataContext(_ctx, getState());
		enterRule(_localctx, 118, RULE_inlineData);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(703);
			match(T__25);
			setState(704);
			dataBlock();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DataBlockContext extends ParserRuleContext {
		public InlineDataOneVarContext inlineDataOneVar() {
			return getRuleContext(InlineDataOneVarContext.class,0);
		}
		public InlineDataFullContext inlineDataFull() {
			return getRuleContext(InlineDataFullContext.class,0);
		}
		public DataBlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dataBlock; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterDataBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitDataBlock(this);
		}
	}

	public final DataBlockContext dataBlock() throws RecognitionException {
		DataBlockContext _localctx = new DataBlockContext(_ctx, getState());
		enterRule(_localctx, 120, RULE_dataBlock);
		try {
			setState(708);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case VAR1:
			case VAR2:
				enterOuterAlt(_localctx, 1);
				{
				setState(706);
				inlineDataOneVar();
				}
				break;
			case T__5:
			case NIL:
				enterOuterAlt(_localctx, 2);
				{
				setState(707);
				inlineDataFull();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InlineDataOneVarContext extends ParserRuleContext {
		public VarContext var() {
			return getRuleContext(VarContext.class,0);
		}
		public List<DataBlockValueContext> dataBlockValue() {
			return getRuleContexts(DataBlockValueContext.class);
		}
		public DataBlockValueContext dataBlockValue(int i) {
			return getRuleContext(DataBlockValueContext.class,i);
		}
		public InlineDataOneVarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_inlineDataOneVar; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterInlineDataOneVar(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitInlineDataOneVar(this);
		}
	}

	public final InlineDataOneVarContext inlineDataOneVar() throws RecognitionException {
		InlineDataOneVarContext _localctx = new InlineDataOneVarContext(_ctx, getState());
		enterRule(_localctx, 122, RULE_inlineDataOneVar);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(710);
			var();
			setState(711);
			match(T__11);
			setState(715);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__51 || ((((_la - 140)) & ~0x3f) == 0 && ((1L << (_la - 140)) & ((1L << (T__139 - 140)) | (1L << (T__140 - 140)) | (1L << (T__141 - 140)) | (1L << (T__142 - 140)) | (1L << (IRIREF - 140)) | (1L << (PNAME_NS - 140)) | (1L << (PNAME_LN - 140)) | (1L << (INTEGER - 140)) | (1L << (DECIMAL - 140)) | (1L << (DOUBLE - 140)) | (1L << (INTEGER_POSITIVE - 140)) | (1L << (DECIMAL_POSITIVE - 140)) | (1L << (DOUBLE_POSITIVE - 140)) | (1L << (INTEGER_NEGATIVE - 140)) | (1L << (DECIMAL_NEGATIVE - 140)) | (1L << (DOUBLE_NEGATIVE - 140)) | (1L << (STRING_LITERAL1 - 140)) | (1L << (STRING_LITERAL2 - 140)) | (1L << (STRING_LITERAL_LONG1 - 140)) | (1L << (STRING_LITERAL_LONG2 - 140)))) != 0)) {
				{
				{
				setState(712);
				dataBlockValue();
				}
				}
				setState(717);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(718);
			match(T__12);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InlineDataFullContext extends ParserRuleContext {
		public List<TerminalNode> NIL() { return getTokens(SparqlParser.NIL); }
		public TerminalNode NIL(int i) {
			return getToken(SparqlParser.NIL, i);
		}
		public List<VarContext> var() {
			return getRuleContexts(VarContext.class);
		}
		public VarContext var(int i) {
			return getRuleContext(VarContext.class,i);
		}
		public List<DataBlockValueContext> dataBlockValue() {
			return getRuleContexts(DataBlockValueContext.class);
		}
		public DataBlockValueContext dataBlockValue(int i) {
			return getRuleContext(DataBlockValueContext.class,i);
		}
		public InlineDataFullContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_inlineDataFull; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterInlineDataFull(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitInlineDataFull(this);
		}
	}

	public final InlineDataFullContext inlineDataFull() throws RecognitionException {
		InlineDataFullContext _localctx = new InlineDataFullContext(_ctx, getState());
		enterRule(_localctx, 124, RULE_inlineDataFull);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(729);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case NIL:
				{
				setState(720);
				match(NIL);
				}
				break;
			case T__5:
				{
				setState(721);
				match(T__5);
				setState(725);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==VAR1 || _la==VAR2) {
					{
					{
					setState(722);
					var();
					}
					}
					setState(727);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(728);
				match(T__7);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(731);
			match(T__11);
			setState(743);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__5 || _la==NIL) {
				{
				setState(741);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__5:
					{
					setState(732);
					match(T__5);
					setState(736);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==T__51 || ((((_la - 140)) & ~0x3f) == 0 && ((1L << (_la - 140)) & ((1L << (T__139 - 140)) | (1L << (T__140 - 140)) | (1L << (T__141 - 140)) | (1L << (T__142 - 140)) | (1L << (IRIREF - 140)) | (1L << (PNAME_NS - 140)) | (1L << (PNAME_LN - 140)) | (1L << (INTEGER - 140)) | (1L << (DECIMAL - 140)) | (1L << (DOUBLE - 140)) | (1L << (INTEGER_POSITIVE - 140)) | (1L << (DECIMAL_POSITIVE - 140)) | (1L << (DOUBLE_POSITIVE - 140)) | (1L << (INTEGER_NEGATIVE - 140)) | (1L << (DECIMAL_NEGATIVE - 140)) | (1L << (DOUBLE_NEGATIVE - 140)) | (1L << (STRING_LITERAL1 - 140)) | (1L << (STRING_LITERAL2 - 140)) | (1L << (STRING_LITERAL_LONG1 - 140)) | (1L << (STRING_LITERAL_LONG2 - 140)))) != 0)) {
						{
						{
						setState(733);
						dataBlockValue();
						}
						}
						setState(738);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					setState(739);
					match(T__7);
					}
					break;
				case NIL:
					{
					setState(740);
					match(NIL);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(745);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(746);
			match(T__12);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DataBlockValueContext extends ParserRuleContext {
		public IriContext iri() {
			return getRuleContext(IriContext.class,0);
		}
		public RdfLiteralContext rdfLiteral() {
			return getRuleContext(RdfLiteralContext.class,0);
		}
		public NumericLiteralContext numericLiteral() {
			return getRuleContext(NumericLiteralContext.class,0);
		}
		public BooleanLiteralContext booleanLiteral() {
			return getRuleContext(BooleanLiteralContext.class,0);
		}
		public DataBlockValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_dataBlockValue; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterDataBlockValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitDataBlockValue(this);
		}
	}

	public final DataBlockValueContext dataBlockValue() throws RecognitionException {
		DataBlockValueContext _localctx = new DataBlockValueContext(_ctx, getState());
		enterRule(_localctx, 126, RULE_dataBlockValue);
		try {
			setState(753);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case IRIREF:
			case PNAME_NS:
			case PNAME_LN:
				enterOuterAlt(_localctx, 1);
				{
				setState(748);
				iri();
				}
				break;
			case STRING_LITERAL1:
			case STRING_LITERAL2:
			case STRING_LITERAL_LONG1:
			case STRING_LITERAL_LONG2:
				enterOuterAlt(_localctx, 2);
				{
				setState(749);
				rdfLiteral();
				}
				break;
			case INTEGER:
			case DECIMAL:
			case DOUBLE:
			case INTEGER_POSITIVE:
			case DECIMAL_POSITIVE:
			case DOUBLE_POSITIVE:
			case INTEGER_NEGATIVE:
			case DECIMAL_NEGATIVE:
			case DOUBLE_NEGATIVE:
				enterOuterAlt(_localctx, 3);
				{
				setState(750);
				numericLiteral();
				}
				break;
			case T__139:
			case T__140:
			case T__141:
			case T__142:
				enterOuterAlt(_localctx, 4);
				{
				setState(751);
				booleanLiteral();
				}
				break;
			case T__51:
				enterOuterAlt(_localctx, 5);
				{
				setState(752);
				match(T__51);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MinusGraphPatternContext extends ParserRuleContext {
		public GroupGraphPatternContext groupGraphPattern() {
			return getRuleContext(GroupGraphPatternContext.class,0);
		}
		public MinusGraphPatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_minusGraphPattern; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterMinusGraphPattern(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitMinusGraphPattern(this);
		}
	}

	public final MinusGraphPatternContext minusGraphPattern() throws RecognitionException {
		MinusGraphPatternContext _localctx = new MinusGraphPatternContext(_ctx, getState());
		enterRule(_localctx, 128, RULE_minusGraphPattern);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(755);
			match(T__52);
			setState(756);
			groupGraphPattern();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GroupOrUnionGraphPatternContext extends ParserRuleContext {
		public List<GroupGraphPatternContext> groupGraphPattern() {
			return getRuleContexts(GroupGraphPatternContext.class);
		}
		public GroupGraphPatternContext groupGraphPattern(int i) {
			return getRuleContext(GroupGraphPatternContext.class,i);
		}
		public GroupOrUnionGraphPatternContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_groupOrUnionGraphPattern; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterGroupOrUnionGraphPattern(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitGroupOrUnionGraphPattern(this);
		}
	}

	public final GroupOrUnionGraphPatternContext groupOrUnionGraphPattern() throws RecognitionException {
		GroupOrUnionGraphPatternContext _localctx = new GroupOrUnionGraphPatternContext(_ctx, getState());
		enterRule(_localctx, 130, RULE_groupOrUnionGraphPattern);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(758);
			groupGraphPattern();
			setState(763);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,77,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(759);
					match(T__53);
					setState(760);
					groupGraphPattern();
					}
					} 
				}
				setState(765);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,77,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FilterClauseContext extends ParserRuleContext {
		public ConstraintContext constraint() {
			return getRuleContext(ConstraintContext.class,0);
		}
		public FilterClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_filterClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterFilterClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitFilterClause(this);
		}
	}

	public final FilterClauseContext filterClause() throws RecognitionException {
		FilterClauseContext _localctx = new FilterClauseContext(_ctx, getState());
		enterRule(_localctx, 132, RULE_filterClause);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(766);
			match(T__54);
			setState(767);
			constraint();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstraintContext extends ParserRuleContext {
		public BrackettedExpressionContext brackettedExpression() {
			return getRuleContext(BrackettedExpressionContext.class,0);
		}
		public BuiltInCallContext builtInCall() {
			return getRuleContext(BuiltInCallContext.class,0);
		}
		public FunctionCallContext functionCall() {
			return getRuleContext(FunctionCallContext.class,0);
		}
		public ConstraintContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constraint; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterConstraint(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitConstraint(this);
		}
	}

	public final ConstraintContext constraint() throws RecognitionException {
		ConstraintContext _localctx = new ConstraintContext(_ctx, getState());
		enterRule(_localctx, 134, RULE_constraint);
		try {
			setState(772);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__5:
				enterOuterAlt(_localctx, 1);
				{
				setState(769);
				brackettedExpression();
				}
				break;
			case T__75:
			case T__77:
			case T__78:
			case T__79:
			case T__80:
			case T__81:
			case T__82:
			case T__83:
			case T__84:
			case T__85:
			case T__86:
			case T__87:
			case T__88:
			case T__89:
			case T__90:
			case T__91:
			case T__92:
			case T__93:
			case T__94:
			case T__95:
			case T__96:
			case T__97:
			case T__98:
			case T__99:
			case T__100:
			case T__101:
			case T__102:
			case T__103:
			case T__104:
			case T__105:
			case T__106:
			case T__107:
			case T__108:
			case T__109:
			case T__110:
			case T__111:
			case T__112:
			case T__113:
			case T__114:
			case T__115:
			case T__116:
			case T__117:
			case T__118:
			case T__119:
			case T__120:
			case T__121:
			case T__122:
			case T__123:
			case T__124:
			case T__125:
			case T__126:
			case T__127:
			case T__128:
			case T__129:
			case T__130:
			case T__131:
			case T__132:
			case T__133:
			case T__134:
			case T__135:
			case T__136:
				enterOuterAlt(_localctx, 2);
				{
				setState(770);
				builtInCall();
				}
				break;
			case IRIREF:
			case PNAME_NS:
			case PNAME_LN:
				enterOuterAlt(_localctx, 3);
				{
				setState(771);
				functionCall();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionCallContext extends ParserRuleContext {
		public IriContext iri() {
			return getRuleContext(IriContext.class,0);
		}
		public ArgListContext argList() {
			return getRuleContext(ArgListContext.class,0);
		}
		public FunctionCallContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionCall; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterFunctionCall(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitFunctionCall(this);
		}
	}

	public final FunctionCallContext functionCall() throws RecognitionException {
		FunctionCallContext _localctx = new FunctionCallContext(_ctx, getState());
		enterRule(_localctx, 136, RULE_functionCall);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(774);
			iri();
			setState(775);
			argList();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArgListContext extends ParserRuleContext {
		public TerminalNode NIL() { return getToken(SparqlParser.NIL, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public ArgListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_argList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterArgList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitArgList(this);
		}
	}

	public final ArgListContext argList() throws RecognitionException {
		ArgListContext _localctx = new ArgListContext(_ctx, getState());
		enterRule(_localctx, 138, RULE_argList);
		int _la;
		try {
			setState(792);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case NIL:
				enterOuterAlt(_localctx, 1);
				{
				setState(777);
				match(NIL);
				}
				break;
			case T__5:
				enterOuterAlt(_localctx, 2);
				{
				setState(778);
				match(T__5);
				setState(780);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__3) {
					{
					setState(779);
					match(T__3);
					}
				}

				setState(782);
				expression();
				setState(787);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__55) {
					{
					{
					setState(783);
					match(T__55);
					setState(784);
					expression();
					}
					}
					setState(789);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(790);
				match(T__7);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionListContext extends ParserRuleContext {
		public TerminalNode NIL() { return getToken(SparqlParser.NIL, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public ExpressionListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expressionList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterExpressionList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitExpressionList(this);
		}
	}

	public final ExpressionListContext expressionList() throws RecognitionException {
		ExpressionListContext _localctx = new ExpressionListContext(_ctx, getState());
		enterRule(_localctx, 140, RULE_expressionList);
		int _la;
		try {
			setState(806);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case NIL:
				enterOuterAlt(_localctx, 1);
				{
				setState(794);
				match(NIL);
				}
				break;
			case T__5:
				enterOuterAlt(_localctx, 2);
				{
				setState(795);
				match(T__5);
				setState(796);
				expression();
				setState(801);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__55) {
					{
					{
					setState(797);
					match(T__55);
					setState(798);
					expression();
					}
					}
					setState(803);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(804);
				match(T__7);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstructTemplateContext extends ParserRuleContext {
		public ConstructTriplesContext constructTriples() {
			return getRuleContext(ConstructTriplesContext.class,0);
		}
		public ConstructTemplateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constructTemplate; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterConstructTemplate(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitConstructTemplate(this);
		}
	}

	public final ConstructTemplateContext constructTemplate() throws RecognitionException {
		ConstructTemplateContext _localctx = new ConstructTemplateContext(_ctx, getState());
		enterRule(_localctx, 142, RULE_constructTemplate);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(808);
			match(T__11);
			setState(810);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,84,_ctx) ) {
			case 1:
				{
				setState(809);
				constructTriples();
				}
				break;
			}
			setState(812);
			match(T__12);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstructTriplesContext extends ParserRuleContext {
		public TriplesSameSubjectContext triplesSameSubject() {
			return getRuleContext(TriplesSameSubjectContext.class,0);
		}
		public ConstructTriplesContext constructTriples() {
			return getRuleContext(ConstructTriplesContext.class,0);
		}
		public ConstructTriplesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constructTriples; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterConstructTriples(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitConstructTriples(this);
		}
	}

	public final ConstructTriplesContext constructTriples() throws RecognitionException {
		ConstructTriplesContext _localctx = new ConstructTriplesContext(_ctx, getState());
		enterRule(_localctx, 144, RULE_constructTriples);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(814);
			triplesSameSubject();
			setState(819);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__47) {
				{
				setState(815);
				match(T__47);
				setState(817);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,85,_ctx) ) {
				case 1:
					{
					setState(816);
					constructTriples();
					}
					break;
				}
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TriplesSameSubjectContext extends ParserRuleContext {
		public VarOrTermContext varOrTerm() {
			return getRuleContext(VarOrTermContext.class,0);
		}
		public PropertyListNotEmptyContext propertyListNotEmpty() {
			return getRuleContext(PropertyListNotEmptyContext.class,0);
		}
		public TriplesNodeContext triplesNode() {
			return getRuleContext(TriplesNodeContext.class,0);
		}
		public PropertyListContext propertyList() {
			return getRuleContext(PropertyListContext.class,0);
		}
		public TriplesSameSubjectContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_triplesSameSubject; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterTriplesSameSubject(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitTriplesSameSubject(this);
		}
	}

	public final TriplesSameSubjectContext triplesSameSubject() throws RecognitionException {
		TriplesSameSubjectContext _localctx = new TriplesSameSubjectContext(_ctx, getState());
		enterRule(_localctx, 146, RULE_triplesSameSubject);
		try {
			setState(827);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,87,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(821);
				varOrTerm();
				setState(822);
				propertyListNotEmpty();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(824);
				triplesNode();
				setState(825);
				propertyList();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyListContext extends ParserRuleContext {
		public PropertyListNotEmptyContext propertyListNotEmpty() {
			return getRuleContext(PropertyListNotEmptyContext.class,0);
		}
		public PropertyListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_propertyList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterPropertyList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitPropertyList(this);
		}
	}

	public final PropertyListContext propertyList() throws RecognitionException {
		PropertyListContext _localctx = new PropertyListContext(_ctx, getState());
		enterRule(_localctx, 148, RULE_propertyList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(830);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__56 || _la==T__57 || ((((_la - 144)) & ~0x3f) == 0 && ((1L << (_la - 144)) & ((1L << (IRIREF - 144)) | (1L << (PNAME_NS - 144)) | (1L << (PNAME_LN - 144)) | (1L << (VAR1 - 144)) | (1L << (VAR2 - 144)))) != 0)) {
				{
				setState(829);
				propertyListNotEmpty();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyListNotEmptyContext extends ParserRuleContext {
		public List<VerbContext> verb() {
			return getRuleContexts(VerbContext.class);
		}
		public VerbContext verb(int i) {
			return getRuleContext(VerbContext.class,i);
		}
		public List<ObjectListContext> objectList() {
			return getRuleContexts(ObjectListContext.class);
		}
		public ObjectListContext objectList(int i) {
			return getRuleContext(ObjectListContext.class,i);
		}
		public PropertyListNotEmptyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_propertyListNotEmpty; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterPropertyListNotEmpty(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitPropertyListNotEmpty(this);
		}
	}

	public final PropertyListNotEmptyContext propertyListNotEmpty() throws RecognitionException {
		PropertyListNotEmptyContext _localctx = new PropertyListNotEmptyContext(_ctx, getState());
		enterRule(_localctx, 150, RULE_propertyListNotEmpty);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(832);
			verb();
			setState(833);
			objectList();
			setState(842);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__26) {
				{
				{
				setState(834);
				match(T__26);
				setState(838);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__56 || _la==T__57 || ((((_la - 144)) & ~0x3f) == 0 && ((1L << (_la - 144)) & ((1L << (IRIREF - 144)) | (1L << (PNAME_NS - 144)) | (1L << (PNAME_LN - 144)) | (1L << (VAR1 - 144)) | (1L << (VAR2 - 144)))) != 0)) {
					{
					setState(835);
					verb();
					setState(836);
					objectList();
					}
				}

				}
				}
				setState(844);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VerbContext extends ParserRuleContext {
		public VarOrIriContext varOrIri() {
			return getRuleContext(VarOrIriContext.class,0);
		}
		public VerbContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_verb; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterVerb(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitVerb(this);
		}
	}

	public final VerbContext verb() throws RecognitionException {
		VerbContext _localctx = new VerbContext(_ctx, getState());
		enterRule(_localctx, 152, RULE_verb);
		try {
			setState(848);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case IRIREF:
			case PNAME_NS:
			case PNAME_LN:
			case VAR1:
			case VAR2:
				enterOuterAlt(_localctx, 1);
				{
				setState(845);
				varOrIri();
				}
				break;
			case T__56:
				enterOuterAlt(_localctx, 2);
				{
				setState(846);
				match(T__56);
				}
				break;
			case T__57:
				enterOuterAlt(_localctx, 3);
				{
				setState(847);
				match(T__57);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ObjectListContext extends ParserRuleContext {
		public List<ObjectClauseContext> objectClause() {
			return getRuleContexts(ObjectClauseContext.class);
		}
		public ObjectClauseContext objectClause(int i) {
			return getRuleContext(ObjectClauseContext.class,i);
		}
		public ObjectListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_objectList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterObjectList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitObjectList(this);
		}
	}

	public final ObjectListContext objectList() throws RecognitionException {
		ObjectListContext _localctx = new ObjectListContext(_ctx, getState());
		enterRule(_localctx, 154, RULE_objectList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(850);
			objectClause();
			setState(855);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__55) {
				{
				{
				setState(851);
				match(T__55);
				setState(852);
				objectClause();
				}
				}
				setState(857);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ObjectClauseContext extends ParserRuleContext {
		public GraphNodeContext graphNode() {
			return getRuleContext(GraphNodeContext.class,0);
		}
		public ObjectClauseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_objectClause; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterObjectClause(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitObjectClause(this);
		}
	}

	public final ObjectClauseContext objectClause() throws RecognitionException {
		ObjectClauseContext _localctx = new ObjectClauseContext(_ctx, getState());
		enterRule(_localctx, 156, RULE_objectClause);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(858);
			graphNode();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TriplesSameSubjectPathContext extends ParserRuleContext {
		public VarOrTermContext varOrTerm() {
			return getRuleContext(VarOrTermContext.class,0);
		}
		public PropertyListPathNotEmptyContext propertyListPathNotEmpty() {
			return getRuleContext(PropertyListPathNotEmptyContext.class,0);
		}
		public TriplesNodePathContext triplesNodePath() {
			return getRuleContext(TriplesNodePathContext.class,0);
		}
		public PropertyListPathContext propertyListPath() {
			return getRuleContext(PropertyListPathContext.class,0);
		}
		public TriplesSameSubjectPathContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_triplesSameSubjectPath; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterTriplesSameSubjectPath(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitTriplesSameSubjectPath(this);
		}
	}

	public final TriplesSameSubjectPathContext triplesSameSubjectPath() throws RecognitionException {
		TriplesSameSubjectPathContext _localctx = new TriplesSameSubjectPathContext(_ctx, getState());
		enterRule(_localctx, 158, RULE_triplesSameSubjectPath);
		try {
			setState(866);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,93,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(860);
				varOrTerm();
				setState(861);
				propertyListPathNotEmpty();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(863);
				triplesNodePath();
				setState(864);
				propertyListPath();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyListPathContext extends ParserRuleContext {
		public PropertyListPathNotEmptyContext propertyListPathNotEmpty() {
			return getRuleContext(PropertyListPathNotEmptyContext.class,0);
		}
		public PropertyListPathContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_propertyListPath; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterPropertyListPath(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitPropertyListPath(this);
		}
	}

	public final PropertyListPathContext propertyListPath() throws RecognitionException {
		PropertyListPathContext _localctx = new PropertyListPathContext(_ctx, getState());
		enterRule(_localctx, 160, RULE_propertyListPath);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(869);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (((((_la - 6)) & ~0x3f) == 0 && ((1L << (_la - 6)) & ((1L << (T__5 - 6)) | (1L << (T__56 - 6)) | (1L << (T__57 - 6)) | (1L << (T__60 - 6)) | (1L << (T__63 - 6)))) != 0) || ((((_la - 144)) & ~0x3f) == 0 && ((1L << (_la - 144)) & ((1L << (IRIREF - 144)) | (1L << (PNAME_NS - 144)) | (1L << (PNAME_LN - 144)) | (1L << (VAR1 - 144)) | (1L << (VAR2 - 144)))) != 0)) {
				{
				setState(868);
				propertyListPathNotEmpty();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PropertyListPathNotEmptyContext extends ParserRuleContext {
		public ObjectListPathContext objectListPath() {
			return getRuleContext(ObjectListPathContext.class,0);
		}
		public List<VerbPathContext> verbPath() {
			return getRuleContexts(VerbPathContext.class);
		}
		public VerbPathContext verbPath(int i) {
			return getRuleContext(VerbPathContext.class,i);
		}
		public List<VerbSimpleContext> verbSimple() {
			return getRuleContexts(VerbSimpleContext.class);
		}
		public VerbSimpleContext verbSimple(int i) {
			return getRuleContext(VerbSimpleContext.class,i);
		}
		public List<ObjectListContext> objectList() {
			return getRuleContexts(ObjectListContext.class);
		}
		public ObjectListContext objectList(int i) {
			return getRuleContext(ObjectListContext.class,i);
		}
		public PropertyListPathNotEmptyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_propertyListPathNotEmpty; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterPropertyListPathNotEmpty(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitPropertyListPathNotEmpty(this);
		}
	}

	public final PropertyListPathNotEmptyContext propertyListPathNotEmpty() throws RecognitionException {
		PropertyListPathNotEmptyContext _localctx = new PropertyListPathNotEmptyContext(_ctx, getState());
		enterRule(_localctx, 162, RULE_propertyListPathNotEmpty);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(873);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__5:
			case T__56:
			case T__57:
			case T__60:
			case T__63:
			case IRIREF:
			case PNAME_NS:
			case PNAME_LN:
				{
				setState(871);
				verbPath();
				}
				break;
			case VAR1:
			case VAR2:
				{
				setState(872);
				verbSimple();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			setState(875);
			objectListPath();
			setState(887);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__26) {
				{
				{
				setState(876);
				match(T__26);
				setState(883);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (((((_la - 6)) & ~0x3f) == 0 && ((1L << (_la - 6)) & ((1L << (T__5 - 6)) | (1L << (T__56 - 6)) | (1L << (T__57 - 6)) | (1L << (T__60 - 6)) | (1L << (T__63 - 6)))) != 0) || ((((_la - 144)) & ~0x3f) == 0 && ((1L << (_la - 144)) & ((1L << (IRIREF - 144)) | (1L << (PNAME_NS - 144)) | (1L << (PNAME_LN - 144)) | (1L << (VAR1 - 144)) | (1L << (VAR2 - 144)))) != 0)) {
					{
					setState(879);
					_errHandler.sync(this);
					switch (_input.LA(1)) {
					case T__5:
					case T__56:
					case T__57:
					case T__60:
					case T__63:
					case IRIREF:
					case PNAME_NS:
					case PNAME_LN:
						{
						setState(877);
						verbPath();
						}
						break;
					case VAR1:
					case VAR2:
						{
						setState(878);
						verbSimple();
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(881);
					objectList();
					}
				}

				}
				}
				setState(889);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VerbPathContext extends ParserRuleContext {
		public PathContext path() {
			return getRuleContext(PathContext.class,0);
		}
		public VerbPathContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_verbPath; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterVerbPath(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitVerbPath(this);
		}
	}

	public final VerbPathContext verbPath() throws RecognitionException {
		VerbPathContext _localctx = new VerbPathContext(_ctx, getState());
		enterRule(_localctx, 164, RULE_verbPath);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(890);
			path();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VerbSimpleContext extends ParserRuleContext {
		public VarContext var() {
			return getRuleContext(VarContext.class,0);
		}
		public VerbSimpleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_verbSimple; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterVerbSimple(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitVerbSimple(this);
		}
	}

	public final VerbSimpleContext verbSimple() throws RecognitionException {
		VerbSimpleContext _localctx = new VerbSimpleContext(_ctx, getState());
		enterRule(_localctx, 166, RULE_verbSimple);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(892);
			var();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ObjectListPathContext extends ParserRuleContext {
		public List<ObjectPathContext> objectPath() {
			return getRuleContexts(ObjectPathContext.class);
		}
		public ObjectPathContext objectPath(int i) {
			return getRuleContext(ObjectPathContext.class,i);
		}
		public ObjectListPathContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_objectListPath; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterObjectListPath(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitObjectListPath(this);
		}
	}

	public final ObjectListPathContext objectListPath() throws RecognitionException {
		ObjectListPathContext _localctx = new ObjectListPathContext(_ctx, getState());
		enterRule(_localctx, 168, RULE_objectListPath);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(894);
			objectPath();
			setState(899);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__55) {
				{
				{
				setState(895);
				match(T__55);
				setState(896);
				objectPath();
				}
				}
				setState(901);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ObjectPathContext extends ParserRuleContext {
		public GraphNodePathContext graphNodePath() {
			return getRuleContext(GraphNodePathContext.class,0);
		}
		public ObjectPathContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_objectPath; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterObjectPath(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitObjectPath(this);
		}
	}

	public final ObjectPathContext objectPath() throws RecognitionException {
		ObjectPathContext _localctx = new ObjectPathContext(_ctx, getState());
		enterRule(_localctx, 170, RULE_objectPath);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(902);
			graphNodePath();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PathContext extends ParserRuleContext {
		public PathAlternativeContext pathAlternative() {
			return getRuleContext(PathAlternativeContext.class,0);
		}
		public PathContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_path; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterPath(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitPath(this);
		}
	}

	public final PathContext path() throws RecognitionException {
		PathContext _localctx = new PathContext(_ctx, getState());
		enterRule(_localctx, 172, RULE_path);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(904);
			pathAlternative();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PathAlternativeContext extends ParserRuleContext {
		public List<PathSequenceContext> pathSequence() {
			return getRuleContexts(PathSequenceContext.class);
		}
		public PathSequenceContext pathSequence(int i) {
			return getRuleContext(PathSequenceContext.class,i);
		}
		public PathAlternativeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pathAlternative; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterPathAlternative(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitPathAlternative(this);
		}
	}

	public final PathAlternativeContext pathAlternative() throws RecognitionException {
		PathAlternativeContext _localctx = new PathAlternativeContext(_ctx, getState());
		enterRule(_localctx, 174, RULE_pathAlternative);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(906);
			pathSequence();
			setState(911);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,100,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(907);
					match(T__58);
					setState(908);
					pathSequence();
					}
					} 
				}
				setState(913);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,100,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PathSequenceContext extends ParserRuleContext {
		public List<PathEltOrInverseContext> pathEltOrInverse() {
			return getRuleContexts(PathEltOrInverseContext.class);
		}
		public PathEltOrInverseContext pathEltOrInverse(int i) {
			return getRuleContext(PathEltOrInverseContext.class,i);
		}
		public PathSequenceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pathSequence; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterPathSequence(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitPathSequence(this);
		}
	}

	public final PathSequenceContext pathSequence() throws RecognitionException {
		PathSequenceContext _localctx = new PathSequenceContext(_ctx, getState());
		enterRule(_localctx, 176, RULE_pathSequence);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(914);
			pathEltOrInverse();
			setState(919);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,101,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(915);
					match(T__59);
					setState(916);
					pathEltOrInverse();
					}
					} 
				}
				setState(921);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,101,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PathEltContext extends ParserRuleContext {
		public PathPrimaryContext pathPrimary() {
			return getRuleContext(PathPrimaryContext.class,0);
		}
		public PathModContext pathMod() {
			return getRuleContext(PathModContext.class,0);
		}
		public PathEltContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pathElt; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterPathElt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitPathElt(this);
		}
	}

	public final PathEltContext pathElt() throws RecognitionException {
		PathEltContext _localctx = new PathEltContext(_ctx, getState());
		enterRule(_localctx, 178, RULE_pathElt);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(922);
			pathPrimary();
			setState(924);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,102,_ctx) ) {
			case 1:
				{
				setState(923);
				pathMod();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PathEltOrInverseContext extends ParserRuleContext {
		public PathEltContext pathElt() {
			return getRuleContext(PathEltContext.class,0);
		}
		public PathEltOrInverseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pathEltOrInverse; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterPathEltOrInverse(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitPathEltOrInverse(this);
		}
	}

	public final PathEltOrInverseContext pathEltOrInverse() throws RecognitionException {
		PathEltOrInverseContext _localctx = new PathEltOrInverseContext(_ctx, getState());
		enterRule(_localctx, 180, RULE_pathEltOrInverse);
		try {
			setState(929);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__5:
			case T__56:
			case T__57:
			case T__63:
			case IRIREF:
			case PNAME_NS:
			case PNAME_LN:
				enterOuterAlt(_localctx, 1);
				{
				setState(926);
				pathElt();
				}
				break;
			case T__60:
				enterOuterAlt(_localctx, 2);
				{
				setState(927);
				match(T__60);
				setState(928);
				pathElt();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PathModContext extends ParserRuleContext {
		public PathModContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pathMod; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterPathMod(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitPathMod(this);
		}
	}

	public final PathModContext pathMod() throws RecognitionException {
		PathModContext _localctx = new PathModContext(_ctx, getState());
		enterRule(_localctx, 182, RULE_pathMod);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(931);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__8) | (1L << T__61) | (1L << T__62))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PathPrimaryContext extends ParserRuleContext {
		public IriContext iri() {
			return getRuleContext(IriContext.class,0);
		}
		public PathNegatedPropertySetContext pathNegatedPropertySet() {
			return getRuleContext(PathNegatedPropertySetContext.class,0);
		}
		public PathContext path() {
			return getRuleContext(PathContext.class,0);
		}
		public PathPrimaryContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pathPrimary; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterPathPrimary(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitPathPrimary(this);
		}
	}

	public final PathPrimaryContext pathPrimary() throws RecognitionException {
		PathPrimaryContext _localctx = new PathPrimaryContext(_ctx, getState());
		enterRule(_localctx, 184, RULE_pathPrimary);
		try {
			setState(942);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case IRIREF:
			case PNAME_NS:
			case PNAME_LN:
				enterOuterAlt(_localctx, 1);
				{
				setState(933);
				iri();
				}
				break;
			case T__56:
				enterOuterAlt(_localctx, 2);
				{
				setState(934);
				match(T__56);
				}
				break;
			case T__57:
				enterOuterAlt(_localctx, 3);
				{
				setState(935);
				match(T__57);
				}
				break;
			case T__63:
				enterOuterAlt(_localctx, 4);
				{
				setState(936);
				match(T__63);
				setState(937);
				pathNegatedPropertySet();
				}
				break;
			case T__5:
				enterOuterAlt(_localctx, 5);
				{
				setState(938);
				match(T__5);
				setState(939);
				path();
				setState(940);
				match(T__7);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PathNegatedPropertySetContext extends ParserRuleContext {
		public List<PathOneInPropertySetContext> pathOneInPropertySet() {
			return getRuleContexts(PathOneInPropertySetContext.class);
		}
		public PathOneInPropertySetContext pathOneInPropertySet(int i) {
			return getRuleContext(PathOneInPropertySetContext.class,i);
		}
		public PathNegatedPropertySetContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pathNegatedPropertySet; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterPathNegatedPropertySet(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitPathNegatedPropertySet(this);
		}
	}

	public final PathNegatedPropertySetContext pathNegatedPropertySet() throws RecognitionException {
		PathNegatedPropertySetContext _localctx = new PathNegatedPropertySetContext(_ctx, getState());
		enterRule(_localctx, 186, RULE_pathNegatedPropertySet);
		int _la;
		try {
			setState(957);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__56:
			case T__57:
			case T__60:
			case IRIREF:
			case PNAME_NS:
			case PNAME_LN:
				enterOuterAlt(_localctx, 1);
				{
				setState(944);
				pathOneInPropertySet();
				}
				break;
			case T__5:
				enterOuterAlt(_localctx, 2);
				{
				setState(945);
				match(T__5);
				setState(954);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__56) | (1L << T__57) | (1L << T__60))) != 0) || ((((_la - 144)) & ~0x3f) == 0 && ((1L << (_la - 144)) & ((1L << (IRIREF - 144)) | (1L << (PNAME_NS - 144)) | (1L << (PNAME_LN - 144)))) != 0)) {
					{
					setState(946);
					pathOneInPropertySet();
					setState(951);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==T__58) {
						{
						{
						setState(947);
						match(T__58);
						setState(948);
						pathOneInPropertySet();
						}
						}
						setState(953);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
				}

				setState(956);
				match(T__7);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PathOneInPropertySetContext extends ParserRuleContext {
		public IriContext iri() {
			return getRuleContext(IriContext.class,0);
		}
		public PathOneInPropertySetContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pathOneInPropertySet; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterPathOneInPropertySet(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitPathOneInPropertySet(this);
		}
	}

	public final PathOneInPropertySetContext pathOneInPropertySet() throws RecognitionException {
		PathOneInPropertySetContext _localctx = new PathOneInPropertySetContext(_ctx, getState());
		enterRule(_localctx, 188, RULE_pathOneInPropertySet);
		try {
			setState(968);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case IRIREF:
			case PNAME_NS:
			case PNAME_LN:
				enterOuterAlt(_localctx, 1);
				{
				setState(959);
				iri();
				}
				break;
			case T__56:
				enterOuterAlt(_localctx, 2);
				{
				setState(960);
				match(T__56);
				}
				break;
			case T__57:
				enterOuterAlt(_localctx, 3);
				{
				setState(961);
				match(T__57);
				}
				break;
			case T__60:
				enterOuterAlt(_localctx, 4);
				{
				setState(962);
				match(T__60);
				setState(966);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case IRIREF:
				case PNAME_NS:
				case PNAME_LN:
					{
					setState(963);
					iri();
					}
					break;
				case T__56:
					{
					setState(964);
					match(T__56);
					}
					break;
				case T__57:
					{
					setState(965);
					match(T__57);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TriplesNodeContext extends ParserRuleContext {
		public CollectionContext collection() {
			return getRuleContext(CollectionContext.class,0);
		}
		public BlankNodePropertyListContext blankNodePropertyList() {
			return getRuleContext(BlankNodePropertyListContext.class,0);
		}
		public TriplesNodeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_triplesNode; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterTriplesNode(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitTriplesNode(this);
		}
	}

	public final TriplesNodeContext triplesNode() throws RecognitionException {
		TriplesNodeContext _localctx = new TriplesNodeContext(_ctx, getState());
		enterRule(_localctx, 190, RULE_triplesNode);
		try {
			setState(972);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__5:
				enterOuterAlt(_localctx, 1);
				{
				setState(970);
				collection();
				}
				break;
			case T__64:
				enterOuterAlt(_localctx, 2);
				{
				setState(971);
				blankNodePropertyList();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlankNodePropertyListContext extends ParserRuleContext {
		public PropertyListNotEmptyContext propertyListNotEmpty() {
			return getRuleContext(PropertyListNotEmptyContext.class,0);
		}
		public BlankNodePropertyListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_blankNodePropertyList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterBlankNodePropertyList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitBlankNodePropertyList(this);
		}
	}

	public final BlankNodePropertyListContext blankNodePropertyList() throws RecognitionException {
		BlankNodePropertyListContext _localctx = new BlankNodePropertyListContext(_ctx, getState());
		enterRule(_localctx, 192, RULE_blankNodePropertyList);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(974);
			match(T__64);
			setState(975);
			propertyListNotEmpty();
			setState(976);
			match(T__65);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TriplesNodePathContext extends ParserRuleContext {
		public CollectionPathContext collectionPath() {
			return getRuleContext(CollectionPathContext.class,0);
		}
		public BlankNodePropertyListPathContext blankNodePropertyListPath() {
			return getRuleContext(BlankNodePropertyListPathContext.class,0);
		}
		public TriplesNodePathContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_triplesNodePath; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterTriplesNodePath(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitTriplesNodePath(this);
		}
	}

	public final TriplesNodePathContext triplesNodePath() throws RecognitionException {
		TriplesNodePathContext _localctx = new TriplesNodePathContext(_ctx, getState());
		enterRule(_localctx, 194, RULE_triplesNodePath);
		try {
			setState(980);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__5:
				enterOuterAlt(_localctx, 1);
				{
				setState(978);
				collectionPath();
				}
				break;
			case T__64:
				enterOuterAlt(_localctx, 2);
				{
				setState(979);
				blankNodePropertyListPath();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlankNodePropertyListPathContext extends ParserRuleContext {
		public PropertyListPathNotEmptyContext propertyListPathNotEmpty() {
			return getRuleContext(PropertyListPathNotEmptyContext.class,0);
		}
		public BlankNodePropertyListPathContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_blankNodePropertyListPath; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterBlankNodePropertyListPath(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitBlankNodePropertyListPath(this);
		}
	}

	public final BlankNodePropertyListPathContext blankNodePropertyListPath() throws RecognitionException {
		BlankNodePropertyListPathContext _localctx = new BlankNodePropertyListPathContext(_ctx, getState());
		enterRule(_localctx, 196, RULE_blankNodePropertyListPath);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(982);
			match(T__64);
			setState(983);
			propertyListPathNotEmpty();
			setState(984);
			match(T__65);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CollectionContext extends ParserRuleContext {
		public List<GraphNodeContext> graphNode() {
			return getRuleContexts(GraphNodeContext.class);
		}
		public GraphNodeContext graphNode(int i) {
			return getRuleContext(GraphNodeContext.class,i);
		}
		public CollectionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_collection; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterCollection(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitCollection(this);
		}
	}

	public final CollectionContext collection() throws RecognitionException {
		CollectionContext _localctx = new CollectionContext(_ctx, getState());
		enterRule(_localctx, 198, RULE_collection);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(986);
			match(T__5);
			setState(988); 
			_errHandler.sync(this);
			_alt = 1;
			do {
				switch (_alt) {
				case 1:
					{
					{
					setState(987);
					graphNode();
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(990); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,112,_ctx);
			} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
			setState(992);
			match(T__7);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CollectionPathContext extends ParserRuleContext {
		public List<GraphNodePathContext> graphNodePath() {
			return getRuleContexts(GraphNodePathContext.class);
		}
		public GraphNodePathContext graphNodePath(int i) {
			return getRuleContext(GraphNodePathContext.class,i);
		}
		public CollectionPathContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_collectionPath; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterCollectionPath(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitCollectionPath(this);
		}
	}

	public final CollectionPathContext collectionPath() throws RecognitionException {
		CollectionPathContext _localctx = new CollectionPathContext(_ctx, getState());
		enterRule(_localctx, 200, RULE_collectionPath);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(994);
			match(T__5);
			setState(996); 
			_errHandler.sync(this);
			_alt = 1;
			do {
				switch (_alt) {
				case 1:
					{
					{
					setState(995);
					graphNodePath();
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(998); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,113,_ctx);
			} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
			setState(1000);
			match(T__7);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GraphNodeContext extends ParserRuleContext {
		public VarOrTermContext varOrTerm() {
			return getRuleContext(VarOrTermContext.class,0);
		}
		public TriplesNodeContext triplesNode() {
			return getRuleContext(TriplesNodeContext.class,0);
		}
		public GraphNodeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_graphNode; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterGraphNode(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitGraphNode(this);
		}
	}

	public final GraphNodeContext graphNode() throws RecognitionException {
		GraphNodeContext _localctx = new GraphNodeContext(_ctx, getState());
		enterRule(_localctx, 202, RULE_graphNode);
		try {
			setState(1004);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,114,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1002);
				varOrTerm();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1003);
				triplesNode();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GraphNodePathContext extends ParserRuleContext {
		public VarOrTermContext varOrTerm() {
			return getRuleContext(VarOrTermContext.class,0);
		}
		public TriplesNodePathContext triplesNodePath() {
			return getRuleContext(TriplesNodePathContext.class,0);
		}
		public GraphNodePathContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_graphNodePath; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterGraphNodePath(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitGraphNodePath(this);
		}
	}

	public final GraphNodePathContext graphNodePath() throws RecognitionException {
		GraphNodePathContext _localctx = new GraphNodePathContext(_ctx, getState());
		enterRule(_localctx, 204, RULE_graphNodePath);
		try {
			setState(1008);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,115,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1006);
				varOrTerm();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1007);
				triplesNodePath();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VarOrTermContext extends ParserRuleContext {
		public VarContext var() {
			return getRuleContext(VarContext.class,0);
		}
		public GraphTermContext graphTerm() {
			return getRuleContext(GraphTermContext.class,0);
		}
		public VarOrTermContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_varOrTerm; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterVarOrTerm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitVarOrTerm(this);
		}
	}

	public final VarOrTermContext varOrTerm() throws RecognitionException {
		VarOrTermContext _localctx = new VarOrTermContext(_ctx, getState());
		enterRule(_localctx, 206, RULE_varOrTerm);
		try {
			setState(1012);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,116,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1010);
				var();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1011);
				graphTerm();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VarOrIriContext extends ParserRuleContext {
		public VarContext var() {
			return getRuleContext(VarContext.class,0);
		}
		public IriContext iri() {
			return getRuleContext(IriContext.class,0);
		}
		public VarOrIriContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_varOrIri; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterVarOrIri(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitVarOrIri(this);
		}
	}

	public final VarOrIriContext varOrIri() throws RecognitionException {
		VarOrIriContext _localctx = new VarOrIriContext(_ctx, getState());
		enterRule(_localctx, 208, RULE_varOrIri);
		try {
			setState(1016);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case VAR1:
			case VAR2:
				enterOuterAlt(_localctx, 1);
				{
				setState(1014);
				var();
				}
				break;
			case IRIREF:
			case PNAME_NS:
			case PNAME_LN:
				enterOuterAlt(_localctx, 2);
				{
				setState(1015);
				iri();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VarContext extends ParserRuleContext {
		public TerminalNode VAR1() { return getToken(SparqlParser.VAR1, 0); }
		public TerminalNode VAR2() { return getToken(SparqlParser.VAR2, 0); }
		public VarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_var; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterVar(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitVar(this);
		}
	}

	public final VarContext var() throws RecognitionException {
		VarContext _localctx = new VarContext(_ctx, getState());
		enterRule(_localctx, 210, RULE_var);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1018);
			_la = _input.LA(1);
			if ( !(_la==VAR1 || _la==VAR2) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GraphTermContext extends ParserRuleContext {
		public IriContext iri() {
			return getRuleContext(IriContext.class,0);
		}
		public RdfLiteralContext rdfLiteral() {
			return getRuleContext(RdfLiteralContext.class,0);
		}
		public NumericLiteralContext numericLiteral() {
			return getRuleContext(NumericLiteralContext.class,0);
		}
		public BooleanLiteralContext booleanLiteral() {
			return getRuleContext(BooleanLiteralContext.class,0);
		}
		public BlankNodeContext blankNode() {
			return getRuleContext(BlankNodeContext.class,0);
		}
		public TerminalNode NIL() { return getToken(SparqlParser.NIL, 0); }
		public GraphTermContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_graphTerm; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterGraphTerm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitGraphTerm(this);
		}
	}

	public final GraphTermContext graphTerm() throws RecognitionException {
		GraphTermContext _localctx = new GraphTermContext(_ctx, getState());
		enterRule(_localctx, 212, RULE_graphTerm);
		try {
			setState(1027);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,118,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(1020);
				iri();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(1021);
				rdfLiteral();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(1022);
				numericLiteral();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(1023);
				booleanLiteral();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(1024);
				if (!(allowsBlankNodes)) throw new FailedPredicateException(this, "allowsBlankNodes");
				setState(1025);
				blankNode();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(1026);
				match(NIL);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public ConditionalOrExpressionContext conditionalOrExpression() {
			return getRuleContext(ConditionalOrExpressionContext.class,0);
		}
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitExpression(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		ExpressionContext _localctx = new ExpressionContext(_ctx, getState());
		enterRule(_localctx, 214, RULE_expression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1029);
			conditionalOrExpression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConditionalOrExpressionContext extends ParserRuleContext {
		public List<ConditionalAndExpressionContext> conditionalAndExpression() {
			return getRuleContexts(ConditionalAndExpressionContext.class);
		}
		public ConditionalAndExpressionContext conditionalAndExpression(int i) {
			return getRuleContext(ConditionalAndExpressionContext.class,i);
		}
		public ConditionalOrExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_conditionalOrExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterConditionalOrExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitConditionalOrExpression(this);
		}
	}

	public final ConditionalOrExpressionContext conditionalOrExpression() throws RecognitionException {
		ConditionalOrExpressionContext _localctx = new ConditionalOrExpressionContext(_ctx, getState());
		enterRule(_localctx, 216, RULE_conditionalOrExpression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1031);
			conditionalAndExpression();
			setState(1036);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__66) {
				{
				{
				setState(1032);
				match(T__66);
				setState(1033);
				conditionalAndExpression();
				}
				}
				setState(1038);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConditionalAndExpressionContext extends ParserRuleContext {
		public List<ValueLogicalContext> valueLogical() {
			return getRuleContexts(ValueLogicalContext.class);
		}
		public ValueLogicalContext valueLogical(int i) {
			return getRuleContext(ValueLogicalContext.class,i);
		}
		public ConditionalAndExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_conditionalAndExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterConditionalAndExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitConditionalAndExpression(this);
		}
	}

	public final ConditionalAndExpressionContext conditionalAndExpression() throws RecognitionException {
		ConditionalAndExpressionContext _localctx = new ConditionalAndExpressionContext(_ctx, getState());
		enterRule(_localctx, 218, RULE_conditionalAndExpression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1039);
			valueLogical();
			setState(1044);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__67) {
				{
				{
				setState(1040);
				match(T__67);
				setState(1041);
				valueLogical();
				}
				}
				setState(1046);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValueLogicalContext extends ParserRuleContext {
		public RelationalExpressionContext relationalExpression() {
			return getRuleContext(RelationalExpressionContext.class,0);
		}
		public ValueLogicalContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_valueLogical; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterValueLogical(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitValueLogical(this);
		}
	}

	public final ValueLogicalContext valueLogical() throws RecognitionException {
		ValueLogicalContext _localctx = new ValueLogicalContext(_ctx, getState());
		enterRule(_localctx, 220, RULE_valueLogical);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1047);
			relationalExpression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RelationalExpressionContext extends ParserRuleContext {
		public List<NumericExpressionContext> numericExpression() {
			return getRuleContexts(NumericExpressionContext.class);
		}
		public NumericExpressionContext numericExpression(int i) {
			return getRuleContext(NumericExpressionContext.class,i);
		}
		public ExpressionListContext expressionList() {
			return getRuleContext(ExpressionListContext.class,0);
		}
		public RelationalExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_relationalExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterRelationalExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitRelationalExpression(this);
		}
	}

	public final RelationalExpressionContext relationalExpression() throws RecognitionException {
		RelationalExpressionContext _localctx = new RelationalExpressionContext(_ctx, getState());
		enterRule(_localctx, 222, RULE_relationalExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1049);
			numericExpression();
			setState(1067);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__68:
				{
				setState(1050);
				match(T__68);
				setState(1051);
				numericExpression();
				}
				break;
			case T__69:
				{
				setState(1052);
				match(T__69);
				setState(1053);
				numericExpression();
				}
				break;
			case T__70:
				{
				setState(1054);
				match(T__70);
				setState(1055);
				numericExpression();
				}
				break;
			case T__71:
				{
				setState(1056);
				match(T__71);
				setState(1057);
				numericExpression();
				}
				break;
			case T__72:
				{
				setState(1058);
				match(T__72);
				setState(1059);
				numericExpression();
				}
				break;
			case T__73:
				{
				setState(1060);
				match(T__73);
				setState(1061);
				numericExpression();
				}
				break;
			case T__74:
				{
				setState(1062);
				match(T__74);
				setState(1063);
				expressionList();
				}
				break;
			case T__75:
				{
				setState(1064);
				match(T__75);
				setState(1065);
				match(T__74);
				setState(1066);
				expressionList();
				}
				break;
			case T__6:
			case T__7:
			case T__26:
			case T__55:
			case T__66:
			case T__67:
				break;
			default:
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NumericExpressionContext extends ParserRuleContext {
		public AdditiveExpressionContext additiveExpression() {
			return getRuleContext(AdditiveExpressionContext.class,0);
		}
		public NumericExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_numericExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterNumericExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitNumericExpression(this);
		}
	}

	public final NumericExpressionContext numericExpression() throws RecognitionException {
		NumericExpressionContext _localctx = new NumericExpressionContext(_ctx, getState());
		enterRule(_localctx, 224, RULE_numericExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1069);
			additiveExpression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AdditiveExpressionContext extends ParserRuleContext {
		public List<MultiplicativeExpressionContext> multiplicativeExpression() {
			return getRuleContexts(MultiplicativeExpressionContext.class);
		}
		public MultiplicativeExpressionContext multiplicativeExpression(int i) {
			return getRuleContext(MultiplicativeExpressionContext.class,i);
		}
		public List<NumericLiteralPositiveContext> numericLiteralPositive() {
			return getRuleContexts(NumericLiteralPositiveContext.class);
		}
		public NumericLiteralPositiveContext numericLiteralPositive(int i) {
			return getRuleContext(NumericLiteralPositiveContext.class,i);
		}
		public List<NumericLiteralNegativeContext> numericLiteralNegative() {
			return getRuleContexts(NumericLiteralNegativeContext.class);
		}
		public NumericLiteralNegativeContext numericLiteralNegative(int i) {
			return getRuleContext(NumericLiteralNegativeContext.class,i);
		}
		public List<UnaryExpressionContext> unaryExpression() {
			return getRuleContexts(UnaryExpressionContext.class);
		}
		public UnaryExpressionContext unaryExpression(int i) {
			return getRuleContext(UnaryExpressionContext.class,i);
		}
		public AdditiveExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_additiveExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterAdditiveExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitAdditiveExpression(this);
		}
	}

	public final AdditiveExpressionContext additiveExpression() throws RecognitionException {
		AdditiveExpressionContext _localctx = new AdditiveExpressionContext(_ctx, getState());
		enterRule(_localctx, 226, RULE_additiveExpression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1071);
			multiplicativeExpression();
			setState(1091);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__62 || _la==T__76 || ((((_la - 154)) & ~0x3f) == 0 && ((1L << (_la - 154)) & ((1L << (INTEGER_POSITIVE - 154)) | (1L << (DECIMAL_POSITIVE - 154)) | (1L << (DOUBLE_POSITIVE - 154)) | (1L << (INTEGER_NEGATIVE - 154)) | (1L << (DECIMAL_NEGATIVE - 154)) | (1L << (DOUBLE_NEGATIVE - 154)))) != 0)) {
				{
				setState(1089);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__62:
					{
					setState(1072);
					match(T__62);
					setState(1073);
					multiplicativeExpression();
					}
					break;
				case T__76:
					{
					setState(1074);
					match(T__76);
					setState(1075);
					multiplicativeExpression();
					}
					break;
				case INTEGER_POSITIVE:
				case DECIMAL_POSITIVE:
				case DOUBLE_POSITIVE:
				case INTEGER_NEGATIVE:
				case DECIMAL_NEGATIVE:
				case DOUBLE_NEGATIVE:
					{
					setState(1078);
					_errHandler.sync(this);
					switch (_input.LA(1)) {
					case INTEGER_POSITIVE:
					case DECIMAL_POSITIVE:
					case DOUBLE_POSITIVE:
						{
						setState(1076);
						numericLiteralPositive();
						}
						break;
					case INTEGER_NEGATIVE:
					case DECIMAL_NEGATIVE:
					case DOUBLE_NEGATIVE:
						{
						setState(1077);
						numericLiteralNegative();
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(1086);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la==T__8 || _la==T__59) {
						{
						setState(1084);
						_errHandler.sync(this);
						switch (_input.LA(1)) {
						case T__8:
							{
							{
							setState(1080);
							match(T__8);
							setState(1081);
							unaryExpression();
							}
							}
							break;
						case T__59:
							{
							{
							setState(1082);
							match(T__59);
							setState(1083);
							unaryExpression();
							}
							}
							break;
						default:
							throw new NoViableAltException(this);
						}
						}
						setState(1088);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(1093);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MultiplicativeExpressionContext extends ParserRuleContext {
		public List<UnaryExpressionContext> unaryExpression() {
			return getRuleContexts(UnaryExpressionContext.class);
		}
		public UnaryExpressionContext unaryExpression(int i) {
			return getRuleContext(UnaryExpressionContext.class,i);
		}
		public MultiplicativeExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_multiplicativeExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterMultiplicativeExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitMultiplicativeExpression(this);
		}
	}

	public final MultiplicativeExpressionContext multiplicativeExpression() throws RecognitionException {
		MultiplicativeExpressionContext _localctx = new MultiplicativeExpressionContext(_ctx, getState());
		enterRule(_localctx, 228, RULE_multiplicativeExpression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1094);
			unaryExpression();
			setState(1101);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__8 || _la==T__59) {
				{
				setState(1099);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__8:
					{
					setState(1095);
					match(T__8);
					setState(1096);
					unaryExpression();
					}
					break;
				case T__59:
					{
					setState(1097);
					match(T__59);
					setState(1098);
					unaryExpression();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				setState(1103);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UnaryExpressionContext extends ParserRuleContext {
		public PrimaryExpressionContext primaryExpression() {
			return getRuleContext(PrimaryExpressionContext.class,0);
		}
		public UnaryExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unaryExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterUnaryExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitUnaryExpression(this);
		}
	}

	public final UnaryExpressionContext unaryExpression() throws RecognitionException {
		UnaryExpressionContext _localctx = new UnaryExpressionContext(_ctx, getState());
		enterRule(_localctx, 230, RULE_unaryExpression);
		try {
			setState(1111);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__63:
				enterOuterAlt(_localctx, 1);
				{
				setState(1104);
				match(T__63);
				setState(1105);
				primaryExpression();
				}
				break;
			case T__62:
				enterOuterAlt(_localctx, 2);
				{
				setState(1106);
				match(T__62);
				setState(1107);
				primaryExpression();
				}
				break;
			case T__76:
				enterOuterAlt(_localctx, 3);
				{
				setState(1108);
				match(T__76);
				setState(1109);
				primaryExpression();
				}
				break;
			case T__5:
			case T__75:
			case T__77:
			case T__78:
			case T__79:
			case T__80:
			case T__81:
			case T__82:
			case T__83:
			case T__84:
			case T__85:
			case T__86:
			case T__87:
			case T__88:
			case T__89:
			case T__90:
			case T__91:
			case T__92:
			case T__93:
			case T__94:
			case T__95:
			case T__96:
			case T__97:
			case T__98:
			case T__99:
			case T__100:
			case T__101:
			case T__102:
			case T__103:
			case T__104:
			case T__105:
			case T__106:
			case T__107:
			case T__108:
			case T__109:
			case T__110:
			case T__111:
			case T__112:
			case T__113:
			case T__114:
			case T__115:
			case T__116:
			case T__117:
			case T__118:
			case T__119:
			case T__120:
			case T__121:
			case T__122:
			case T__123:
			case T__124:
			case T__125:
			case T__126:
			case T__127:
			case T__128:
			case T__129:
			case T__130:
			case T__131:
			case T__132:
			case T__133:
			case T__134:
			case T__135:
			case T__136:
			case T__139:
			case T__140:
			case T__141:
			case T__142:
			case IRIREF:
			case PNAME_NS:
			case PNAME_LN:
			case VAR1:
			case VAR2:
			case INTEGER:
			case DECIMAL:
			case DOUBLE:
			case INTEGER_POSITIVE:
			case DECIMAL_POSITIVE:
			case DOUBLE_POSITIVE:
			case INTEGER_NEGATIVE:
			case DECIMAL_NEGATIVE:
			case DOUBLE_NEGATIVE:
			case STRING_LITERAL1:
			case STRING_LITERAL2:
			case STRING_LITERAL_LONG1:
			case STRING_LITERAL_LONG2:
				enterOuterAlt(_localctx, 4);
				{
				setState(1110);
				primaryExpression();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrimaryExpressionContext extends ParserRuleContext {
		public BrackettedExpressionContext brackettedExpression() {
			return getRuleContext(BrackettedExpressionContext.class,0);
		}
		public BuiltInCallContext builtInCall() {
			return getRuleContext(BuiltInCallContext.class,0);
		}
		public IriOrFunctionContext iriOrFunction() {
			return getRuleContext(IriOrFunctionContext.class,0);
		}
		public RdfLiteralContext rdfLiteral() {
			return getRuleContext(RdfLiteralContext.class,0);
		}
		public NumericLiteralContext numericLiteral() {
			return getRuleContext(NumericLiteralContext.class,0);
		}
		public BooleanLiteralContext booleanLiteral() {
			return getRuleContext(BooleanLiteralContext.class,0);
		}
		public VarContext var() {
			return getRuleContext(VarContext.class,0);
		}
		public PrimaryExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_primaryExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterPrimaryExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitPrimaryExpression(this);
		}
	}

	public final PrimaryExpressionContext primaryExpression() throws RecognitionException {
		PrimaryExpressionContext _localctx = new PrimaryExpressionContext(_ctx, getState());
		enterRule(_localctx, 232, RULE_primaryExpression);
		try {
			setState(1120);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__5:
				enterOuterAlt(_localctx, 1);
				{
				setState(1113);
				brackettedExpression();
				}
				break;
			case T__75:
			case T__77:
			case T__78:
			case T__79:
			case T__80:
			case T__81:
			case T__82:
			case T__83:
			case T__84:
			case T__85:
			case T__86:
			case T__87:
			case T__88:
			case T__89:
			case T__90:
			case T__91:
			case T__92:
			case T__93:
			case T__94:
			case T__95:
			case T__96:
			case T__97:
			case T__98:
			case T__99:
			case T__100:
			case T__101:
			case T__102:
			case T__103:
			case T__104:
			case T__105:
			case T__106:
			case T__107:
			case T__108:
			case T__109:
			case T__110:
			case T__111:
			case T__112:
			case T__113:
			case T__114:
			case T__115:
			case T__116:
			case T__117:
			case T__118:
			case T__119:
			case T__120:
			case T__121:
			case T__122:
			case T__123:
			case T__124:
			case T__125:
			case T__126:
			case T__127:
			case T__128:
			case T__129:
			case T__130:
			case T__131:
			case T__132:
			case T__133:
			case T__134:
			case T__135:
			case T__136:
				enterOuterAlt(_localctx, 2);
				{
				setState(1114);
				builtInCall();
				}
				break;
			case IRIREF:
			case PNAME_NS:
			case PNAME_LN:
				enterOuterAlt(_localctx, 3);
				{
				setState(1115);
				iriOrFunction();
				}
				break;
			case STRING_LITERAL1:
			case STRING_LITERAL2:
			case STRING_LITERAL_LONG1:
			case STRING_LITERAL_LONG2:
				enterOuterAlt(_localctx, 4);
				{
				setState(1116);
				rdfLiteral();
				}
				break;
			case INTEGER:
			case DECIMAL:
			case DOUBLE:
			case INTEGER_POSITIVE:
			case DECIMAL_POSITIVE:
			case DOUBLE_POSITIVE:
			case INTEGER_NEGATIVE:
			case DECIMAL_NEGATIVE:
			case DOUBLE_NEGATIVE:
				enterOuterAlt(_localctx, 5);
				{
				setState(1117);
				numericLiteral();
				}
				break;
			case T__139:
			case T__140:
			case T__141:
			case T__142:
				enterOuterAlt(_localctx, 6);
				{
				setState(1118);
				booleanLiteral();
				}
				break;
			case VAR1:
			case VAR2:
				enterOuterAlt(_localctx, 7);
				{
				setState(1119);
				var();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BrackettedExpressionContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public BrackettedExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_brackettedExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterBrackettedExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitBrackettedExpression(this);
		}
	}

	public final BrackettedExpressionContext brackettedExpression() throws RecognitionException {
		BrackettedExpressionContext _localctx = new BrackettedExpressionContext(_ctx, getState());
		enterRule(_localctx, 234, RULE_brackettedExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1122);
			match(T__5);
			setState(1123);
			expression();
			setState(1124);
			match(T__7);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BuiltInCallContext extends ParserRuleContext {
		public AggregateContext aggregate() {
			return getRuleContext(AggregateContext.class,0);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public VarContext var() {
			return getRuleContext(VarContext.class,0);
		}
		public TerminalNode NIL() { return getToken(SparqlParser.NIL, 0); }
		public ExpressionListContext expressionList() {
			return getRuleContext(ExpressionListContext.class,0);
		}
		public SubstringExpressionContext substringExpression() {
			return getRuleContext(SubstringExpressionContext.class,0);
		}
		public StrReplaceExpressionContext strReplaceExpression() {
			return getRuleContext(StrReplaceExpressionContext.class,0);
		}
		public RegexExpressionContext regexExpression() {
			return getRuleContext(RegexExpressionContext.class,0);
		}
		public ExistsFuncContext existsFunc() {
			return getRuleContext(ExistsFuncContext.class,0);
		}
		public NotExistsFuncContext notExistsFunc() {
			return getRuleContext(NotExistsFuncContext.class,0);
		}
		public BuiltInCallContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_builtInCall; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterBuiltInCall(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitBuiltInCall(this);
		}
	}

	public final BuiltInCallContext builtInCall() throws RecognitionException {
		BuiltInCallContext _localctx = new BuiltInCallContext(_ctx, getState());
		enterRule(_localctx, 236, RULE_builtInCall);
		try {
			setState(1384);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__130:
			case T__131:
			case T__132:
			case T__133:
			case T__134:
			case T__135:
			case T__136:
				enterOuterAlt(_localctx, 1);
				{
				setState(1126);
				aggregate();
				}
				break;
			case T__77:
				enterOuterAlt(_localctx, 2);
				{
				setState(1127);
				match(T__77);
				setState(1128);
				match(T__5);
				setState(1129);
				expression();
				setState(1130);
				match(T__7);
				}
				break;
			case T__78:
				enterOuterAlt(_localctx, 3);
				{
				setState(1132);
				match(T__78);
				setState(1133);
				match(T__5);
				setState(1134);
				expression();
				setState(1135);
				match(T__7);
				}
				break;
			case T__79:
				enterOuterAlt(_localctx, 4);
				{
				setState(1137);
				match(T__79);
				setState(1138);
				match(T__5);
				setState(1139);
				expression();
				setState(1140);
				match(T__55);
				setState(1141);
				expression();
				setState(1142);
				match(T__7);
				}
				break;
			case T__80:
				enterOuterAlt(_localctx, 5);
				{
				setState(1144);
				match(T__80);
				setState(1145);
				match(T__5);
				setState(1146);
				expression();
				setState(1147);
				match(T__7);
				}
				break;
			case T__81:
				enterOuterAlt(_localctx, 6);
				{
				setState(1149);
				match(T__81);
				setState(1150);
				match(T__5);
				setState(1151);
				var();
				setState(1152);
				match(T__7);
				}
				break;
			case T__82:
				enterOuterAlt(_localctx, 7);
				{
				setState(1154);
				match(T__82);
				setState(1155);
				match(T__5);
				setState(1156);
				expression();
				setState(1157);
				match(T__7);
				}
				break;
			case T__83:
				enterOuterAlt(_localctx, 8);
				{
				setState(1159);
				match(T__83);
				setState(1160);
				match(T__5);
				setState(1161);
				expression();
				setState(1162);
				match(T__7);
				}
				break;
			case T__84:
				enterOuterAlt(_localctx, 9);
				{
				setState(1164);
				match(T__84);
				setState(1170);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__5:
					{
					setState(1165);
					match(T__5);
					setState(1166);
					expression();
					setState(1167);
					match(T__7);
					}
					break;
				case NIL:
					{
					setState(1169);
					match(NIL);
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				break;
			case T__85:
				enterOuterAlt(_localctx, 10);
				{
				setState(1172);
				match(T__85);
				setState(1173);
				match(NIL);
				}
				break;
			case T__86:
				enterOuterAlt(_localctx, 11);
				{
				setState(1174);
				match(T__86);
				setState(1175);
				match(T__5);
				setState(1176);
				expression();
				setState(1177);
				match(T__7);
				}
				break;
			case T__87:
				enterOuterAlt(_localctx, 12);
				{
				setState(1179);
				match(T__87);
				setState(1180);
				match(T__5);
				setState(1181);
				expression();
				setState(1182);
				match(T__7);
				}
				break;
			case T__88:
				enterOuterAlt(_localctx, 13);
				{
				setState(1184);
				match(T__88);
				setState(1185);
				match(T__5);
				setState(1186);
				expression();
				setState(1187);
				match(T__7);
				}
				break;
			case T__89:
				enterOuterAlt(_localctx, 14);
				{
				setState(1189);
				match(T__89);
				setState(1190);
				match(T__5);
				setState(1191);
				expression();
				setState(1192);
				match(T__7);
				}
				break;
			case T__90:
				enterOuterAlt(_localctx, 15);
				{
				setState(1194);
				match(T__90);
				setState(1195);
				expressionList();
				}
				break;
			case T__127:
				enterOuterAlt(_localctx, 16);
				{
				setState(1196);
				substringExpression();
				}
				break;
			case T__91:
				enterOuterAlt(_localctx, 17);
				{
				setState(1197);
				match(T__91);
				setState(1198);
				match(T__5);
				setState(1199);
				expression();
				setState(1200);
				match(T__7);
				}
				break;
			case T__128:
				enterOuterAlt(_localctx, 18);
				{
				setState(1202);
				strReplaceExpression();
				}
				break;
			case T__92:
				enterOuterAlt(_localctx, 19);
				{
				setState(1203);
				match(T__92);
				setState(1204);
				match(T__5);
				setState(1205);
				expression();
				setState(1206);
				match(T__7);
				}
				break;
			case T__93:
				enterOuterAlt(_localctx, 20);
				{
				setState(1208);
				match(T__93);
				setState(1209);
				match(T__5);
				setState(1210);
				expression();
				setState(1211);
				match(T__7);
				}
				break;
			case T__94:
				enterOuterAlt(_localctx, 21);
				{
				setState(1213);
				match(T__94);
				setState(1214);
				match(T__5);
				setState(1215);
				expression();
				setState(1216);
				match(T__7);
				}
				break;
			case T__95:
				enterOuterAlt(_localctx, 22);
				{
				setState(1218);
				match(T__95);
				setState(1219);
				match(T__5);
				setState(1220);
				expression();
				setState(1221);
				match(T__55);
				setState(1222);
				expression();
				setState(1223);
				match(T__7);
				}
				break;
			case T__96:
				enterOuterAlt(_localctx, 23);
				{
				setState(1225);
				match(T__96);
				setState(1226);
				match(T__5);
				setState(1227);
				expression();
				setState(1228);
				match(T__55);
				setState(1229);
				expression();
				setState(1230);
				match(T__7);
				}
				break;
			case T__97:
				enterOuterAlt(_localctx, 24);
				{
				setState(1232);
				match(T__97);
				setState(1233);
				match(T__5);
				setState(1234);
				expression();
				setState(1235);
				match(T__55);
				setState(1236);
				expression();
				setState(1237);
				match(T__7);
				}
				break;
			case T__98:
				enterOuterAlt(_localctx, 25);
				{
				setState(1239);
				match(T__98);
				setState(1240);
				match(T__5);
				setState(1241);
				expression();
				setState(1242);
				match(T__55);
				setState(1243);
				expression();
				setState(1244);
				match(T__7);
				}
				break;
			case T__99:
				enterOuterAlt(_localctx, 26);
				{
				setState(1246);
				match(T__99);
				setState(1247);
				match(T__5);
				setState(1248);
				expression();
				setState(1249);
				match(T__55);
				setState(1250);
				expression();
				setState(1251);
				match(T__7);
				}
				break;
			case T__100:
				enterOuterAlt(_localctx, 27);
				{
				setState(1253);
				match(T__100);
				setState(1254);
				match(T__5);
				setState(1255);
				expression();
				setState(1256);
				match(T__7);
				}
				break;
			case T__101:
				enterOuterAlt(_localctx, 28);
				{
				setState(1258);
				match(T__101);
				setState(1259);
				match(T__5);
				setState(1260);
				expression();
				setState(1261);
				match(T__7);
				}
				break;
			case T__102:
				enterOuterAlt(_localctx, 29);
				{
				setState(1263);
				match(T__102);
				setState(1264);
				match(T__5);
				setState(1265);
				expression();
				setState(1266);
				match(T__7);
				}
				break;
			case T__103:
				enterOuterAlt(_localctx, 30);
				{
				setState(1268);
				match(T__103);
				setState(1269);
				match(T__5);
				setState(1270);
				expression();
				setState(1271);
				match(T__7);
				}
				break;
			case T__104:
				enterOuterAlt(_localctx, 31);
				{
				setState(1273);
				match(T__104);
				setState(1274);
				match(T__5);
				setState(1275);
				expression();
				setState(1276);
				match(T__7);
				}
				break;
			case T__105:
				enterOuterAlt(_localctx, 32);
				{
				setState(1278);
				match(T__105);
				setState(1279);
				match(T__5);
				setState(1280);
				expression();
				setState(1281);
				match(T__7);
				}
				break;
			case T__106:
				enterOuterAlt(_localctx, 33);
				{
				setState(1283);
				match(T__106);
				setState(1284);
				match(T__5);
				setState(1285);
				expression();
				setState(1286);
				match(T__7);
				}
				break;
			case T__107:
				enterOuterAlt(_localctx, 34);
				{
				setState(1288);
				match(T__107);
				setState(1289);
				match(T__5);
				setState(1290);
				expression();
				setState(1291);
				match(T__7);
				}
				break;
			case T__108:
				enterOuterAlt(_localctx, 35);
				{
				setState(1293);
				match(T__108);
				setState(1294);
				match(NIL);
				}
				break;
			case T__109:
				enterOuterAlt(_localctx, 36);
				{
				setState(1295);
				match(T__109);
				setState(1296);
				match(NIL);
				}
				break;
			case T__110:
				enterOuterAlt(_localctx, 37);
				{
				setState(1297);
				match(T__110);
				setState(1298);
				match(NIL);
				}
				break;
			case T__111:
				enterOuterAlt(_localctx, 38);
				{
				setState(1299);
				match(T__111);
				setState(1300);
				match(T__5);
				setState(1301);
				expression();
				setState(1302);
				match(T__7);
				}
				break;
			case T__112:
				enterOuterAlt(_localctx, 39);
				{
				setState(1304);
				match(T__112);
				setState(1305);
				match(T__5);
				setState(1306);
				expression();
				setState(1307);
				match(T__7);
				}
				break;
			case T__113:
				enterOuterAlt(_localctx, 40);
				{
				setState(1309);
				match(T__113);
				setState(1310);
				match(T__5);
				setState(1311);
				expression();
				setState(1312);
				match(T__7);
				}
				break;
			case T__114:
				enterOuterAlt(_localctx, 41);
				{
				setState(1314);
				match(T__114);
				setState(1315);
				match(T__5);
				setState(1316);
				expression();
				setState(1317);
				match(T__7);
				}
				break;
			case T__115:
				enterOuterAlt(_localctx, 42);
				{
				setState(1319);
				match(T__115);
				setState(1320);
				match(T__5);
				setState(1321);
				expression();
				setState(1322);
				match(T__7);
				}
				break;
			case T__116:
				enterOuterAlt(_localctx, 43);
				{
				setState(1324);
				match(T__116);
				setState(1325);
				expressionList();
				}
				break;
			case T__117:
				enterOuterAlt(_localctx, 44);
				{
				setState(1326);
				match(T__117);
				setState(1327);
				match(T__5);
				setState(1328);
				expression();
				setState(1329);
				match(T__55);
				setState(1330);
				expression();
				setState(1331);
				match(T__55);
				setState(1332);
				expression();
				setState(1333);
				match(T__7);
				}
				break;
			case T__118:
				enterOuterAlt(_localctx, 45);
				{
				setState(1335);
				match(T__118);
				setState(1336);
				match(T__5);
				setState(1337);
				expression();
				setState(1338);
				match(T__55);
				setState(1339);
				expression();
				setState(1340);
				match(T__7);
				}
				break;
			case T__119:
				enterOuterAlt(_localctx, 46);
				{
				setState(1342);
				match(T__119);
				setState(1343);
				match(T__5);
				setState(1344);
				expression();
				setState(1345);
				match(T__55);
				setState(1346);
				expression();
				setState(1347);
				match(T__7);
				}
				break;
			case T__120:
				enterOuterAlt(_localctx, 47);
				{
				setState(1349);
				match(T__120);
				setState(1350);
				match(T__5);
				setState(1351);
				expression();
				setState(1352);
				match(T__55);
				setState(1353);
				expression();
				setState(1354);
				match(T__7);
				}
				break;
			case T__121:
				enterOuterAlt(_localctx, 48);
				{
				setState(1356);
				match(T__121);
				setState(1357);
				match(T__5);
				setState(1358);
				expression();
				setState(1359);
				match(T__7);
				}
				break;
			case T__122:
				enterOuterAlt(_localctx, 49);
				{
				setState(1361);
				match(T__122);
				setState(1362);
				match(T__5);
				setState(1363);
				expression();
				setState(1364);
				match(T__7);
				}
				break;
			case T__123:
				enterOuterAlt(_localctx, 50);
				{
				setState(1366);
				match(T__123);
				setState(1367);
				match(T__5);
				setState(1368);
				expression();
				setState(1369);
				match(T__7);
				}
				break;
			case T__124:
				enterOuterAlt(_localctx, 51);
				{
				setState(1371);
				match(T__124);
				setState(1372);
				match(T__5);
				setState(1373);
				expression();
				setState(1374);
				match(T__7);
				}
				break;
			case T__125:
				enterOuterAlt(_localctx, 52);
				{
				setState(1376);
				match(T__125);
				setState(1377);
				match(T__5);
				setState(1378);
				expression();
				setState(1379);
				match(T__7);
				}
				break;
			case T__126:
				enterOuterAlt(_localctx, 53);
				{
				setState(1381);
				regexExpression();
				}
				break;
			case T__129:
				enterOuterAlt(_localctx, 54);
				{
				setState(1382);
				existsFunc();
				}
				break;
			case T__75:
				enterOuterAlt(_localctx, 55);
				{
				setState(1383);
				notExistsFunc();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RegexExpressionContext extends ParserRuleContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public RegexExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_regexExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterRegexExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitRegexExpression(this);
		}
	}

	public final RegexExpressionContext regexExpression() throws RecognitionException {
		RegexExpressionContext _localctx = new RegexExpressionContext(_ctx, getState());
		enterRule(_localctx, 238, RULE_regexExpression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1386);
			match(T__126);
			setState(1387);
			match(T__5);
			setState(1388);
			expression();
			setState(1389);
			match(T__55);
			setState(1390);
			expression();
			setState(1393);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__55) {
				{
				setState(1391);
				match(T__55);
				setState(1392);
				expression();
				}
			}

			setState(1395);
			match(T__7);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SubstringExpressionContext extends ParserRuleContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public SubstringExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_substringExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterSubstringExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitSubstringExpression(this);
		}
	}

	public final SubstringExpressionContext substringExpression() throws RecognitionException {
		SubstringExpressionContext _localctx = new SubstringExpressionContext(_ctx, getState());
		enterRule(_localctx, 240, RULE_substringExpression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1397);
			match(T__127);
			setState(1398);
			match(T__5);
			setState(1399);
			expression();
			setState(1400);
			match(T__55);
			setState(1401);
			expression();
			setState(1404);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__55) {
				{
				setState(1402);
				match(T__55);
				setState(1403);
				expression();
				}
			}

			setState(1406);
			match(T__7);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StrReplaceExpressionContext extends ParserRuleContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public StrReplaceExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_strReplaceExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterStrReplaceExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitStrReplaceExpression(this);
		}
	}

	public final StrReplaceExpressionContext strReplaceExpression() throws RecognitionException {
		StrReplaceExpressionContext _localctx = new StrReplaceExpressionContext(_ctx, getState());
		enterRule(_localctx, 242, RULE_strReplaceExpression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1408);
			match(T__128);
			setState(1409);
			match(T__5);
			setState(1410);
			expression();
			setState(1411);
			match(T__55);
			setState(1412);
			expression();
			setState(1413);
			match(T__55);
			setState(1414);
			expression();
			setState(1417);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__55) {
				{
				setState(1415);
				match(T__55);
				setState(1416);
				expression();
				}
			}

			setState(1419);
			match(T__7);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExistsFuncContext extends ParserRuleContext {
		public GroupGraphPatternContext groupGraphPattern() {
			return getRuleContext(GroupGraphPatternContext.class,0);
		}
		public ExistsFuncContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_existsFunc; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterExistsFunc(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitExistsFunc(this);
		}
	}

	public final ExistsFuncContext existsFunc() throws RecognitionException {
		ExistsFuncContext _localctx = new ExistsFuncContext(_ctx, getState());
		enterRule(_localctx, 244, RULE_existsFunc);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1421);
			match(T__129);
			setState(1422);
			groupGraphPattern();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NotExistsFuncContext extends ParserRuleContext {
		public GroupGraphPatternContext groupGraphPattern() {
			return getRuleContext(GroupGraphPatternContext.class,0);
		}
		public NotExistsFuncContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_notExistsFunc; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterNotExistsFunc(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitNotExistsFunc(this);
		}
	}

	public final NotExistsFuncContext notExistsFunc() throws RecognitionException {
		NotExistsFuncContext _localctx = new NotExistsFuncContext(_ctx, getState());
		enterRule(_localctx, 246, RULE_notExistsFunc);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1424);
			match(T__75);
			setState(1425);
			match(T__129);
			setState(1426);
			groupGraphPattern();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AggregateContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public StringContext string() {
			return getRuleContext(StringContext.class,0);
		}
		public AggregateContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_aggregate; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterAggregate(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitAggregate(this);
		}
	}

	public final AggregateContext aggregate() throws RecognitionException {
		AggregateContext _localctx = new AggregateContext(_ctx, getState());
		enterRule(_localctx, 248, RULE_aggregate);
		int _la;
		try {
			setState(1492);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__130:
				enterOuterAlt(_localctx, 1);
				{
				setState(1428);
				match(T__130);
				setState(1429);
				match(T__5);
				setState(1431);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__3) {
					{
					setState(1430);
					match(T__3);
					}
				}

				setState(1435);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__8:
					{
					setState(1433);
					match(T__8);
					}
					break;
				case T__5:
				case T__62:
				case T__63:
				case T__75:
				case T__76:
				case T__77:
				case T__78:
				case T__79:
				case T__80:
				case T__81:
				case T__82:
				case T__83:
				case T__84:
				case T__85:
				case T__86:
				case T__87:
				case T__88:
				case T__89:
				case T__90:
				case T__91:
				case T__92:
				case T__93:
				case T__94:
				case T__95:
				case T__96:
				case T__97:
				case T__98:
				case T__99:
				case T__100:
				case T__101:
				case T__102:
				case T__103:
				case T__104:
				case T__105:
				case T__106:
				case T__107:
				case T__108:
				case T__109:
				case T__110:
				case T__111:
				case T__112:
				case T__113:
				case T__114:
				case T__115:
				case T__116:
				case T__117:
				case T__118:
				case T__119:
				case T__120:
				case T__121:
				case T__122:
				case T__123:
				case T__124:
				case T__125:
				case T__126:
				case T__127:
				case T__128:
				case T__129:
				case T__130:
				case T__131:
				case T__132:
				case T__133:
				case T__134:
				case T__135:
				case T__136:
				case T__139:
				case T__140:
				case T__141:
				case T__142:
				case IRIREF:
				case PNAME_NS:
				case PNAME_LN:
				case VAR1:
				case VAR2:
				case INTEGER:
				case DECIMAL:
				case DOUBLE:
				case INTEGER_POSITIVE:
				case DECIMAL_POSITIVE:
				case DOUBLE_POSITIVE:
				case INTEGER_NEGATIVE:
				case DECIMAL_NEGATIVE:
				case DOUBLE_NEGATIVE:
				case STRING_LITERAL1:
				case STRING_LITERAL2:
				case STRING_LITERAL_LONG1:
				case STRING_LITERAL_LONG2:
					{
					setState(1434);
					expression();
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(1437);
				match(T__7);
				}
				break;
			case T__131:
				enterOuterAlt(_localctx, 2);
				{
				setState(1438);
				match(T__131);
				setState(1439);
				match(T__5);
				setState(1441);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__3) {
					{
					setState(1440);
					match(T__3);
					}
				}

				setState(1443);
				expression();
				setState(1444);
				match(T__7);
				}
				break;
			case T__132:
				enterOuterAlt(_localctx, 3);
				{
				setState(1446);
				match(T__132);
				setState(1447);
				match(T__5);
				setState(1449);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__3) {
					{
					setState(1448);
					match(T__3);
					}
				}

				setState(1451);
				expression();
				setState(1452);
				match(T__7);
				}
				break;
			case T__133:
				enterOuterAlt(_localctx, 4);
				{
				setState(1454);
				match(T__133);
				setState(1455);
				match(T__5);
				setState(1457);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__3) {
					{
					setState(1456);
					match(T__3);
					}
				}

				setState(1459);
				expression();
				setState(1460);
				match(T__7);
				}
				break;
			case T__134:
				enterOuterAlt(_localctx, 5);
				{
				setState(1462);
				match(T__134);
				setState(1463);
				match(T__5);
				setState(1465);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__3) {
					{
					setState(1464);
					match(T__3);
					}
				}

				setState(1467);
				expression();
				setState(1468);
				match(T__7);
				}
				break;
			case T__135:
				enterOuterAlt(_localctx, 6);
				{
				setState(1470);
				match(T__135);
				setState(1471);
				match(T__5);
				setState(1473);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__3) {
					{
					setState(1472);
					match(T__3);
					}
				}

				setState(1475);
				expression();
				setState(1476);
				match(T__7);
				}
				break;
			case T__136:
				enterOuterAlt(_localctx, 7);
				{
				setState(1478);
				match(T__136);
				setState(1479);
				match(T__5);
				setState(1481);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__3) {
					{
					setState(1480);
					match(T__3);
					}
				}

				setState(1483);
				expression();
				setState(1488);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==T__26) {
					{
					setState(1484);
					match(T__26);
					setState(1485);
					match(T__137);
					setState(1486);
					match(T__68);
					setState(1487);
					string();
					}
				}

				setState(1490);
				match(T__7);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IriOrFunctionContext extends ParserRuleContext {
		public IriContext iri() {
			return getRuleContext(IriContext.class,0);
		}
		public ArgListContext argList() {
			return getRuleContext(ArgListContext.class,0);
		}
		public IriOrFunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_iriOrFunction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterIriOrFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitIriOrFunction(this);
		}
	}

	public final IriOrFunctionContext iriOrFunction() throws RecognitionException {
		IriOrFunctionContext _localctx = new IriOrFunctionContext(_ctx, getState());
		enterRule(_localctx, 250, RULE_iriOrFunction);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1494);
			iri();
			setState(1496);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__5 || _la==NIL) {
				{
				setState(1495);
				argList();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RdfLiteralContext extends ParserRuleContext {
		public StringContext string() {
			return getRuleContext(StringContext.class,0);
		}
		public TerminalNode LANGTAG() { return getToken(SparqlParser.LANGTAG, 0); }
		public IriContext iri() {
			return getRuleContext(IriContext.class,0);
		}
		public RdfLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rdfLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterRdfLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitRdfLiteral(this);
		}
	}

	public final RdfLiteralContext rdfLiteral() throws RecognitionException {
		RdfLiteralContext _localctx = new RdfLiteralContext(_ctx, getState());
		enterRule(_localctx, 252, RULE_rdfLiteral);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1498);
			string();
			setState(1502);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,147,_ctx) ) {
			case 1:
				{
				setState(1499);
				match(LANGTAG);
				}
				break;
			case 2:
				{
				{
				setState(1500);
				match(T__138);
				setState(1501);
				iri();
				}
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NumericLiteralContext extends ParserRuleContext {
		public NumericLiteralUnsignedContext numericLiteralUnsigned() {
			return getRuleContext(NumericLiteralUnsignedContext.class,0);
		}
		public NumericLiteralPositiveContext numericLiteralPositive() {
			return getRuleContext(NumericLiteralPositiveContext.class,0);
		}
		public NumericLiteralNegativeContext numericLiteralNegative() {
			return getRuleContext(NumericLiteralNegativeContext.class,0);
		}
		public NumericLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_numericLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterNumericLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitNumericLiteral(this);
		}
	}

	public final NumericLiteralContext numericLiteral() throws RecognitionException {
		NumericLiteralContext _localctx = new NumericLiteralContext(_ctx, getState());
		enterRule(_localctx, 254, RULE_numericLiteral);
		try {
			setState(1507);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case INTEGER:
			case DECIMAL:
			case DOUBLE:
				enterOuterAlt(_localctx, 1);
				{
				setState(1504);
				numericLiteralUnsigned();
				}
				break;
			case INTEGER_POSITIVE:
			case DECIMAL_POSITIVE:
			case DOUBLE_POSITIVE:
				enterOuterAlt(_localctx, 2);
				{
				setState(1505);
				numericLiteralPositive();
				}
				break;
			case INTEGER_NEGATIVE:
			case DECIMAL_NEGATIVE:
			case DOUBLE_NEGATIVE:
				enterOuterAlt(_localctx, 3);
				{
				setState(1506);
				numericLiteralNegative();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NumericLiteralUnsignedContext extends ParserRuleContext {
		public TerminalNode INTEGER() { return getToken(SparqlParser.INTEGER, 0); }
		public TerminalNode DECIMAL() { return getToken(SparqlParser.DECIMAL, 0); }
		public TerminalNode DOUBLE() { return getToken(SparqlParser.DOUBLE, 0); }
		public NumericLiteralUnsignedContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_numericLiteralUnsigned; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterNumericLiteralUnsigned(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitNumericLiteralUnsigned(this);
		}
	}

	public final NumericLiteralUnsignedContext numericLiteralUnsigned() throws RecognitionException {
		NumericLiteralUnsignedContext _localctx = new NumericLiteralUnsignedContext(_ctx, getState());
		enterRule(_localctx, 256, RULE_numericLiteralUnsigned);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1509);
			_la = _input.LA(1);
			if ( !(((((_la - 151)) & ~0x3f) == 0 && ((1L << (_la - 151)) & ((1L << (INTEGER - 151)) | (1L << (DECIMAL - 151)) | (1L << (DOUBLE - 151)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NumericLiteralPositiveContext extends ParserRuleContext {
		public TerminalNode INTEGER_POSITIVE() { return getToken(SparqlParser.INTEGER_POSITIVE, 0); }
		public TerminalNode DECIMAL_POSITIVE() { return getToken(SparqlParser.DECIMAL_POSITIVE, 0); }
		public TerminalNode DOUBLE_POSITIVE() { return getToken(SparqlParser.DOUBLE_POSITIVE, 0); }
		public NumericLiteralPositiveContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_numericLiteralPositive; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterNumericLiteralPositive(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitNumericLiteralPositive(this);
		}
	}

	public final NumericLiteralPositiveContext numericLiteralPositive() throws RecognitionException {
		NumericLiteralPositiveContext _localctx = new NumericLiteralPositiveContext(_ctx, getState());
		enterRule(_localctx, 258, RULE_numericLiteralPositive);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1511);
			_la = _input.LA(1);
			if ( !(((((_la - 154)) & ~0x3f) == 0 && ((1L << (_la - 154)) & ((1L << (INTEGER_POSITIVE - 154)) | (1L << (DECIMAL_POSITIVE - 154)) | (1L << (DOUBLE_POSITIVE - 154)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NumericLiteralNegativeContext extends ParserRuleContext {
		public TerminalNode INTEGER_NEGATIVE() { return getToken(SparqlParser.INTEGER_NEGATIVE, 0); }
		public TerminalNode DECIMAL_NEGATIVE() { return getToken(SparqlParser.DECIMAL_NEGATIVE, 0); }
		public TerminalNode DOUBLE_NEGATIVE() { return getToken(SparqlParser.DOUBLE_NEGATIVE, 0); }
		public NumericLiteralNegativeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_numericLiteralNegative; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterNumericLiteralNegative(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitNumericLiteralNegative(this);
		}
	}

	public final NumericLiteralNegativeContext numericLiteralNegative() throws RecognitionException {
		NumericLiteralNegativeContext _localctx = new NumericLiteralNegativeContext(_ctx, getState());
		enterRule(_localctx, 260, RULE_numericLiteralNegative);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1513);
			_la = _input.LA(1);
			if ( !(((((_la - 157)) & ~0x3f) == 0 && ((1L << (_la - 157)) & ((1L << (INTEGER_NEGATIVE - 157)) | (1L << (DECIMAL_NEGATIVE - 157)) | (1L << (DOUBLE_NEGATIVE - 157)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BooleanLiteralContext extends ParserRuleContext {
		public BooleanLiteralContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_booleanLiteral; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterBooleanLiteral(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitBooleanLiteral(this);
		}
	}

	public final BooleanLiteralContext booleanLiteral() throws RecognitionException {
		BooleanLiteralContext _localctx = new BooleanLiteralContext(_ctx, getState());
		enterRule(_localctx, 262, RULE_booleanLiteral);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1515);
			_la = _input.LA(1);
			if ( !(((((_la - 140)) & ~0x3f) == 0 && ((1L << (_la - 140)) & ((1L << (T__139 - 140)) | (1L << (T__140 - 140)) | (1L << (T__141 - 140)) | (1L << (T__142 - 140)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StringContext extends ParserRuleContext {
		public TerminalNode STRING_LITERAL1() { return getToken(SparqlParser.STRING_LITERAL1, 0); }
		public TerminalNode STRING_LITERAL2() { return getToken(SparqlParser.STRING_LITERAL2, 0); }
		public TerminalNode STRING_LITERAL_LONG1() { return getToken(SparqlParser.STRING_LITERAL_LONG1, 0); }
		public TerminalNode STRING_LITERAL_LONG2() { return getToken(SparqlParser.STRING_LITERAL_LONG2, 0); }
		public StringContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_string; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterString(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitString(this);
		}
	}

	public final StringContext string() throws RecognitionException {
		StringContext _localctx = new StringContext(_ctx, getState());
		enterRule(_localctx, 264, RULE_string);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1517);
			_la = _input.LA(1);
			if ( !(((((_la - 161)) & ~0x3f) == 0 && ((1L << (_la - 161)) & ((1L << (STRING_LITERAL1 - 161)) | (1L << (STRING_LITERAL2 - 161)) | (1L << (STRING_LITERAL_LONG1 - 161)) | (1L << (STRING_LITERAL_LONG2 - 161)))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IriContext extends ParserRuleContext {
		public TerminalNode IRIREF() { return getToken(SparqlParser.IRIREF, 0); }
		public PrefixedNameContext prefixedName() {
			return getRuleContext(PrefixedNameContext.class,0);
		}
		public IriContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_iri; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterIri(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitIri(this);
		}
	}

	public final IriContext iri() throws RecognitionException {
		IriContext _localctx = new IriContext(_ctx, getState());
		enterRule(_localctx, 266, RULE_iri);
		try {
			setState(1521);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case IRIREF:
				enterOuterAlt(_localctx, 1);
				{
				setState(1519);
				match(IRIREF);
				}
				break;
			case PNAME_NS:
			case PNAME_LN:
				enterOuterAlt(_localctx, 2);
				{
				setState(1520);
				prefixedName();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PrefixedNameContext extends ParserRuleContext {
		public TerminalNode PNAME_LN() { return getToken(SparqlParser.PNAME_LN, 0); }
		public TerminalNode PNAME_NS() { return getToken(SparqlParser.PNAME_NS, 0); }
		public PrefixedNameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prefixedName; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterPrefixedName(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitPrefixedName(this);
		}
	}

	public final PrefixedNameContext prefixedName() throws RecognitionException {
		PrefixedNameContext _localctx = new PrefixedNameContext(_ctx, getState());
		enterRule(_localctx, 268, RULE_prefixedName);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1523);
			_la = _input.LA(1);
			if ( !(_la==PNAME_NS || _la==PNAME_LN) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlankNodeContext extends ParserRuleContext {
		public TerminalNode BLANK_NODE_LABEL() { return getToken(SparqlParser.BLANK_NODE_LABEL, 0); }
		public TerminalNode ANON() { return getToken(SparqlParser.ANON, 0); }
		public BlankNodeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_blankNode; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).enterBlankNode(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SparqlListener ) ((SparqlListener)listener).exitBlankNode(this);
		}
	}

	public final BlankNodeContext blankNode() throws RecognitionException {
		BlankNodeContext _localctx = new BlankNodeContext(_ctx, getState());
		enterRule(_localctx, 270, RULE_blankNode);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(1525);
			_la = _input.LA(1);
			if ( !(_la==BLANK_NODE_LABEL || _la==ANON) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 106:
			return graphTerm_sempred((GraphTermContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean graphTerm_sempred(GraphTermContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return allowsBlankNodes;
		}
		return true;
	}

	public static final String _serializedATN =
		"\u0004\u0001\u00b3\u05f8\u0002\u0000\u0007\u0000\u0002\u0001\u0007\u0001"+
		"\u0002\u0002\u0007\u0002\u0002\u0003\u0007\u0003\u0002\u0004\u0007\u0004"+
		"\u0002\u0005\u0007\u0005\u0002\u0006\u0007\u0006\u0002\u0007\u0007\u0007"+
		"\u0002\b\u0007\b\u0002\t\u0007\t\u0002\n\u0007\n\u0002\u000b\u0007\u000b"+
		"\u0002\f\u0007\f\u0002\r\u0007\r\u0002\u000e\u0007\u000e\u0002\u000f\u0007"+
		"\u000f\u0002\u0010\u0007\u0010\u0002\u0011\u0007\u0011\u0002\u0012\u0007"+
		"\u0012\u0002\u0013\u0007\u0013\u0002\u0014\u0007\u0014\u0002\u0015\u0007"+
		"\u0015\u0002\u0016\u0007\u0016\u0002\u0017\u0007\u0017\u0002\u0018\u0007"+
		"\u0018\u0002\u0019\u0007\u0019\u0002\u001a\u0007\u001a\u0002\u001b\u0007"+
		"\u001b\u0002\u001c\u0007\u001c\u0002\u001d\u0007\u001d\u0002\u001e\u0007"+
		"\u001e\u0002\u001f\u0007\u001f\u0002 \u0007 \u0002!\u0007!\u0002\"\u0007"+
		"\"\u0002#\u0007#\u0002$\u0007$\u0002%\u0007%\u0002&\u0007&\u0002\'\u0007"+
		"\'\u0002(\u0007(\u0002)\u0007)\u0002*\u0007*\u0002+\u0007+\u0002,\u0007"+
		",\u0002-\u0007-\u0002.\u0007.\u0002/\u0007/\u00020\u00070\u00021\u0007"+
		"1\u00022\u00072\u00023\u00073\u00024\u00074\u00025\u00075\u00026\u0007"+
		"6\u00027\u00077\u00028\u00078\u00029\u00079\u0002:\u0007:\u0002;\u0007"+
		";\u0002<\u0007<\u0002=\u0007=\u0002>\u0007>\u0002?\u0007?\u0002@\u0007"+
		"@\u0002A\u0007A\u0002B\u0007B\u0002C\u0007C\u0002D\u0007D\u0002E\u0007"+
		"E\u0002F\u0007F\u0002G\u0007G\u0002H\u0007H\u0002I\u0007I\u0002J\u0007"+
		"J\u0002K\u0007K\u0002L\u0007L\u0002M\u0007M\u0002N\u0007N\u0002O\u0007"+
		"O\u0002P\u0007P\u0002Q\u0007Q\u0002R\u0007R\u0002S\u0007S\u0002T\u0007"+
		"T\u0002U\u0007U\u0002V\u0007V\u0002W\u0007W\u0002X\u0007X\u0002Y\u0007"+
		"Y\u0002Z\u0007Z\u0002[\u0007[\u0002\\\u0007\\\u0002]\u0007]\u0002^\u0007"+
		"^\u0002_\u0007_\u0002`\u0007`\u0002a\u0007a\u0002b\u0007b\u0002c\u0007"+
		"c\u0002d\u0007d\u0002e\u0007e\u0002f\u0007f\u0002g\u0007g\u0002h\u0007"+
		"h\u0002i\u0007i\u0002j\u0007j\u0002k\u0007k\u0002l\u0007l\u0002m\u0007"+
		"m\u0002n\u0007n\u0002o\u0007o\u0002p\u0007p\u0002q\u0007q\u0002r\u0007"+
		"r\u0002s\u0007s\u0002t\u0007t\u0002u\u0007u\u0002v\u0007v\u0002w\u0007"+
		"w\u0002x\u0007x\u0002y\u0007y\u0002z\u0007z\u0002{\u0007{\u0002|\u0007"+
		"|\u0002}\u0007}\u0002~\u0007~\u0002\u007f\u0007\u007f\u0002\u0080\u0007"+
		"\u0080\u0002\u0081\u0007\u0081\u0002\u0082\u0007\u0082\u0002\u0083\u0007"+
		"\u0083\u0002\u0084\u0007\u0084\u0002\u0085\u0007\u0085\u0002\u0086\u0007"+
		"\u0086\u0002\u0087\u0007\u0087\u0001\u0000\u0001\u0000\u0003\u0000\u0113"+
		"\b\u0000\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0001\u0003"+
		"\u0001\u011a\b\u0001\u0001\u0001\u0001\u0001\u0001\u0002\u0001\u0002\u0005"+
		"\u0002\u0120\b\u0002\n\u0002\f\u0002\u0123\t\u0002\u0001\u0003\u0001\u0003"+
		"\u0001\u0003\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0004\u0001\u0005"+
		"\u0001\u0005\u0005\u0005\u012e\b\u0005\n\u0005\f\u0005\u0131\t\u0005\u0001"+
		"\u0005\u0001\u0005\u0001\u0005\u0001\u0006\u0001\u0006\u0001\u0006\u0001"+
		"\u0006\u0001\u0006\u0001\u0007\u0001\u0007\u0003\u0007\u013d\b\u0007\u0001"+
		"\u0007\u0001\u0007\u0001\u0007\u0001\u0007\u0001\u0007\u0001\u0007\u0001"+
		"\u0007\u0004\u0007\u0146\b\u0007\u000b\u0007\f\u0007\u0147\u0001\u0007"+
		"\u0003\u0007\u014b\b\u0007\u0001\b\u0001\b\u0001\b\u0005\b\u0150\b\b\n"+
		"\b\f\b\u0153\t\b\u0001\b\u0001\b\u0001\b\u0001\b\u0005\b\u0159\b\b\n\b"+
		"\f\b\u015c\t\b\u0001\b\u0001\b\u0001\b\u0003\b\u0161\b\b\u0001\b\u0001"+
		"\b\u0003\b\u0165\b\b\u0001\t\u0001\t\u0004\t\u0169\b\t\u000b\t\f\t\u016a"+
		"\u0001\t\u0003\t\u016e\b\t\u0001\t\u0005\t\u0171\b\t\n\t\f\t\u0174\t\t"+
		"\u0001\t\u0003\t\u0177\b\t\u0001\t\u0001\t\u0001\n\u0001\n\u0005\n\u017d"+
		"\b\n\n\n\f\n\u0180\t\n\u0001\n\u0001\n\u0001\n\u0001\u000b\u0001\u000b"+
		"\u0001\u000b\u0003\u000b\u0188\b\u000b\u0001\f\u0001\f\u0001\r\u0001\r"+
		"\u0001\r\u0001\u000e\u0001\u000e\u0001\u000f\u0003\u000f\u0192\b\u000f"+
		"\u0001\u000f\u0001\u000f\u0001\u0010\u0003\u0010\u0197\b\u0010\u0001\u0010"+
		"\u0003\u0010\u019a\b\u0010\u0001\u0010\u0003\u0010\u019d\b\u0010\u0001"+
		"\u0010\u0003\u0010\u01a0\b\u0010\u0001\u0011\u0001\u0011\u0001\u0011\u0004"+
		"\u0011\u01a5\b\u0011\u000b\u0011\f\u0011\u01a6\u0001\u0012\u0001\u0012"+
		"\u0001\u0012\u0001\u0012\u0001\u0012\u0001\u0012\u0003\u0012\u01af\b\u0012"+
		"\u0001\u0012\u0001\u0012\u0001\u0012\u0003\u0012\u01b4\b\u0012\u0001\u0013"+
		"\u0001\u0013\u0004\u0013\u01b8\b\u0013\u000b\u0013\f\u0013\u01b9\u0001"+
		"\u0014\u0001\u0014\u0001\u0015\u0001\u0015\u0001\u0015\u0004\u0015\u01c1"+
		"\b\u0015\u000b\u0015\f\u0015\u01c2\u0001\u0016\u0001\u0016\u0001\u0016"+
		"\u0001\u0016\u0003\u0016\u01c9\b\u0016\u0003\u0016\u01cb\b\u0016\u0001"+
		"\u0017\u0001\u0017\u0003\u0017\u01cf\b\u0017\u0001\u0017\u0001\u0017\u0003"+
		"\u0017\u01d3\b\u0017\u0003\u0017\u01d5\b\u0017\u0001\u0018\u0001\u0018"+
		"\u0001\u0018\u0001\u0019\u0001\u0019\u0001\u0019\u0001\u001a\u0001\u001a"+
		"\u0003\u001a\u01df\b\u001a\u0001\u001b\u0001\u001b\u0001\u001b\u0001\u001b"+
		"\u0003\u001b\u01e5\b\u001b\u0003\u001b\u01e7\b\u001b\u0001\u001c\u0001"+
		"\u001c\u0001\u001c\u0001\u001c\u0001\u001c\u0001\u001c\u0001\u001c\u0001"+
		"\u001c\u0001\u001c\u0001\u001c\u0001\u001c\u0003\u001c\u01f4\b\u001c\u0001"+
		"\u001d\u0001\u001d\u0003\u001d\u01f8\b\u001d\u0001\u001d\u0001\u001d\u0001"+
		"\u001d\u0003\u001d\u01fd\b\u001d\u0001\u001e\u0001\u001e\u0003\u001e\u0201"+
		"\b\u001e\u0001\u001e\u0001\u001e\u0001\u001f\u0001\u001f\u0003\u001f\u0207"+
		"\b\u001f\u0001\u001f\u0001\u001f\u0001 \u0001 \u0003 \u020d\b \u0001 "+
		"\u0001 \u0001!\u0001!\u0003!\u0213\b!\u0001!\u0001!\u0001!\u0001!\u0001"+
		"\"\u0001\"\u0003\"\u021b\b\"\u0001\"\u0001\"\u0001\"\u0001\"\u0001#\u0001"+
		"#\u0003#\u0223\b#\u0001#\u0001#\u0001#\u0001#\u0001$\u0001$\u0001$\u0001"+
		"%\u0001%\u0001%\u0001&\u0001&\u0001&\u0001\'\u0001\'\u0003\'\u0234\b\'"+
		"\u0001\'\u0001\'\u0003\'\u0238\b\'\u0001\'\u0003\'\u023b\b\'\u0001\'\u0005"+
		"\'\u023e\b\'\n\'\f\'\u0241\t\'\u0001\'\u0001\'\u0001\'\u0001(\u0001(\u0001"+
		"(\u0001)\u0001)\u0001)\u0001*\u0001*\u0001*\u0001*\u0003*\u0250\b*\u0001"+
		"+\u0001+\u0003+\u0254\b+\u0001+\u0003+\u0257\b+\u0001,\u0001,\u0001,\u0001"+
		"-\u0001-\u0001-\u0001-\u0003-\u0260\b-\u0001.\u0001.\u0001.\u0001.\u0001"+
		"/\u0001/\u0001/\u0001/\u00010\u00030\u026b\b0\u00010\u00010\u00030\u026f"+
		"\b0\u00010\u00030\u0272\b0\u00050\u0274\b0\n0\f0\u0277\t0\u00011\u0001"+
		"1\u00011\u00011\u00012\u00012\u00012\u00032\u0280\b2\u00032\u0282\b2\u0001"+
		"3\u00013\u00013\u00033\u0287\b3\u00013\u00013\u00014\u00034\u028c\b4\u0001"+
		"4\u00014\u00034\u0290\b4\u00014\u00034\u0293\b4\u00054\u0295\b4\n4\f4"+
		"\u0298\t4\u00015\u00015\u00015\u00035\u029d\b5\u00035\u029f\b5\u00016"+
		"\u00016\u00016\u00016\u00016\u00016\u00016\u00016\u00036\u02a9\b6\u0001"+
		"7\u00017\u00017\u00018\u00018\u00018\u00018\u00019\u00019\u00039\u02b4"+
		"\b9\u00019\u00019\u00019\u0001:\u0001:\u0001:\u0001:\u0001:\u0001:\u0001"+
		":\u0001;\u0001;\u0001;\u0001<\u0001<\u0003<\u02c5\b<\u0001=\u0001=\u0001"+
		"=\u0005=\u02ca\b=\n=\f=\u02cd\t=\u0001=\u0001=\u0001>\u0001>\u0001>\u0005"+
		">\u02d4\b>\n>\f>\u02d7\t>\u0001>\u0003>\u02da\b>\u0001>\u0001>\u0001>"+
		"\u0005>\u02df\b>\n>\f>\u02e2\t>\u0001>\u0001>\u0005>\u02e6\b>\n>\f>\u02e9"+
		"\t>\u0001>\u0001>\u0001?\u0001?\u0001?\u0001?\u0001?\u0003?\u02f2\b?\u0001"+
		"@\u0001@\u0001@\u0001A\u0001A\u0001A\u0005A\u02fa\bA\nA\fA\u02fd\tA\u0001"+
		"B\u0001B\u0001B\u0001C\u0001C\u0001C\u0003C\u0305\bC\u0001D\u0001D\u0001"+
		"D\u0001E\u0001E\u0001E\u0003E\u030d\bE\u0001E\u0001E\u0001E\u0005E\u0312"+
		"\bE\nE\fE\u0315\tE\u0001E\u0001E\u0003E\u0319\bE\u0001F\u0001F\u0001F"+
		"\u0001F\u0001F\u0005F\u0320\bF\nF\fF\u0323\tF\u0001F\u0001F\u0003F\u0327"+
		"\bF\u0001G\u0001G\u0003G\u032b\bG\u0001G\u0001G\u0001H\u0001H\u0001H\u0003"+
		"H\u0332\bH\u0003H\u0334\bH\u0001I\u0001I\u0001I\u0001I\u0001I\u0001I\u0003"+
		"I\u033c\bI\u0001J\u0003J\u033f\bJ\u0001K\u0001K\u0001K\u0001K\u0001K\u0001"+
		"K\u0003K\u0347\bK\u0005K\u0349\bK\nK\fK\u034c\tK\u0001L\u0001L\u0001L"+
		"\u0003L\u0351\bL\u0001M\u0001M\u0001M\u0005M\u0356\bM\nM\fM\u0359\tM\u0001"+
		"N\u0001N\u0001O\u0001O\u0001O\u0001O\u0001O\u0001O\u0003O\u0363\bO\u0001"+
		"P\u0003P\u0366\bP\u0001Q\u0001Q\u0003Q\u036a\bQ\u0001Q\u0001Q\u0001Q\u0001"+
		"Q\u0003Q\u0370\bQ\u0001Q\u0001Q\u0003Q\u0374\bQ\u0005Q\u0376\bQ\nQ\fQ"+
		"\u0379\tQ\u0001R\u0001R\u0001S\u0001S\u0001T\u0001T\u0001T\u0005T\u0382"+
		"\bT\nT\fT\u0385\tT\u0001U\u0001U\u0001V\u0001V\u0001W\u0001W\u0001W\u0005"+
		"W\u038e\bW\nW\fW\u0391\tW\u0001X\u0001X\u0001X\u0005X\u0396\bX\nX\fX\u0399"+
		"\tX\u0001Y\u0001Y\u0003Y\u039d\bY\u0001Z\u0001Z\u0001Z\u0003Z\u03a2\b"+
		"Z\u0001[\u0001[\u0001\\\u0001\\\u0001\\\u0001\\\u0001\\\u0001\\\u0001"+
		"\\\u0001\\\u0001\\\u0003\\\u03af\b\\\u0001]\u0001]\u0001]\u0001]\u0001"+
		"]\u0005]\u03b6\b]\n]\f]\u03b9\t]\u0003]\u03bb\b]\u0001]\u0003]\u03be\b"+
		"]\u0001^\u0001^\u0001^\u0001^\u0001^\u0001^\u0001^\u0003^\u03c7\b^\u0003"+
		"^\u03c9\b^\u0001_\u0001_\u0003_\u03cd\b_\u0001`\u0001`\u0001`\u0001`\u0001"+
		"a\u0001a\u0003a\u03d5\ba\u0001b\u0001b\u0001b\u0001b\u0001c\u0001c\u0004"+
		"c\u03dd\bc\u000bc\fc\u03de\u0001c\u0001c\u0001d\u0001d\u0004d\u03e5\b"+
		"d\u000bd\fd\u03e6\u0001d\u0001d\u0001e\u0001e\u0003e\u03ed\be\u0001f\u0001"+
		"f\u0003f\u03f1\bf\u0001g\u0001g\u0003g\u03f5\bg\u0001h\u0001h\u0003h\u03f9"+
		"\bh\u0001i\u0001i\u0001j\u0001j\u0001j\u0001j\u0001j\u0001j\u0001j\u0003"+
		"j\u0404\bj\u0001k\u0001k\u0001l\u0001l\u0001l\u0005l\u040b\bl\nl\fl\u040e"+
		"\tl\u0001m\u0001m\u0001m\u0005m\u0413\bm\nm\fm\u0416\tm\u0001n\u0001n"+
		"\u0001o\u0001o\u0001o\u0001o\u0001o\u0001o\u0001o\u0001o\u0001o\u0001"+
		"o\u0001o\u0001o\u0001o\u0001o\u0001o\u0001o\u0001o\u0001o\u0003o\u042c"+
		"\bo\u0001p\u0001p\u0001q\u0001q\u0001q\u0001q\u0001q\u0001q\u0001q\u0003"+
		"q\u0437\bq\u0001q\u0001q\u0001q\u0001q\u0005q\u043d\bq\nq\fq\u0440\tq"+
		"\u0005q\u0442\bq\nq\fq\u0445\tq\u0001r\u0001r\u0001r\u0001r\u0001r\u0005"+
		"r\u044c\br\nr\fr\u044f\tr\u0001s\u0001s\u0001s\u0001s\u0001s\u0001s\u0001"+
		"s\u0003s\u0458\bs\u0001t\u0001t\u0001t\u0001t\u0001t\u0001t\u0001t\u0003"+
		"t\u0461\bt\u0001u\u0001u\u0001u\u0001u\u0001v\u0001v\u0001v\u0001v\u0001"+
		"v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001"+
		"v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001"+
		"v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001"+
		"v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0003"+
		"v\u0493\bv\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001"+
		"v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001"+
		"v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001"+
		"v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001"+
		"v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001"+
		"v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001"+
		"v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001"+
		"v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001"+
		"v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001"+
		"v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001"+
		"v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001"+
		"v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001"+
		"v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001"+
		"v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001"+
		"v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001"+
		"v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001"+
		"v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001"+
		"v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001"+
		"v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001"+
		"v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001"+
		"v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001v\u0001"+
		"v\u0001v\u0001v\u0001v\u0003v\u0569\bv\u0001w\u0001w\u0001w\u0001w\u0001"+
		"w\u0001w\u0001w\u0003w\u0572\bw\u0001w\u0001w\u0001x\u0001x\u0001x\u0001"+
		"x\u0001x\u0001x\u0001x\u0003x\u057d\bx\u0001x\u0001x\u0001y\u0001y\u0001"+
		"y\u0001y\u0001y\u0001y\u0001y\u0001y\u0001y\u0003y\u058a\by\u0001y\u0001"+
		"y\u0001z\u0001z\u0001z\u0001{\u0001{\u0001{\u0001{\u0001|\u0001|\u0001"+
		"|\u0003|\u0598\b|\u0001|\u0001|\u0003|\u059c\b|\u0001|\u0001|\u0001|\u0001"+
		"|\u0003|\u05a2\b|\u0001|\u0001|\u0001|\u0001|\u0001|\u0001|\u0003|\u05aa"+
		"\b|\u0001|\u0001|\u0001|\u0001|\u0001|\u0001|\u0003|\u05b2\b|\u0001|\u0001"+
		"|\u0001|\u0001|\u0001|\u0001|\u0003|\u05ba\b|\u0001|\u0001|\u0001|\u0001"+
		"|\u0001|\u0001|\u0003|\u05c2\b|\u0001|\u0001|\u0001|\u0001|\u0001|\u0001"+
		"|\u0003|\u05ca\b|\u0001|\u0001|\u0001|\u0001|\u0001|\u0003|\u05d1\b|\u0001"+
		"|\u0001|\u0003|\u05d5\b|\u0001}\u0001}\u0003}\u05d9\b}\u0001~\u0001~\u0001"+
		"~\u0001~\u0003~\u05df\b~\u0001\u007f\u0001\u007f\u0001\u007f\u0003\u007f"+
		"\u05e4\b\u007f\u0001\u0080\u0001\u0080\u0001\u0081\u0001\u0081\u0001\u0082"+
		"\u0001\u0082\u0001\u0083\u0001\u0083\u0001\u0084\u0001\u0084\u0001\u0085"+
		"\u0001\u0085\u0003\u0085\u05f2\b\u0085\u0001\u0086\u0001\u0086\u0001\u0087"+
		"\u0001\u0087\u0001\u0087\u0000\u0000\u0088\u0000\u0002\u0004\u0006\b\n"+
		"\f\u000e\u0010\u0012\u0014\u0016\u0018\u001a\u001c\u001e \"$&(*,.0246"+
		"8:<>@BDFHJLNPRTVXZ\\^`bdfhjlnprtvxz|~\u0080\u0082\u0084\u0086\u0088\u008a"+
		"\u008c\u008e\u0090\u0092\u0094\u0096\u0098\u009a\u009c\u009e\u00a0\u00a2"+
		"\u00a4\u00a6\u00a8\u00aa\u00ac\u00ae\u00b0\u00b2\u00b4\u00b6\u00b8\u00ba"+
		"\u00bc\u00be\u00c0\u00c2\u00c4\u00c6\u00c8\u00ca\u00cc\u00ce\u00d0\u00d2"+
		"\u00d4\u00d6\u00d8\u00da\u00dc\u00de\u00e0\u00e2\u00e4\u00e6\u00e8\u00ea"+
		"\u00ec\u00ee\u00f0\u00f2\u00f4\u00f6\u00f8\u00fa\u00fc\u00fe\u0100\u0102"+
		"\u0104\u0106\u0108\u010a\u010c\u010e\u0000\u000b\u0001\u0000\u0004\u0005"+
		"\u0001\u0000\u0016\u0017\u0002\u0000\t\t>?\u0001\u0000\u0094\u0095\u0001"+
		"\u0000\u0097\u0099\u0001\u0000\u009a\u009c\u0001\u0000\u009d\u009f\u0001"+
		"\u0000\u008c\u008f\u0001\u0000\u00a1\u00a4\u0001\u0000\u0091\u0092\u0002"+
		"\u0000\u0093\u0093\u00a9\u00a9\u0674\u0000\u0112\u0001\u0000\u0000\u0000"+
		"\u0002\u0114\u0001\u0000\u0000\u0000\u0004\u0121\u0001\u0000\u0000\u0000"+
		"\u0006\u0124\u0001\u0000\u0000\u0000\b\u0127\u0001\u0000\u0000\u0000\n"+
		"\u012b\u0001\u0000\u0000\u0000\f\u0135\u0001\u0000\u0000\u0000\u000e\u013a"+
		"\u0001\u0000\u0000\u0000\u0010\u014c\u0001\u0000\u0000\u0000\u0012\u0166"+
		"\u0001\u0000\u0000\u0000\u0014\u017a\u0001\u0000\u0000\u0000\u0016\u0184"+
		"\u0001\u0000\u0000\u0000\u0018\u0189\u0001\u0000\u0000\u0000\u001a\u018b"+
		"\u0001\u0000\u0000\u0000\u001c\u018e\u0001\u0000\u0000\u0000\u001e\u0191"+
		"\u0001\u0000\u0000\u0000 \u0196\u0001\u0000\u0000\u0000\"\u01a1\u0001"+
		"\u0000\u0000\u0000$\u01b3\u0001\u0000\u0000\u0000&\u01b5\u0001\u0000\u0000"+
		"\u0000(\u01bb\u0001\u0000\u0000\u0000*\u01bd\u0001\u0000\u0000\u0000,"+
		"\u01ca\u0001\u0000\u0000\u0000.\u01d4\u0001\u0000\u0000\u00000\u01d6\u0001"+
		"\u0000\u0000\u00002\u01d9\u0001\u0000\u0000\u00004\u01de\u0001\u0000\u0000"+
		"\u00006\u01e0\u0001\u0000\u0000\u00008\u01f3\u0001\u0000\u0000\u0000:"+
		"\u01f5\u0001\u0000\u0000\u0000<\u01fe\u0001\u0000\u0000\u0000>\u0204\u0001"+
		"\u0000\u0000\u0000@\u020a\u0001\u0000\u0000\u0000B\u0210\u0001\u0000\u0000"+
		"\u0000D\u0218\u0001\u0000\u0000\u0000F\u0220\u0001\u0000\u0000\u0000H"+
		"\u0228\u0001\u0000\u0000\u0000J\u022b\u0001\u0000\u0000\u0000L\u022e\u0001"+
		"\u0000\u0000\u0000N\u0233\u0001\u0000\u0000\u0000P\u0245\u0001\u0000\u0000"+
		"\u0000R\u0248\u0001\u0000\u0000\u0000T\u024b\u0001\u0000\u0000\u0000V"+
		"\u0256\u0001\u0000\u0000\u0000X\u0258\u0001\u0000\u0000\u0000Z\u025f\u0001"+
		"\u0000\u0000\u0000\\\u0261\u0001\u0000\u0000\u0000^\u0265\u0001\u0000"+
		"\u0000\u0000`\u026a\u0001\u0000\u0000\u0000b\u0278\u0001\u0000\u0000\u0000"+
		"d\u027c\u0001\u0000\u0000\u0000f\u0283\u0001\u0000\u0000\u0000h\u028b"+
		"\u0001\u0000\u0000\u0000j\u0299\u0001\u0000\u0000\u0000l\u02a8\u0001\u0000"+
		"\u0000\u0000n\u02aa\u0001\u0000\u0000\u0000p\u02ad\u0001\u0000\u0000\u0000"+
		"r\u02b1\u0001\u0000\u0000\u0000t\u02b8\u0001\u0000\u0000\u0000v\u02bf"+
		"\u0001\u0000\u0000\u0000x\u02c4\u0001\u0000\u0000\u0000z\u02c6\u0001\u0000"+
		"\u0000\u0000|\u02d9\u0001\u0000\u0000\u0000~\u02f1\u0001\u0000\u0000\u0000"+
		"\u0080\u02f3\u0001\u0000\u0000\u0000\u0082\u02f6\u0001\u0000\u0000\u0000"+
		"\u0084\u02fe\u0001\u0000\u0000\u0000\u0086\u0304\u0001\u0000\u0000\u0000"+
		"\u0088\u0306\u0001\u0000\u0000\u0000\u008a\u0318\u0001\u0000\u0000\u0000"+
		"\u008c\u0326\u0001\u0000\u0000\u0000\u008e\u0328\u0001\u0000\u0000\u0000"+
		"\u0090\u032e\u0001\u0000\u0000\u0000\u0092\u033b\u0001\u0000\u0000\u0000"+
		"\u0094\u033e\u0001\u0000\u0000\u0000\u0096\u0340\u0001\u0000\u0000\u0000"+
		"\u0098\u0350\u0001\u0000\u0000\u0000\u009a\u0352\u0001\u0000\u0000\u0000"+
		"\u009c\u035a\u0001\u0000\u0000\u0000\u009e\u0362\u0001\u0000\u0000\u0000"+
		"\u00a0\u0365\u0001\u0000\u0000\u0000\u00a2\u0369\u0001\u0000\u0000\u0000"+
		"\u00a4\u037a\u0001\u0000\u0000\u0000\u00a6\u037c\u0001\u0000\u0000\u0000"+
		"\u00a8\u037e\u0001\u0000\u0000\u0000\u00aa\u0386\u0001\u0000\u0000\u0000"+
		"\u00ac\u0388\u0001\u0000\u0000\u0000\u00ae\u038a\u0001\u0000\u0000\u0000"+
		"\u00b0\u0392\u0001\u0000\u0000\u0000\u00b2\u039a\u0001\u0000\u0000\u0000"+
		"\u00b4\u03a1\u0001\u0000\u0000\u0000\u00b6\u03a3\u0001\u0000\u0000\u0000"+
		"\u00b8\u03ae\u0001\u0000\u0000\u0000\u00ba\u03bd\u0001\u0000\u0000\u0000"+
		"\u00bc\u03c8\u0001\u0000\u0000\u0000\u00be\u03cc\u0001\u0000\u0000\u0000"+
		"\u00c0\u03ce\u0001\u0000\u0000\u0000\u00c2\u03d4\u0001\u0000\u0000\u0000"+
		"\u00c4\u03d6\u0001\u0000\u0000\u0000\u00c6\u03da\u0001\u0000\u0000\u0000"+
		"\u00c8\u03e2\u0001\u0000\u0000\u0000\u00ca\u03ec\u0001\u0000\u0000\u0000"+
		"\u00cc\u03f0\u0001\u0000\u0000\u0000\u00ce\u03f4\u0001\u0000\u0000\u0000"+
		"\u00d0\u03f8\u0001\u0000\u0000\u0000\u00d2\u03fa\u0001\u0000\u0000\u0000"+
		"\u00d4\u0403\u0001\u0000\u0000\u0000\u00d6\u0405\u0001\u0000\u0000\u0000"+
		"\u00d8\u0407\u0001\u0000\u0000\u0000\u00da\u040f\u0001\u0000\u0000\u0000"+
		"\u00dc\u0417\u0001\u0000\u0000\u0000\u00de\u0419\u0001\u0000\u0000\u0000"+
		"\u00e0\u042d\u0001\u0000\u0000\u0000\u00e2\u042f\u0001\u0000\u0000\u0000"+
		"\u00e4\u0446\u0001\u0000\u0000\u0000\u00e6\u0457\u0001\u0000\u0000\u0000"+
		"\u00e8\u0460\u0001\u0000\u0000\u0000\u00ea\u0462\u0001\u0000\u0000\u0000"+
		"\u00ec\u0568\u0001\u0000\u0000\u0000\u00ee\u056a\u0001\u0000\u0000\u0000"+
		"\u00f0\u0575\u0001\u0000\u0000\u0000\u00f2\u0580\u0001\u0000\u0000\u0000"+
		"\u00f4\u058d\u0001\u0000\u0000\u0000\u00f6\u0590\u0001\u0000\u0000\u0000"+
		"\u00f8\u05d4\u0001\u0000\u0000\u0000\u00fa\u05d6\u0001\u0000\u0000\u0000"+
		"\u00fc\u05da\u0001\u0000\u0000\u0000\u00fe\u05e3\u0001\u0000\u0000\u0000"+
		"\u0100\u05e5\u0001\u0000\u0000\u0000\u0102\u05e7\u0001\u0000\u0000\u0000"+
		"\u0104\u05e9\u0001\u0000\u0000\u0000\u0106\u05eb\u0001\u0000\u0000\u0000"+
		"\u0108\u05ed\u0001\u0000\u0000\u0000\u010a\u05f1\u0001\u0000\u0000\u0000"+
		"\u010c\u05f3\u0001\u0000\u0000\u0000\u010e\u05f5\u0001\u0000\u0000\u0000"+
		"\u0110\u0113\u0003\u0002\u0001\u0000\u0111\u0113\u00036\u001b\u0000\u0112"+
		"\u0110\u0001\u0000\u0000\u0000\u0112\u0111\u0001\u0000\u0000\u0000\u0113"+
		"\u0001\u0001\u0000\u0000\u0000\u0114\u0119\u0003\u0004\u0002\u0000\u0115"+
		"\u011a\u0003\n\u0005\u0000\u0116\u011a\u0003\u0010\b\u0000\u0117\u011a"+
		"\u0003\u0012\t\u0000\u0118\u011a\u0003\u0014\n\u0000\u0119\u0115\u0001"+
		"\u0000\u0000\u0000\u0119\u0116\u0001\u0000\u0000\u0000\u0119\u0117\u0001"+
		"\u0000\u0000\u0000\u0119\u0118\u0001\u0000\u0000\u0000\u011a\u011b\u0001"+
		"\u0000\u0000\u0000\u011b\u011c\u00034\u001a\u0000\u011c\u0003\u0001\u0000"+
		"\u0000\u0000\u011d\u0120\u0003\u0006\u0003\u0000\u011e\u0120\u0003\b\u0004"+
		"\u0000\u011f\u011d\u0001\u0000\u0000\u0000\u011f\u011e\u0001\u0000\u0000"+
		"\u0000\u0120\u0123\u0001\u0000\u0000\u0000\u0121\u011f\u0001\u0000\u0000"+
		"\u0000\u0121\u0122\u0001\u0000\u0000\u0000\u0122\u0005\u0001\u0000\u0000"+
		"\u0000\u0123\u0121\u0001\u0000\u0000\u0000\u0124\u0125\u0005\u0001\u0000"+
		"\u0000\u0125\u0126\u0005\u0090\u0000\u0000\u0126\u0007\u0001\u0000\u0000"+
		"\u0000\u0127\u0128\u0005\u0002\u0000\u0000\u0128\u0129\u0005\u0091\u0000"+
		"\u0000\u0129\u012a\u0005\u0090\u0000\u0000\u012a\t\u0001\u0000\u0000\u0000"+
		"\u012b\u012f\u0003\u000e\u0007\u0000\u012c\u012e\u0003\u0016\u000b\u0000"+
		"\u012d\u012c\u0001\u0000\u0000\u0000\u012e\u0131\u0001\u0000\u0000\u0000"+
		"\u012f\u012d\u0001\u0000\u0000\u0000\u012f\u0130\u0001\u0000\u0000\u0000"+
		"\u0130\u0132\u0001\u0000\u0000\u0000\u0131\u012f\u0001\u0000\u0000\u0000"+
		"\u0132\u0133\u0003\u001e\u000f\u0000\u0133\u0134\u0003 \u0010\u0000\u0134"+
		"\u000b\u0001\u0000\u0000\u0000\u0135\u0136\u0003\u000e\u0007\u0000\u0136"+
		"\u0137\u0003\u001e\u000f\u0000\u0137\u0138\u0003 \u0010\u0000\u0138\u0139"+
		"\u00034\u001a\u0000\u0139\r\u0001\u0000\u0000\u0000\u013a\u013c\u0005"+
		"\u0003\u0000\u0000\u013b\u013d\u0007\u0000\u0000\u0000\u013c\u013b\u0001"+
		"\u0000\u0000\u0000\u013c\u013d\u0001\u0000\u0000\u0000\u013d\u014a\u0001"+
		"\u0000\u0000\u0000\u013e\u0146\u0003\u00d2i\u0000\u013f\u0140\u0005\u0006"+
		"\u0000\u0000\u0140\u0141\u0003\u00d6k\u0000\u0141\u0142\u0005\u0007\u0000"+
		"\u0000\u0142\u0143\u0003\u00d2i\u0000\u0143\u0144\u0005\b\u0000\u0000"+
		"\u0144\u0146\u0001\u0000\u0000\u0000\u0145\u013e\u0001\u0000\u0000\u0000"+
		"\u0145\u013f\u0001\u0000\u0000\u0000\u0146\u0147\u0001\u0000\u0000\u0000"+
		"\u0147\u0145\u0001\u0000\u0000\u0000\u0147\u0148\u0001\u0000\u0000\u0000"+
		"\u0148\u014b\u0001\u0000\u0000\u0000\u0149\u014b\u0005\t\u0000\u0000\u014a"+
		"\u0145\u0001\u0000\u0000\u0000\u014a\u0149\u0001\u0000\u0000\u0000\u014b"+
		"\u000f\u0001\u0000\u0000\u0000\u014c\u0164\u0005\n\u0000\u0000\u014d\u0151"+
		"\u0003\u008eG\u0000\u014e\u0150\u0003\u0016\u000b\u0000\u014f\u014e\u0001"+
		"\u0000\u0000\u0000\u0150\u0153\u0001\u0000\u0000\u0000\u0151\u014f\u0001"+
		"\u0000\u0000\u0000\u0151\u0152\u0001\u0000\u0000\u0000\u0152\u0154\u0001"+
		"\u0000\u0000\u0000\u0153\u0151\u0001\u0000\u0000\u0000\u0154\u0155\u0003"+
		"\u001e\u000f\u0000\u0155\u0156\u0003 \u0010\u0000\u0156\u0165\u0001\u0000"+
		"\u0000\u0000\u0157\u0159\u0003\u0016\u000b\u0000\u0158\u0157\u0001\u0000"+
		"\u0000\u0000\u0159\u015c\u0001\u0000\u0000\u0000\u015a\u0158\u0001\u0000"+
		"\u0000\u0000\u015a\u015b\u0001\u0000\u0000\u0000\u015b\u015d\u0001\u0000"+
		"\u0000\u0000\u015c\u015a\u0001\u0000\u0000\u0000\u015d\u015e\u0005\u000b"+
		"\u0000\u0000\u015e\u0160\u0005\f\u0000\u0000\u015f\u0161\u0003d2\u0000"+
		"\u0160\u015f\u0001\u0000\u0000\u0000\u0160\u0161\u0001\u0000\u0000\u0000"+
		"\u0161\u0162\u0001\u0000\u0000\u0000\u0162\u0163\u0005\r\u0000\u0000\u0163"+
		"\u0165\u0003 \u0010\u0000\u0164\u014d\u0001\u0000\u0000\u0000\u0164\u015a"+
		"\u0001\u0000\u0000\u0000\u0165\u0011\u0001\u0000\u0000\u0000\u0166\u016d"+
		"\u0005\u000e\u0000\u0000\u0167\u0169\u0003\u00d0h\u0000\u0168\u0167\u0001"+
		"\u0000\u0000\u0000\u0169\u016a\u0001\u0000\u0000\u0000\u016a\u0168\u0001"+
		"\u0000\u0000\u0000\u016a\u016b\u0001\u0000\u0000\u0000\u016b\u016e\u0001"+
		"\u0000\u0000\u0000\u016c\u016e\u0005\t\u0000\u0000\u016d\u0168\u0001\u0000"+
		"\u0000\u0000\u016d\u016c\u0001\u0000\u0000\u0000\u016e\u0172\u0001\u0000"+
		"\u0000\u0000\u016f\u0171\u0003\u0016\u000b\u0000\u0170\u016f\u0001\u0000"+
		"\u0000\u0000\u0171\u0174\u0001\u0000\u0000\u0000\u0172\u0170\u0001\u0000"+
		"\u0000\u0000\u0172\u0173\u0001\u0000\u0000\u0000\u0173\u0176\u0001\u0000"+
		"\u0000\u0000\u0174\u0172\u0001\u0000\u0000\u0000\u0175\u0177\u0003\u001e"+
		"\u000f\u0000\u0176\u0175\u0001\u0000\u0000\u0000\u0176\u0177\u0001\u0000"+
		"\u0000\u0000\u0177\u0178\u0001\u0000\u0000\u0000\u0178\u0179\u0003 \u0010"+
		"\u0000\u0179\u0013\u0001\u0000\u0000\u0000\u017a\u017e\u0005\u000f\u0000"+
		"\u0000\u017b\u017d\u0003\u0016\u000b\u0000\u017c\u017b\u0001\u0000\u0000"+
		"\u0000\u017d\u0180\u0001\u0000\u0000\u0000\u017e\u017c\u0001\u0000\u0000"+
		"\u0000\u017e\u017f\u0001\u0000\u0000\u0000\u017f\u0181\u0001\u0000\u0000"+
		"\u0000\u0180\u017e\u0001\u0000\u0000\u0000\u0181\u0182\u0003\u001e\u000f"+
		"\u0000\u0182\u0183\u0003 \u0010\u0000\u0183\u0015\u0001\u0000\u0000\u0000"+
		"\u0184\u0187\u0005\u0010\u0000\u0000\u0185\u0188\u0003\u0018\f\u0000\u0186"+
		"\u0188\u0003\u001a\r\u0000\u0187\u0185\u0001\u0000\u0000\u0000\u0187\u0186"+
		"\u0001\u0000\u0000\u0000\u0188\u0017\u0001\u0000\u0000\u0000\u0189\u018a"+
		"\u0003\u001c\u000e\u0000\u018a\u0019\u0001\u0000\u0000\u0000\u018b\u018c"+
		"\u0005\u0011\u0000\u0000\u018c\u018d\u0003\u001c\u000e\u0000\u018d\u001b"+
		"\u0001\u0000\u0000\u0000\u018e\u018f\u0003\u010a\u0085\u0000\u018f\u001d"+
		"\u0001\u0000\u0000\u0000\u0190\u0192\u0005\u000b\u0000\u0000\u0191\u0190"+
		"\u0001\u0000\u0000\u0000\u0191\u0192\u0001\u0000\u0000\u0000\u0192\u0193"+
		"\u0001\u0000\u0000\u0000\u0193\u0194\u0003f3\u0000\u0194\u001f\u0001\u0000"+
		"\u0000\u0000\u0195\u0197\u0003\"\u0011\u0000\u0196\u0195\u0001\u0000\u0000"+
		"\u0000\u0196\u0197\u0001\u0000\u0000\u0000\u0197\u0199\u0001\u0000\u0000"+
		"\u0000\u0198\u019a\u0003&\u0013\u0000\u0199\u0198\u0001\u0000\u0000\u0000"+
		"\u0199\u019a\u0001\u0000\u0000\u0000\u019a\u019c\u0001\u0000\u0000\u0000"+
		"\u019b\u019d\u0003*\u0015\u0000\u019c\u019b\u0001\u0000\u0000\u0000\u019c"+
		"\u019d\u0001\u0000\u0000\u0000\u019d\u019f\u0001\u0000\u0000\u0000\u019e"+
		"\u01a0\u0003.\u0017\u0000\u019f\u019e\u0001\u0000\u0000\u0000\u019f\u01a0"+
		"\u0001\u0000\u0000\u0000\u01a0!\u0001\u0000\u0000\u0000\u01a1\u01a2\u0005"+
		"\u0012\u0000\u0000\u01a2\u01a4\u0005\u0013\u0000\u0000\u01a3\u01a5\u0003"+
		"$\u0012\u0000\u01a4\u01a3\u0001\u0000\u0000\u0000\u01a5\u01a6\u0001\u0000"+
		"\u0000\u0000\u01a6\u01a4\u0001\u0000\u0000\u0000\u01a6\u01a7\u0001\u0000"+
		"\u0000\u0000\u01a7#\u0001\u0000\u0000\u0000\u01a8\u01b4\u0003\u00ecv\u0000"+
		"\u01a9\u01b4\u0003\u0088D\u0000\u01aa\u01ab\u0005\u0006\u0000\u0000\u01ab"+
		"\u01ae\u0003\u00d6k\u0000\u01ac\u01ad\u0005\u0007\u0000\u0000\u01ad\u01af"+
		"\u0003\u00d2i\u0000\u01ae\u01ac\u0001\u0000\u0000\u0000\u01ae\u01af\u0001"+
		"\u0000\u0000\u0000\u01af\u01b0\u0001\u0000\u0000\u0000\u01b0\u01b1\u0005"+
		"\b\u0000\u0000\u01b1\u01b4\u0001\u0000\u0000\u0000\u01b2\u01b4\u0003\u00d2"+
		"i\u0000\u01b3\u01a8\u0001\u0000\u0000\u0000\u01b3\u01a9\u0001\u0000\u0000"+
		"\u0000\u01b3\u01aa\u0001\u0000\u0000\u0000\u01b3\u01b2\u0001\u0000\u0000"+
		"\u0000\u01b4%\u0001\u0000\u0000\u0000\u01b5\u01b7\u0005\u0014\u0000\u0000"+
		"\u01b6\u01b8\u0003(\u0014\u0000\u01b7\u01b6\u0001\u0000\u0000\u0000\u01b8"+
		"\u01b9\u0001\u0000\u0000\u0000\u01b9\u01b7\u0001\u0000\u0000\u0000\u01b9"+
		"\u01ba\u0001\u0000\u0000\u0000\u01ba\'\u0001\u0000\u0000\u0000\u01bb\u01bc"+
		"\u0003\u0086C\u0000\u01bc)\u0001\u0000\u0000\u0000\u01bd\u01be\u0005\u0015"+
		"\u0000\u0000\u01be\u01c0\u0005\u0013\u0000\u0000\u01bf\u01c1\u0003,\u0016"+
		"\u0000\u01c0\u01bf\u0001\u0000\u0000\u0000\u01c1\u01c2\u0001\u0000\u0000"+
		"\u0000\u01c2\u01c0\u0001\u0000\u0000\u0000\u01c2\u01c3\u0001\u0000\u0000"+
		"\u0000\u01c3+\u0001\u0000\u0000\u0000\u01c4\u01c5\u0007\u0001\u0000\u0000"+
		"\u01c5\u01cb\u0003\u00eau\u0000\u01c6\u01c9\u0003\u0086C\u0000\u01c7\u01c9"+
		"\u0003\u00d2i\u0000\u01c8\u01c6\u0001\u0000\u0000\u0000\u01c8\u01c7\u0001"+
		"\u0000\u0000\u0000\u01c9\u01cb\u0001\u0000\u0000\u0000\u01ca\u01c4\u0001"+
		"\u0000\u0000\u0000\u01ca\u01c8\u0001\u0000\u0000\u0000\u01cb-\u0001\u0000"+
		"\u0000\u0000\u01cc\u01ce\u00030\u0018\u0000\u01cd\u01cf\u00032\u0019\u0000"+
		"\u01ce\u01cd\u0001\u0000\u0000\u0000\u01ce\u01cf\u0001\u0000\u0000\u0000"+
		"\u01cf\u01d5\u0001\u0000\u0000\u0000\u01d0\u01d2\u00032\u0019\u0000\u01d1"+
		"\u01d3\u00030\u0018\u0000\u01d2\u01d1\u0001\u0000\u0000\u0000\u01d2\u01d3"+
		"\u0001\u0000\u0000\u0000\u01d3\u01d5\u0001\u0000\u0000\u0000\u01d4\u01cc"+
		"\u0001\u0000\u0000\u0000\u01d4\u01d0\u0001\u0000\u0000\u0000\u01d5/\u0001"+
		"\u0000\u0000\u0000\u01d6\u01d7\u0005\u0018\u0000\u0000\u01d7\u01d8\u0005"+
		"\u0097\u0000\u0000\u01d81\u0001\u0000\u0000\u0000\u01d9\u01da\u0005\u0019"+
		"\u0000\u0000\u01da\u01db\u0005\u0097\u0000\u0000\u01db3\u0001\u0000\u0000"+
		"\u0000\u01dc\u01dd\u0005\u001a\u0000\u0000\u01dd\u01df\u0003x<\u0000\u01de"+
		"\u01dc\u0001\u0000\u0000\u0000\u01de\u01df\u0001\u0000\u0000\u0000\u01df"+
		"5\u0001\u0000\u0000\u0000\u01e0\u01e6\u0003\u0004\u0002\u0000\u01e1\u01e4"+
		"\u00038\u001c\u0000\u01e2\u01e3\u0005\u001b\u0000\u0000\u01e3\u01e5\u0003"+
		"6\u001b\u0000\u01e4\u01e2\u0001\u0000\u0000\u0000\u01e4\u01e5\u0001\u0000"+
		"\u0000\u0000\u01e5\u01e7\u0001\u0000\u0000\u0000\u01e6\u01e1\u0001\u0000"+
		"\u0000\u0000\u01e6\u01e7\u0001\u0000\u0000\u0000\u01e77\u0001\u0000\u0000"+
		"\u0000\u01e8\u01f4\u0003:\u001d\u0000\u01e9\u01f4\u0003<\u001e\u0000\u01ea"+
		"\u01f4\u0003>\u001f\u0000\u01eb\u01f4\u0003B!\u0000\u01ec\u01f4\u0003"+
		"D\"\u0000\u01ed\u01f4\u0003F#\u0000\u01ee\u01f4\u0003@ \u0000\u01ef\u01f4"+
		"\u0003H$\u0000\u01f0\u01f4\u0003J%\u0000\u01f1\u01f4\u0003L&\u0000\u01f2"+
		"\u01f4\u0003N\'\u0000\u01f3\u01e8\u0001\u0000\u0000\u0000\u01f3\u01e9"+
		"\u0001\u0000\u0000\u0000\u01f3\u01ea\u0001\u0000\u0000\u0000\u01f3\u01eb"+
		"\u0001\u0000\u0000\u0000\u01f3\u01ec\u0001\u0000\u0000\u0000\u01f3\u01ed"+
		"\u0001\u0000\u0000\u0000\u01f3\u01ee\u0001\u0000\u0000\u0000\u01f3\u01ef"+
		"\u0001\u0000\u0000\u0000\u01f3\u01f0\u0001\u0000\u0000\u0000\u01f3\u01f1"+
		"\u0001\u0000\u0000\u0000\u01f3\u01f2\u0001\u0000\u0000\u0000\u01f49\u0001"+
		"\u0000\u0000\u0000\u01f5\u01f7\u0005\u001c\u0000\u0000\u01f6\u01f8\u0005"+
		"\u001d\u0000\u0000\u01f7\u01f6\u0001\u0000\u0000\u0000\u01f7\u01f8\u0001"+
		"\u0000\u0000\u0000\u01f8\u01f9\u0001\u0000\u0000\u0000\u01f9\u01fc\u0003"+
		"\u010a\u0085\u0000\u01fa\u01fb\u0005\u001e\u0000\u0000\u01fb\u01fd\u0003"+
		"X,\u0000\u01fc\u01fa\u0001\u0000\u0000\u0000\u01fc\u01fd\u0001\u0000\u0000"+
		"\u0000\u01fd;\u0001\u0000\u0000\u0000\u01fe\u0200\u0005\u001f\u0000\u0000"+
		"\u01ff\u0201\u0005\u001d\u0000\u0000\u0200\u01ff\u0001\u0000\u0000\u0000"+
		"\u0200\u0201\u0001\u0000\u0000\u0000\u0201\u0202\u0001\u0000\u0000\u0000"+
		"\u0202\u0203\u0003Z-\u0000\u0203=\u0001\u0000\u0000\u0000\u0204\u0206"+
		"\u0005 \u0000\u0000\u0205\u0207\u0005\u001d\u0000\u0000\u0206\u0205\u0001"+
		"\u0000\u0000\u0000\u0206\u0207\u0001\u0000\u0000\u0000\u0207\u0208\u0001"+
		"\u0000\u0000\u0000\u0208\u0209\u0003Z-\u0000\u0209?\u0001\u0000\u0000"+
		"\u0000\u020a\u020c\u0005!\u0000\u0000\u020b\u020d\u0005\u001d\u0000\u0000"+
		"\u020c\u020b\u0001\u0000\u0000\u0000\u020c\u020d\u0001\u0000\u0000\u0000"+
		"\u020d\u020e\u0001\u0000\u0000\u0000\u020e\u020f\u0003X,\u0000\u020fA"+
		"\u0001\u0000\u0000\u0000\u0210\u0212\u0005\"\u0000\u0000\u0211\u0213\u0005"+
		"\u001d\u0000\u0000\u0212\u0211\u0001\u0000\u0000\u0000\u0212\u0213\u0001"+
		"\u0000\u0000\u0000\u0213\u0214\u0001\u0000\u0000\u0000\u0214\u0215\u0003"+
		"V+\u0000\u0215\u0216\u0005#\u0000\u0000\u0216\u0217\u0003V+\u0000\u0217"+
		"C\u0001\u0000\u0000\u0000\u0218\u021a\u0005$\u0000\u0000\u0219\u021b\u0005"+
		"\u001d\u0000\u0000\u021a\u0219\u0001\u0000\u0000\u0000\u021a\u021b\u0001"+
		"\u0000\u0000\u0000\u021b\u021c\u0001\u0000\u0000\u0000\u021c\u021d\u0003"+
		"V+\u0000\u021d\u021e\u0005#\u0000\u0000\u021e\u021f\u0003V+\u0000\u021f"+
		"E\u0001\u0000\u0000\u0000\u0220\u0222\u0005%\u0000\u0000\u0221\u0223\u0005"+
		"\u001d\u0000\u0000\u0222\u0221\u0001\u0000\u0000\u0000\u0222\u0223\u0001"+
		"\u0000\u0000\u0000\u0223\u0224\u0001\u0000\u0000\u0000\u0224\u0225\u0003"+
		"V+\u0000\u0225\u0226\u0005#\u0000\u0000\u0226\u0227\u0003V+\u0000\u0227"+
		"G\u0001\u0000\u0000\u0000\u0228\u0229\u0005&\u0000\u0000\u0229\u022a\u0003"+
		"^/\u0000\u022aI\u0001\u0000\u0000\u0000\u022b\u022c\u0005\'\u0000\u0000"+
		"\u022c\u022d\u0003^/\u0000\u022dK\u0001\u0000\u0000\u0000\u022e\u022f"+
		"\u0005(\u0000\u0000\u022f\u0230\u0003\\.\u0000\u0230M\u0001\u0000\u0000"+
		"\u0000\u0231\u0232\u0005)\u0000\u0000\u0232\u0234\u0003\u010a\u0085\u0000"+
		"\u0233\u0231\u0001\u0000\u0000\u0000\u0233\u0234\u0001\u0000\u0000\u0000"+
		"\u0234\u023a\u0001\u0000\u0000\u0000\u0235\u0237\u0003P(\u0000\u0236\u0238"+
		"\u0003R)\u0000\u0237\u0236\u0001\u0000\u0000\u0000\u0237\u0238\u0001\u0000"+
		"\u0000\u0000\u0238\u023b\u0001\u0000\u0000\u0000\u0239\u023b\u0003R)\u0000"+
		"\u023a\u0235\u0001\u0000\u0000\u0000\u023a\u0239\u0001\u0000\u0000\u0000"+
		"\u023b\u023f\u0001\u0000\u0000\u0000\u023c\u023e\u0003T*\u0000\u023d\u023c"+
		"\u0001\u0000\u0000\u0000\u023e\u0241\u0001\u0000\u0000\u0000\u023f\u023d"+
		"\u0001\u0000\u0000\u0000\u023f\u0240\u0001\u0000\u0000\u0000\u0240\u0242"+
		"\u0001\u0000\u0000\u0000\u0241\u023f\u0001\u0000\u0000\u0000\u0242\u0243"+
		"\u0005\u000b\u0000\u0000\u0243\u0244\u0003f3\u0000\u0244O\u0001\u0000"+
		"\u0000\u0000\u0245\u0246\u0005*\u0000\u0000\u0246\u0247\u0003\\.\u0000"+
		"\u0247Q\u0001\u0000\u0000\u0000\u0248\u0249\u0005+\u0000\u0000\u0249\u024a"+
		"\u0003\\.\u0000\u024aS\u0001\u0000\u0000\u0000\u024b\u024f\u0005,\u0000"+
		"\u0000\u024c\u0250\u0003\u010a\u0085\u0000\u024d\u024e\u0005\u0011\u0000"+
		"\u0000\u024e\u0250\u0003\u010a\u0085\u0000\u024f\u024c\u0001\u0000\u0000"+
		"\u0000\u024f\u024d\u0001\u0000\u0000\u0000\u0250U\u0001\u0000\u0000\u0000"+
		"\u0251\u0257\u0005-\u0000\u0000\u0252\u0254\u0005.\u0000\u0000\u0253\u0252"+
		"\u0001\u0000\u0000\u0000\u0253\u0254\u0001\u0000\u0000\u0000\u0254\u0255"+
		"\u0001\u0000\u0000\u0000\u0255\u0257\u0003\u010a\u0085\u0000\u0256\u0251"+
		"\u0001\u0000\u0000\u0000\u0256\u0253\u0001\u0000\u0000\u0000\u0257W\u0001"+
		"\u0000\u0000\u0000\u0258\u0259\u0005.\u0000\u0000\u0259\u025a\u0003\u010a"+
		"\u0085\u0000\u025aY\u0001\u0000\u0000\u0000\u025b\u0260\u0003X,\u0000"+
		"\u025c\u0260\u0005-\u0000\u0000\u025d\u0260\u0005\u0011\u0000\u0000\u025e"+
		"\u0260\u0005/\u0000\u0000\u025f\u025b\u0001\u0000\u0000\u0000\u025f\u025c"+
		"\u0001\u0000\u0000\u0000\u025f\u025d\u0001\u0000\u0000\u0000\u025f\u025e"+
		"\u0001\u0000\u0000\u0000\u0260[\u0001\u0000\u0000\u0000\u0261\u0262\u0005"+
		"\f\u0000\u0000\u0262\u0263\u0003`0\u0000\u0263\u0264\u0005\r\u0000\u0000"+
		"\u0264]\u0001\u0000\u0000\u0000\u0265\u0266\u0005\f\u0000\u0000\u0266"+
		"\u0267\u0003`0\u0000\u0267\u0268\u0005\r\u0000\u0000\u0268_\u0001\u0000"+
		"\u0000\u0000\u0269\u026b\u0003d2\u0000\u026a\u0269\u0001\u0000\u0000\u0000"+
		"\u026a\u026b\u0001\u0000\u0000\u0000\u026b\u0275\u0001\u0000\u0000\u0000"+
		"\u026c\u026e\u0003b1\u0000\u026d\u026f\u00050\u0000\u0000\u026e\u026d"+
		"\u0001\u0000\u0000\u0000\u026e\u026f\u0001\u0000\u0000\u0000\u026f\u0271"+
		"\u0001\u0000\u0000\u0000\u0270\u0272\u0003d2\u0000\u0271\u0270\u0001\u0000"+
		"\u0000\u0000\u0271\u0272\u0001\u0000\u0000\u0000\u0272\u0274\u0001\u0000"+
		"\u0000\u0000\u0273\u026c\u0001\u0000\u0000\u0000\u0274\u0277\u0001\u0000"+
		"\u0000\u0000\u0275\u0273\u0001\u0000\u0000\u0000\u0275\u0276\u0001\u0000"+
		"\u0000\u0000\u0276a\u0001\u0000\u0000\u0000\u0277\u0275\u0001\u0000\u0000"+
		"\u0000\u0278\u0279\u0005.\u0000\u0000\u0279\u027a\u0003\u00d0h\u0000\u027a"+
		"\u027b\u0003f3\u0000\u027bc\u0001\u0000\u0000\u0000\u027c\u0281\u0003"+
		"\u0092I\u0000\u027d\u027f\u00050\u0000\u0000\u027e\u0280\u0003d2\u0000"+
		"\u027f\u027e\u0001\u0000\u0000\u0000\u027f\u0280\u0001\u0000\u0000\u0000"+
		"\u0280\u0282\u0001\u0000\u0000\u0000\u0281\u027d\u0001\u0000\u0000\u0000"+
		"\u0281\u0282\u0001\u0000\u0000\u0000\u0282e\u0001\u0000\u0000\u0000\u0283"+
		"\u0286\u0005\f\u0000\u0000\u0284\u0287\u0003\f\u0006\u0000\u0285\u0287"+
		"\u0003h4\u0000\u0286\u0284\u0001\u0000\u0000\u0000\u0286\u0285\u0001\u0000"+
		"\u0000\u0000\u0287\u0288\u0001\u0000\u0000\u0000\u0288\u0289\u0005\r\u0000"+
		"\u0000\u0289g\u0001\u0000\u0000\u0000\u028a\u028c\u0003j5\u0000\u028b"+
		"\u028a\u0001\u0000\u0000\u0000\u028b\u028c\u0001\u0000\u0000\u0000\u028c"+
		"\u0296\u0001\u0000\u0000\u0000\u028d\u028f\u0003l6\u0000\u028e\u0290\u0005"+
		"0\u0000\u0000\u028f\u028e\u0001\u0000\u0000\u0000\u028f\u0290\u0001\u0000"+
		"\u0000\u0000\u0290\u0292\u0001\u0000\u0000\u0000\u0291\u0293\u0003j5\u0000"+
		"\u0292\u0291\u0001\u0000\u0000\u0000\u0292\u0293\u0001\u0000\u0000\u0000"+
		"\u0293\u0295\u0001\u0000\u0000\u0000\u0294\u028d\u0001\u0000\u0000\u0000"+
		"\u0295\u0298\u0001\u0000\u0000\u0000\u0296\u0294\u0001\u0000\u0000\u0000"+
		"\u0296\u0297\u0001\u0000\u0000\u0000\u0297i\u0001\u0000\u0000\u0000\u0298"+
		"\u0296\u0001\u0000\u0000\u0000\u0299\u029e\u0003\u009eO\u0000\u029a\u029c"+
		"\u00050\u0000\u0000\u029b\u029d\u0003j5\u0000\u029c\u029b\u0001\u0000"+
		"\u0000\u0000\u029c\u029d\u0001\u0000\u0000\u0000\u029d\u029f\u0001\u0000"+
		"\u0000\u0000\u029e\u029a\u0001\u0000\u0000\u0000\u029e\u029f\u0001\u0000"+
		"\u0000\u0000\u029fk\u0001\u0000\u0000\u0000\u02a0\u02a9\u0003\u0082A\u0000"+
		"\u02a1\u02a9\u0003n7\u0000\u02a2\u02a9\u0003\u0080@\u0000\u02a3\u02a9"+
		"\u0003p8\u0000\u02a4\u02a9\u0003r9\u0000\u02a5\u02a9\u0003\u0084B\u0000"+
		"\u02a6\u02a9\u0003t:\u0000\u02a7\u02a9\u0003v;\u0000\u02a8\u02a0\u0001"+
		"\u0000\u0000\u0000\u02a8\u02a1\u0001\u0000\u0000\u0000\u02a8\u02a2\u0001"+
		"\u0000\u0000\u0000\u02a8\u02a3\u0001\u0000\u0000\u0000\u02a8\u02a4\u0001"+
		"\u0000\u0000\u0000\u02a8\u02a5\u0001\u0000\u0000\u0000\u02a8\u02a6\u0001"+
		"\u0000\u0000\u0000\u02a8\u02a7\u0001\u0000\u0000\u0000\u02a9m\u0001\u0000"+
		"\u0000\u0000\u02aa\u02ab\u00051\u0000\u0000\u02ab\u02ac\u0003f3\u0000"+
		"\u02aco\u0001\u0000\u0000\u0000\u02ad\u02ae\u0005.\u0000\u0000\u02ae\u02af"+
		"\u0003\u00d0h\u0000\u02af\u02b0\u0003f3\u0000\u02b0q\u0001\u0000\u0000"+
		"\u0000\u02b1\u02b3\u00052\u0000\u0000\u02b2\u02b4\u0005\u001d\u0000\u0000"+
		"\u02b3\u02b2\u0001\u0000\u0000\u0000\u02b3\u02b4\u0001\u0000\u0000\u0000"+
		"\u02b4\u02b5\u0001\u0000\u0000\u0000\u02b5\u02b6\u0003\u00d0h\u0000\u02b6"+
		"\u02b7\u0003f3\u0000\u02b7s\u0001\u0000\u0000\u0000\u02b8\u02b9\u0005"+
		"3\u0000\u0000\u02b9\u02ba\u0005\u0006\u0000\u0000\u02ba\u02bb\u0003\u00d6"+
		"k\u0000\u02bb\u02bc\u0005\u0007\u0000\u0000\u02bc\u02bd\u0003\u00d2i\u0000"+
		"\u02bd\u02be\u0005\b\u0000\u0000\u02beu\u0001\u0000\u0000\u0000\u02bf"+
		"\u02c0\u0005\u001a\u0000\u0000\u02c0\u02c1\u0003x<\u0000\u02c1w\u0001"+
		"\u0000\u0000\u0000\u02c2\u02c5\u0003z=\u0000\u02c3\u02c5\u0003|>\u0000"+
		"\u02c4\u02c2\u0001\u0000\u0000\u0000\u02c4\u02c3\u0001\u0000\u0000\u0000"+
		"\u02c5y\u0001\u0000\u0000\u0000\u02c6\u02c7\u0003\u00d2i\u0000\u02c7\u02cb"+
		"\u0005\f\u0000\u0000\u02c8\u02ca\u0003~?\u0000\u02c9\u02c8\u0001\u0000"+
		"\u0000\u0000\u02ca\u02cd\u0001\u0000\u0000\u0000\u02cb\u02c9\u0001\u0000"+
		"\u0000\u0000\u02cb\u02cc\u0001\u0000\u0000\u0000\u02cc\u02ce\u0001\u0000"+
		"\u0000\u0000\u02cd\u02cb\u0001\u0000\u0000\u0000\u02ce\u02cf\u0005\r\u0000"+
		"\u0000\u02cf{\u0001\u0000\u0000\u0000\u02d0\u02da\u0005\u00a6\u0000\u0000"+
		"\u02d1\u02d5\u0005\u0006\u0000\u0000\u02d2\u02d4\u0003\u00d2i\u0000\u02d3"+
		"\u02d2\u0001\u0000\u0000\u0000\u02d4\u02d7\u0001\u0000\u0000\u0000\u02d5"+
		"\u02d3\u0001\u0000\u0000\u0000\u02d5\u02d6\u0001\u0000\u0000\u0000\u02d6"+
		"\u02d8\u0001\u0000\u0000\u0000\u02d7\u02d5\u0001\u0000\u0000\u0000\u02d8"+
		"\u02da\u0005\b\u0000\u0000\u02d9\u02d0\u0001\u0000\u0000\u0000\u02d9\u02d1"+
		"\u0001\u0000\u0000\u0000\u02da\u02db\u0001\u0000\u0000\u0000\u02db\u02e7"+
		"\u0005\f\u0000\u0000\u02dc\u02e0\u0005\u0006\u0000\u0000\u02dd\u02df\u0003"+
		"~?\u0000\u02de\u02dd\u0001\u0000\u0000\u0000\u02df\u02e2\u0001\u0000\u0000"+
		"\u0000\u02e0\u02de\u0001\u0000\u0000\u0000\u02e0\u02e1\u0001\u0000\u0000"+
		"\u0000\u02e1\u02e3\u0001\u0000\u0000\u0000\u02e2\u02e0\u0001\u0000\u0000"+
		"\u0000\u02e3\u02e6\u0005\b\u0000\u0000\u02e4\u02e6\u0005\u00a6\u0000\u0000"+
		"\u02e5\u02dc\u0001\u0000\u0000\u0000\u02e5\u02e4\u0001\u0000\u0000\u0000"+
		"\u02e6\u02e9\u0001\u0000\u0000\u0000\u02e7\u02e5\u0001\u0000\u0000\u0000"+
		"\u02e7\u02e8\u0001\u0000\u0000\u0000\u02e8\u02ea\u0001\u0000\u0000\u0000"+
		"\u02e9\u02e7\u0001\u0000\u0000\u0000\u02ea\u02eb\u0005\r\u0000\u0000\u02eb"+
		"}\u0001\u0000\u0000\u0000\u02ec\u02f2\u0003\u010a\u0085\u0000\u02ed\u02f2"+
		"\u0003\u00fc~\u0000\u02ee\u02f2\u0003\u00fe\u007f\u0000\u02ef\u02f2\u0003"+
		"\u0106\u0083\u0000\u02f0\u02f2\u00054\u0000\u0000\u02f1\u02ec\u0001\u0000"+
		"\u0000\u0000\u02f1\u02ed\u0001\u0000\u0000\u0000\u02f1\u02ee\u0001\u0000"+
		"\u0000\u0000\u02f1\u02ef\u0001\u0000\u0000\u0000\u02f1\u02f0\u0001\u0000"+
		"\u0000\u0000\u02f2\u007f\u0001\u0000\u0000\u0000\u02f3\u02f4\u00055\u0000"+
		"\u0000\u02f4\u02f5\u0003f3\u0000\u02f5\u0081\u0001\u0000\u0000\u0000\u02f6"+
		"\u02fb\u0003f3\u0000\u02f7\u02f8\u00056\u0000\u0000\u02f8\u02fa\u0003"+
		"f3\u0000\u02f9\u02f7\u0001\u0000\u0000\u0000\u02fa\u02fd\u0001\u0000\u0000"+
		"\u0000\u02fb\u02f9\u0001\u0000\u0000\u0000\u02fb\u02fc\u0001\u0000\u0000"+
		"\u0000\u02fc\u0083\u0001\u0000\u0000\u0000\u02fd\u02fb\u0001\u0000\u0000"+
		"\u0000\u02fe\u02ff\u00057\u0000\u0000\u02ff\u0300\u0003\u0086C\u0000\u0300"+
		"\u0085\u0001\u0000\u0000\u0000\u0301\u0305\u0003\u00eau\u0000\u0302\u0305"+
		"\u0003\u00ecv\u0000\u0303\u0305\u0003\u0088D\u0000\u0304\u0301\u0001\u0000"+
		"\u0000\u0000\u0304\u0302\u0001\u0000\u0000\u0000\u0304\u0303\u0001\u0000"+
		"\u0000\u0000\u0305\u0087\u0001\u0000\u0000\u0000\u0306\u0307\u0003\u010a"+
		"\u0085\u0000\u0307\u0308\u0003\u008aE\u0000\u0308\u0089\u0001\u0000\u0000"+
		"\u0000\u0309\u0319\u0005\u00a6\u0000\u0000\u030a\u030c\u0005\u0006\u0000"+
		"\u0000\u030b\u030d\u0005\u0004\u0000\u0000\u030c\u030b\u0001\u0000\u0000"+
		"\u0000\u030c\u030d\u0001\u0000\u0000\u0000\u030d\u030e\u0001\u0000\u0000"+
		"\u0000\u030e\u0313\u0003\u00d6k\u0000\u030f\u0310\u00058\u0000\u0000\u0310"+
		"\u0312\u0003\u00d6k\u0000\u0311\u030f\u0001\u0000\u0000\u0000\u0312\u0315"+
		"\u0001\u0000\u0000\u0000\u0313\u0311\u0001\u0000\u0000\u0000\u0313\u0314"+
		"\u0001\u0000\u0000\u0000\u0314\u0316\u0001\u0000\u0000\u0000\u0315\u0313"+
		"\u0001\u0000\u0000\u0000\u0316\u0317\u0005\b\u0000\u0000\u0317\u0319\u0001"+
		"\u0000\u0000\u0000\u0318\u0309\u0001\u0000\u0000\u0000\u0318\u030a\u0001"+
		"\u0000\u0000\u0000\u0319\u008b\u0001\u0000\u0000\u0000\u031a\u0327\u0005"+
		"\u00a6\u0000\u0000\u031b\u031c\u0005\u0006\u0000\u0000\u031c\u0321\u0003"+
		"\u00d6k\u0000\u031d\u031e\u00058\u0000\u0000\u031e\u0320\u0003\u00d6k"+
		"\u0000\u031f\u031d\u0001\u0000\u0000\u0000\u0320\u0323\u0001\u0000\u0000"+
		"\u0000\u0321\u031f\u0001\u0000\u0000\u0000\u0321\u0322\u0001\u0000\u0000"+
		"\u0000\u0322\u0324\u0001\u0000\u0000\u0000\u0323\u0321\u0001\u0000\u0000"+
		"\u0000\u0324\u0325\u0005\b\u0000\u0000\u0325\u0327\u0001\u0000\u0000\u0000"+
		"\u0326\u031a\u0001\u0000\u0000\u0000\u0326\u031b\u0001\u0000\u0000\u0000"+
		"\u0327\u008d\u0001\u0000\u0000\u0000\u0328\u032a\u0005\f\u0000\u0000\u0329"+
		"\u032b\u0003\u0090H\u0000\u032a\u0329\u0001\u0000\u0000\u0000\u032a\u032b"+
		"\u0001\u0000\u0000\u0000\u032b\u032c\u0001\u0000\u0000\u0000\u032c\u032d"+
		"\u0005\r\u0000\u0000\u032d\u008f\u0001\u0000\u0000\u0000\u032e\u0333\u0003"+
		"\u0092I\u0000\u032f\u0331\u00050\u0000\u0000\u0330\u0332\u0003\u0090H"+
		"\u0000\u0331\u0330\u0001\u0000\u0000\u0000\u0331\u0332\u0001\u0000\u0000"+
		"\u0000\u0332\u0334\u0001\u0000\u0000\u0000\u0333\u032f\u0001\u0000\u0000"+
		"\u0000\u0333\u0334\u0001\u0000\u0000\u0000\u0334\u0091\u0001\u0000\u0000"+
		"\u0000\u0335\u0336\u0003\u00ceg\u0000\u0336\u0337\u0003\u0096K\u0000\u0337"+
		"\u033c\u0001\u0000\u0000\u0000\u0338\u0339\u0003\u00be_\u0000\u0339\u033a"+
		"\u0003\u0094J\u0000\u033a\u033c\u0001\u0000\u0000\u0000\u033b\u0335\u0001"+
		"\u0000\u0000\u0000\u033b\u0338\u0001\u0000\u0000\u0000\u033c\u0093\u0001"+
		"\u0000\u0000\u0000\u033d\u033f\u0003\u0096K\u0000\u033e\u033d\u0001\u0000"+
		"\u0000\u0000\u033e\u033f\u0001\u0000\u0000\u0000\u033f\u0095\u0001\u0000"+
		"\u0000\u0000\u0340\u0341\u0003\u0098L\u0000\u0341\u034a\u0003\u009aM\u0000"+
		"\u0342\u0346\u0005\u001b\u0000\u0000\u0343\u0344\u0003\u0098L\u0000\u0344"+
		"\u0345\u0003\u009aM\u0000\u0345\u0347\u0001\u0000\u0000\u0000\u0346\u0343"+
		"\u0001\u0000\u0000\u0000\u0346\u0347\u0001\u0000\u0000\u0000\u0347\u0349"+
		"\u0001\u0000\u0000\u0000\u0348\u0342\u0001\u0000\u0000\u0000\u0349\u034c"+
		"\u0001\u0000\u0000\u0000\u034a\u0348\u0001\u0000\u0000\u0000\u034a\u034b"+
		"\u0001\u0000\u0000\u0000\u034b\u0097\u0001\u0000\u0000\u0000\u034c\u034a"+
		"\u0001\u0000\u0000\u0000\u034d\u0351\u0003\u00d0h\u0000\u034e\u0351\u0005"+
		"9\u0000\u0000\u034f\u0351\u0005:\u0000\u0000\u0350\u034d\u0001\u0000\u0000"+
		"\u0000\u0350\u034e\u0001\u0000\u0000\u0000\u0350\u034f\u0001\u0000\u0000"+
		"\u0000\u0351\u0099\u0001\u0000\u0000\u0000\u0352\u0357\u0003\u009cN\u0000"+
		"\u0353\u0354\u00058\u0000\u0000\u0354\u0356\u0003\u009cN\u0000\u0355\u0353"+
		"\u0001\u0000\u0000\u0000\u0356\u0359\u0001\u0000\u0000\u0000\u0357\u0355"+
		"\u0001\u0000\u0000\u0000\u0357\u0358\u0001\u0000\u0000\u0000\u0358\u009b"+
		"\u0001\u0000\u0000\u0000\u0359\u0357\u0001\u0000\u0000\u0000\u035a\u035b"+
		"\u0003\u00cae\u0000\u035b\u009d\u0001\u0000\u0000\u0000\u035c\u035d\u0003"+
		"\u00ceg\u0000\u035d\u035e\u0003\u00a2Q\u0000\u035e\u0363\u0001\u0000\u0000"+
		"\u0000\u035f\u0360\u0003\u00c2a\u0000\u0360\u0361\u0003\u00a0P\u0000\u0361"+
		"\u0363\u0001\u0000\u0000\u0000\u0362\u035c\u0001\u0000\u0000\u0000\u0362"+
		"\u035f\u0001\u0000\u0000\u0000\u0363\u009f\u0001\u0000\u0000\u0000\u0364"+
		"\u0366\u0003\u00a2Q\u0000\u0365\u0364\u0001\u0000\u0000\u0000\u0365\u0366"+
		"\u0001\u0000\u0000\u0000\u0366\u00a1\u0001\u0000\u0000\u0000\u0367\u036a"+
		"\u0003\u00a4R\u0000\u0368\u036a\u0003\u00a6S\u0000\u0369\u0367\u0001\u0000"+
		"\u0000\u0000\u0369\u0368\u0001\u0000\u0000\u0000\u036a\u036b\u0001\u0000"+
		"\u0000\u0000\u036b\u0377\u0003\u00a8T\u0000\u036c\u0373\u0005\u001b\u0000"+
		"\u0000\u036d\u0370\u0003\u00a4R\u0000\u036e\u0370\u0003\u00a6S\u0000\u036f"+
		"\u036d\u0001\u0000\u0000\u0000\u036f\u036e\u0001\u0000\u0000\u0000\u0370"+
		"\u0371\u0001\u0000\u0000\u0000\u0371\u0372\u0003\u009aM\u0000\u0372\u0374"+
		"\u0001\u0000\u0000\u0000\u0373\u036f\u0001\u0000\u0000\u0000\u0373\u0374"+
		"\u0001\u0000\u0000\u0000\u0374\u0376\u0001\u0000\u0000\u0000\u0375\u036c"+
		"\u0001\u0000\u0000\u0000\u0376\u0379\u0001\u0000\u0000\u0000\u0377\u0375"+
		"\u0001\u0000\u0000\u0000\u0377\u0378\u0001\u0000\u0000\u0000\u0378\u00a3"+
		"\u0001\u0000\u0000\u0000\u0379\u0377\u0001\u0000\u0000\u0000\u037a\u037b"+
		"\u0003\u00acV\u0000\u037b\u00a5\u0001\u0000\u0000\u0000\u037c\u037d\u0003"+
		"\u00d2i\u0000\u037d\u00a7\u0001\u0000\u0000\u0000\u037e\u0383\u0003\u00aa"+
		"U\u0000\u037f\u0380\u00058\u0000\u0000\u0380\u0382\u0003\u00aaU\u0000"+
		"\u0381\u037f\u0001\u0000\u0000\u0000\u0382\u0385\u0001\u0000\u0000\u0000"+
		"\u0383\u0381\u0001\u0000\u0000\u0000\u0383\u0384\u0001\u0000\u0000\u0000"+
		"\u0384\u00a9\u0001\u0000\u0000\u0000\u0385\u0383\u0001\u0000\u0000\u0000"+
		"\u0386\u0387\u0003\u00ccf\u0000\u0387\u00ab\u0001\u0000\u0000\u0000\u0388"+
		"\u0389\u0003\u00aeW\u0000\u0389\u00ad\u0001\u0000\u0000\u0000\u038a\u038f"+
		"\u0003\u00b0X\u0000\u038b\u038c\u0005;\u0000\u0000\u038c\u038e\u0003\u00b0"+
		"X\u0000\u038d\u038b\u0001\u0000\u0000\u0000\u038e\u0391\u0001\u0000\u0000"+
		"\u0000\u038f\u038d\u0001\u0000\u0000\u0000\u038f\u0390\u0001\u0000\u0000"+
		"\u0000\u0390\u00af\u0001\u0000\u0000\u0000\u0391\u038f\u0001\u0000\u0000"+
		"\u0000\u0392\u0397\u0003\u00b4Z\u0000\u0393\u0394\u0005<\u0000\u0000\u0394"+
		"\u0396\u0003\u00b4Z\u0000\u0395\u0393\u0001\u0000\u0000\u0000\u0396\u0399"+
		"\u0001\u0000\u0000\u0000\u0397\u0395\u0001\u0000\u0000\u0000\u0397\u0398"+
		"\u0001\u0000\u0000\u0000\u0398\u00b1\u0001\u0000\u0000\u0000\u0399\u0397"+
		"\u0001\u0000\u0000\u0000\u039a\u039c\u0003\u00b8\\\u0000\u039b\u039d\u0003"+
		"\u00b6[\u0000\u039c\u039b\u0001\u0000\u0000\u0000\u039c\u039d\u0001\u0000"+
		"\u0000\u0000\u039d\u00b3\u0001\u0000\u0000\u0000\u039e\u03a2\u0003\u00b2"+
		"Y\u0000\u039f\u03a0\u0005=\u0000\u0000\u03a0\u03a2\u0003\u00b2Y\u0000"+
		"\u03a1\u039e\u0001\u0000\u0000\u0000\u03a1\u039f\u0001\u0000\u0000\u0000"+
		"\u03a2\u00b5\u0001\u0000\u0000\u0000\u03a3\u03a4\u0007\u0002\u0000\u0000"+
		"\u03a4\u00b7\u0001\u0000\u0000\u0000\u03a5\u03af\u0003\u010a\u0085\u0000"+
		"\u03a6\u03af\u00059\u0000\u0000\u03a7\u03af\u0005:\u0000\u0000\u03a8\u03a9"+
		"\u0005@\u0000\u0000\u03a9\u03af\u0003\u00ba]\u0000\u03aa\u03ab\u0005\u0006"+
		"\u0000\u0000\u03ab\u03ac\u0003\u00acV\u0000\u03ac\u03ad\u0005\b\u0000"+
		"\u0000\u03ad\u03af\u0001\u0000\u0000\u0000\u03ae\u03a5\u0001\u0000\u0000"+
		"\u0000\u03ae\u03a6\u0001\u0000\u0000\u0000\u03ae\u03a7\u0001\u0000\u0000"+
		"\u0000\u03ae\u03a8\u0001\u0000\u0000\u0000\u03ae\u03aa\u0001\u0000\u0000"+
		"\u0000\u03af\u00b9\u0001\u0000\u0000\u0000\u03b0\u03be\u0003\u00bc^\u0000"+
		"\u03b1\u03ba\u0005\u0006\u0000\u0000\u03b2\u03b7\u0003\u00bc^\u0000\u03b3"+
		"\u03b4\u0005;\u0000\u0000\u03b4\u03b6\u0003\u00bc^\u0000\u03b5\u03b3\u0001"+
		"\u0000\u0000\u0000\u03b6\u03b9\u0001\u0000\u0000\u0000\u03b7\u03b5\u0001"+
		"\u0000\u0000\u0000\u03b7\u03b8\u0001\u0000\u0000\u0000\u03b8\u03bb\u0001"+
		"\u0000\u0000\u0000\u03b9\u03b7\u0001\u0000\u0000\u0000\u03ba\u03b2\u0001"+
		"\u0000\u0000\u0000\u03ba\u03bb\u0001\u0000\u0000\u0000\u03bb\u03bc\u0001"+
		"\u0000\u0000\u0000\u03bc\u03be\u0005\b\u0000\u0000\u03bd\u03b0\u0001\u0000"+
		"\u0000\u0000\u03bd\u03b1\u0001\u0000\u0000\u0000\u03be\u00bb\u0001\u0000"+
		"\u0000\u0000\u03bf\u03c9\u0003\u010a\u0085\u0000\u03c0\u03c9\u00059\u0000"+
		"\u0000\u03c1\u03c9\u0005:\u0000\u0000\u03c2\u03c6\u0005=\u0000\u0000\u03c3"+
		"\u03c7\u0003\u010a\u0085\u0000\u03c4\u03c7\u00059\u0000\u0000\u03c5\u03c7"+
		"\u0005:\u0000\u0000\u03c6\u03c3\u0001\u0000\u0000\u0000\u03c6\u03c4\u0001"+
		"\u0000\u0000\u0000\u03c6\u03c5\u0001\u0000\u0000\u0000\u03c7\u03c9\u0001"+
		"\u0000\u0000\u0000\u03c8\u03bf\u0001\u0000\u0000\u0000\u03c8\u03c0\u0001"+
		"\u0000\u0000\u0000\u03c8\u03c1\u0001\u0000\u0000\u0000\u03c8\u03c2\u0001"+
		"\u0000\u0000\u0000\u03c9\u00bd\u0001\u0000\u0000\u0000\u03ca\u03cd\u0003"+
		"\u00c6c\u0000\u03cb\u03cd\u0003\u00c0`\u0000\u03cc\u03ca\u0001\u0000\u0000"+
		"\u0000\u03cc\u03cb\u0001\u0000\u0000\u0000\u03cd\u00bf\u0001\u0000\u0000"+
		"\u0000\u03ce\u03cf\u0005A\u0000\u0000\u03cf\u03d0\u0003\u0096K\u0000\u03d0"+
		"\u03d1\u0005B\u0000\u0000\u03d1\u00c1\u0001\u0000\u0000\u0000\u03d2\u03d5"+
		"\u0003\u00c8d\u0000\u03d3\u03d5\u0003\u00c4b\u0000\u03d4\u03d2\u0001\u0000"+
		"\u0000\u0000\u03d4\u03d3\u0001\u0000\u0000\u0000\u03d5\u00c3\u0001\u0000"+
		"\u0000\u0000\u03d6\u03d7\u0005A\u0000\u0000\u03d7\u03d8\u0003\u00a2Q\u0000"+
		"\u03d8\u03d9\u0005B\u0000\u0000\u03d9\u00c5\u0001\u0000\u0000\u0000\u03da"+
		"\u03dc\u0005\u0006\u0000\u0000\u03db\u03dd\u0003\u00cae\u0000\u03dc\u03db"+
		"\u0001\u0000\u0000\u0000\u03dd\u03de\u0001\u0000\u0000\u0000\u03de\u03dc"+
		"\u0001\u0000\u0000\u0000\u03de\u03df\u0001\u0000\u0000\u0000\u03df\u03e0"+
		"\u0001\u0000\u0000\u0000\u03e0\u03e1\u0005\b\u0000\u0000\u03e1\u00c7\u0001"+
		"\u0000\u0000\u0000\u03e2\u03e4\u0005\u0006\u0000\u0000\u03e3\u03e5\u0003"+
		"\u00ccf\u0000\u03e4\u03e3\u0001\u0000\u0000\u0000\u03e5\u03e6\u0001\u0000"+
		"\u0000\u0000\u03e6\u03e4\u0001\u0000\u0000\u0000\u03e6\u03e7\u0001\u0000"+
		"\u0000\u0000\u03e7\u03e8\u0001\u0000\u0000\u0000\u03e8\u03e9\u0005\b\u0000"+
		"\u0000\u03e9\u00c9\u0001\u0000\u0000\u0000\u03ea\u03ed\u0003\u00ceg\u0000"+
		"\u03eb\u03ed\u0003\u00be_\u0000\u03ec\u03ea\u0001\u0000\u0000\u0000\u03ec"+
		"\u03eb\u0001\u0000\u0000\u0000\u03ed\u00cb\u0001\u0000\u0000\u0000\u03ee"+
		"\u03f1\u0003\u00ceg\u0000\u03ef\u03f1\u0003\u00c2a\u0000\u03f0\u03ee\u0001"+
		"\u0000\u0000\u0000\u03f0\u03ef\u0001\u0000\u0000\u0000\u03f1\u00cd\u0001"+
		"\u0000\u0000\u0000\u03f2\u03f5\u0003\u00d2i\u0000\u03f3\u03f5\u0003\u00d4"+
		"j\u0000\u03f4\u03f2\u0001\u0000\u0000\u0000\u03f4\u03f3\u0001\u0000\u0000"+
		"\u0000\u03f5\u00cf\u0001\u0000\u0000\u0000\u03f6\u03f9\u0003\u00d2i\u0000"+
		"\u03f7\u03f9\u0003\u010a\u0085\u0000\u03f8\u03f6\u0001\u0000\u0000\u0000"+
		"\u03f8\u03f7\u0001\u0000\u0000\u0000\u03f9\u00d1\u0001\u0000\u0000\u0000"+
		"\u03fa\u03fb\u0007\u0003\u0000\u0000\u03fb\u00d3\u0001\u0000\u0000\u0000"+
		"\u03fc\u0404\u0003\u010a\u0085\u0000\u03fd\u0404\u0003\u00fc~\u0000\u03fe"+
		"\u0404\u0003\u00fe\u007f\u0000\u03ff\u0404\u0003\u0106\u0083\u0000\u0400"+
		"\u0401\u0004j\u0000\u0000\u0401\u0404\u0003\u010e\u0087\u0000\u0402\u0404"+
		"\u0005\u00a6\u0000\u0000\u0403\u03fc\u0001\u0000\u0000\u0000\u0403\u03fd"+
		"\u0001\u0000\u0000\u0000\u0403\u03fe\u0001\u0000\u0000\u0000\u0403\u03ff"+
		"\u0001\u0000\u0000\u0000\u0403\u0400\u0001\u0000\u0000\u0000\u0403\u0402"+
		"\u0001\u0000\u0000\u0000\u0404\u00d5\u0001\u0000\u0000\u0000\u0405\u0406"+
		"\u0003\u00d8l\u0000\u0406\u00d7\u0001\u0000\u0000\u0000\u0407\u040c\u0003"+
		"\u00dam\u0000\u0408\u0409\u0005C\u0000\u0000\u0409\u040b\u0003\u00dam"+
		"\u0000\u040a\u0408\u0001\u0000\u0000\u0000\u040b\u040e\u0001\u0000\u0000"+
		"\u0000\u040c\u040a\u0001\u0000\u0000\u0000\u040c\u040d\u0001\u0000\u0000"+
		"\u0000\u040d\u00d9\u0001\u0000\u0000\u0000\u040e\u040c\u0001\u0000\u0000"+
		"\u0000\u040f\u0414\u0003\u00dcn\u0000\u0410\u0411\u0005D\u0000\u0000\u0411"+
		"\u0413\u0003\u00dcn\u0000\u0412\u0410\u0001\u0000\u0000\u0000\u0413\u0416"+
		"\u0001\u0000\u0000\u0000\u0414\u0412\u0001\u0000\u0000\u0000\u0414\u0415"+
		"\u0001\u0000\u0000\u0000\u0415\u00db\u0001\u0000\u0000\u0000\u0416\u0414"+
		"\u0001\u0000\u0000\u0000\u0417\u0418\u0003\u00deo\u0000\u0418\u00dd\u0001"+
		"\u0000\u0000\u0000\u0419\u042b\u0003\u00e0p\u0000\u041a\u041b\u0005E\u0000"+
		"\u0000\u041b\u042c\u0003\u00e0p\u0000\u041c\u041d\u0005F\u0000\u0000\u041d"+
		"\u042c\u0003\u00e0p\u0000\u041e\u041f\u0005G\u0000\u0000\u041f\u042c\u0003"+
		"\u00e0p\u0000\u0420\u0421\u0005H\u0000\u0000\u0421\u042c\u0003\u00e0p"+
		"\u0000\u0422\u0423\u0005I\u0000\u0000\u0423\u042c\u0003\u00e0p\u0000\u0424"+
		"\u0425\u0005J\u0000\u0000\u0425\u042c\u0003\u00e0p\u0000\u0426\u0427\u0005"+
		"K\u0000\u0000\u0427\u042c\u0003\u008cF\u0000\u0428\u0429\u0005L\u0000"+
		"\u0000\u0429\u042a\u0005K\u0000\u0000\u042a\u042c\u0003\u008cF\u0000\u042b"+
		"\u041a\u0001\u0000\u0000\u0000\u042b\u041c\u0001\u0000\u0000\u0000\u042b"+
		"\u041e\u0001\u0000\u0000\u0000\u042b\u0420\u0001\u0000\u0000\u0000\u042b"+
		"\u0422\u0001\u0000\u0000\u0000\u042b\u0424\u0001\u0000\u0000\u0000\u042b"+
		"\u0426\u0001\u0000\u0000\u0000\u042b\u0428\u0001\u0000\u0000\u0000\u042b"+
		"\u042c\u0001\u0000\u0000\u0000\u042c\u00df\u0001\u0000\u0000\u0000\u042d"+
		"\u042e\u0003\u00e2q\u0000\u042e\u00e1\u0001\u0000\u0000\u0000\u042f\u0443"+
		"\u0003\u00e4r\u0000\u0430\u0431\u0005?\u0000\u0000\u0431\u0442\u0003\u00e4"+
		"r\u0000\u0432\u0433\u0005M\u0000\u0000\u0433\u0442\u0003\u00e4r\u0000"+
		"\u0434\u0437\u0003\u0102\u0081\u0000\u0435\u0437\u0003\u0104\u0082\u0000"+
		"\u0436\u0434\u0001\u0000\u0000\u0000\u0436\u0435\u0001\u0000\u0000\u0000"+
		"\u0437\u043e\u0001\u0000\u0000\u0000\u0438\u0439\u0005\t\u0000\u0000\u0439"+
		"\u043d\u0003\u00e6s\u0000\u043a\u043b\u0005<\u0000\u0000\u043b\u043d\u0003"+
		"\u00e6s\u0000\u043c\u0438\u0001\u0000\u0000\u0000\u043c\u043a\u0001\u0000"+
		"\u0000\u0000\u043d\u0440\u0001\u0000\u0000\u0000\u043e\u043c\u0001\u0000"+
		"\u0000\u0000\u043e\u043f\u0001\u0000\u0000\u0000\u043f\u0442\u0001\u0000"+
		"\u0000\u0000\u0440\u043e\u0001\u0000\u0000\u0000\u0441\u0430\u0001\u0000"+
		"\u0000\u0000\u0441\u0432\u0001\u0000\u0000\u0000\u0441\u0436\u0001\u0000"+
		"\u0000\u0000\u0442\u0445\u0001\u0000\u0000\u0000\u0443\u0441\u0001\u0000"+
		"\u0000\u0000\u0443\u0444\u0001\u0000\u0000\u0000\u0444\u00e3\u0001\u0000"+
		"\u0000\u0000\u0445\u0443\u0001\u0000\u0000\u0000\u0446\u044d\u0003\u00e6"+
		"s\u0000\u0447\u0448\u0005\t\u0000\u0000\u0448\u044c\u0003\u00e6s\u0000"+
		"\u0449\u044a\u0005<\u0000\u0000\u044a\u044c\u0003\u00e6s\u0000\u044b\u0447"+
		"\u0001\u0000\u0000\u0000\u044b\u0449\u0001\u0000\u0000\u0000\u044c\u044f"+
		"\u0001\u0000\u0000\u0000\u044d\u044b\u0001\u0000\u0000\u0000\u044d\u044e"+
		"\u0001\u0000\u0000\u0000\u044e\u00e5\u0001\u0000\u0000\u0000\u044f\u044d"+
		"\u0001\u0000\u0000\u0000\u0450\u0451\u0005@\u0000\u0000\u0451\u0458\u0003"+
		"\u00e8t\u0000\u0452\u0453\u0005?\u0000\u0000\u0453\u0458\u0003\u00e8t"+
		"\u0000\u0454\u0455\u0005M\u0000\u0000\u0455\u0458\u0003\u00e8t\u0000\u0456"+
		"\u0458\u0003\u00e8t\u0000\u0457\u0450\u0001\u0000\u0000\u0000\u0457\u0452"+
		"\u0001\u0000\u0000\u0000\u0457\u0454\u0001\u0000\u0000\u0000\u0457\u0456"+
		"\u0001\u0000\u0000\u0000\u0458\u00e7\u0001\u0000\u0000\u0000\u0459\u0461"+
		"\u0003\u00eau\u0000\u045a\u0461\u0003\u00ecv\u0000\u045b\u0461\u0003\u00fa"+
		"}\u0000\u045c\u0461\u0003\u00fc~\u0000\u045d\u0461\u0003\u00fe\u007f\u0000"+
		"\u045e\u0461\u0003\u0106\u0083\u0000\u045f\u0461\u0003\u00d2i\u0000\u0460"+
		"\u0459\u0001\u0000\u0000\u0000\u0460\u045a\u0001\u0000\u0000\u0000\u0460"+
		"\u045b\u0001\u0000\u0000\u0000\u0460\u045c\u0001\u0000\u0000\u0000\u0460"+
		"\u045d\u0001\u0000\u0000\u0000\u0460\u045e\u0001\u0000\u0000\u0000\u0460"+
		"\u045f\u0001\u0000\u0000\u0000\u0461\u00e9\u0001\u0000\u0000\u0000\u0462"+
		"\u0463\u0005\u0006\u0000\u0000\u0463\u0464\u0003\u00d6k\u0000\u0464\u0465"+
		"\u0005\b\u0000\u0000\u0465\u00eb\u0001\u0000\u0000\u0000\u0466\u0569\u0003"+
		"\u00f8|\u0000\u0467\u0468\u0005N\u0000\u0000\u0468\u0469\u0005\u0006\u0000"+
		"\u0000\u0469\u046a\u0003\u00d6k\u0000\u046a\u046b\u0005\b\u0000\u0000"+
		"\u046b\u0569\u0001\u0000\u0000\u0000\u046c\u046d\u0005O\u0000\u0000\u046d"+
		"\u046e\u0005\u0006\u0000\u0000\u046e\u046f\u0003\u00d6k\u0000\u046f\u0470"+
		"\u0005\b\u0000\u0000\u0470\u0569\u0001\u0000\u0000\u0000\u0471\u0472\u0005"+
		"P\u0000\u0000\u0472\u0473\u0005\u0006\u0000\u0000\u0473\u0474\u0003\u00d6"+
		"k\u0000\u0474\u0475\u00058\u0000\u0000\u0475\u0476\u0003\u00d6k\u0000"+
		"\u0476\u0477\u0005\b\u0000\u0000\u0477\u0569\u0001\u0000\u0000\u0000\u0478"+
		"\u0479\u0005Q\u0000\u0000\u0479\u047a\u0005\u0006\u0000\u0000\u047a\u047b"+
		"\u0003\u00d6k\u0000\u047b\u047c\u0005\b\u0000\u0000\u047c\u0569\u0001"+
		"\u0000\u0000\u0000\u047d\u047e\u0005R\u0000\u0000\u047e\u047f\u0005\u0006"+
		"\u0000\u0000\u047f\u0480\u0003\u00d2i\u0000\u0480\u0481\u0005\b\u0000"+
		"\u0000\u0481\u0569\u0001\u0000\u0000\u0000\u0482\u0483\u0005S\u0000\u0000"+
		"\u0483\u0484\u0005\u0006\u0000\u0000\u0484\u0485\u0003\u00d6k\u0000\u0485"+
		"\u0486\u0005\b\u0000\u0000\u0486\u0569\u0001\u0000\u0000\u0000\u0487\u0488"+
		"\u0005T\u0000\u0000\u0488\u0489\u0005\u0006\u0000\u0000\u0489\u048a\u0003"+
		"\u00d6k\u0000\u048a\u048b\u0005\b\u0000\u0000\u048b\u0569\u0001\u0000"+
		"\u0000\u0000\u048c\u0492\u0005U\u0000\u0000\u048d\u048e\u0005\u0006\u0000"+
		"\u0000\u048e\u048f\u0003\u00d6k\u0000\u048f\u0490\u0005\b\u0000\u0000"+
		"\u0490\u0493\u0001\u0000\u0000\u0000\u0491\u0493\u0005\u00a6\u0000\u0000"+
		"\u0492\u048d\u0001\u0000\u0000\u0000\u0492\u0491\u0001\u0000\u0000\u0000"+
		"\u0493\u0569\u0001\u0000\u0000\u0000\u0494\u0495\u0005V\u0000\u0000\u0495"+
		"\u0569\u0005\u00a6\u0000\u0000\u0496\u0497\u0005W\u0000\u0000\u0497\u0498"+
		"\u0005\u0006\u0000\u0000\u0498\u0499\u0003\u00d6k\u0000\u0499\u049a\u0005"+
		"\b\u0000\u0000\u049a\u0569\u0001\u0000\u0000\u0000\u049b\u049c\u0005X"+
		"\u0000\u0000\u049c\u049d\u0005\u0006\u0000\u0000\u049d\u049e\u0003\u00d6"+
		"k\u0000\u049e\u049f\u0005\b\u0000\u0000\u049f\u0569\u0001\u0000\u0000"+
		"\u0000\u04a0\u04a1\u0005Y\u0000\u0000\u04a1\u04a2\u0005\u0006\u0000\u0000"+
		"\u04a2\u04a3\u0003\u00d6k\u0000\u04a3\u04a4\u0005\b\u0000\u0000\u04a4"+
		"\u0569\u0001\u0000\u0000\u0000\u04a5\u04a6\u0005Z\u0000\u0000\u04a6\u04a7"+
		"\u0005\u0006\u0000\u0000\u04a7\u04a8\u0003\u00d6k\u0000\u04a8\u04a9\u0005"+
		"\b\u0000\u0000\u04a9\u0569\u0001\u0000\u0000\u0000\u04aa\u04ab\u0005["+
		"\u0000\u0000\u04ab\u0569\u0003\u008cF\u0000\u04ac\u0569\u0003\u00f0x\u0000"+
		"\u04ad\u04ae\u0005\\\u0000\u0000\u04ae\u04af\u0005\u0006\u0000\u0000\u04af"+
		"\u04b0\u0003\u00d6k\u0000\u04b0\u04b1\u0005\b\u0000\u0000\u04b1\u0569"+
		"\u0001\u0000\u0000\u0000\u04b2\u0569\u0003\u00f2y\u0000\u04b3\u04b4\u0005"+
		"]\u0000\u0000\u04b4\u04b5\u0005\u0006\u0000\u0000\u04b5\u04b6\u0003\u00d6"+
		"k\u0000\u04b6\u04b7\u0005\b\u0000\u0000\u04b7\u0569\u0001\u0000\u0000"+
		"\u0000\u04b8\u04b9\u0005^\u0000\u0000\u04b9\u04ba\u0005\u0006\u0000\u0000"+
		"\u04ba\u04bb\u0003\u00d6k\u0000\u04bb\u04bc\u0005\b\u0000\u0000\u04bc"+
		"\u0569\u0001\u0000\u0000\u0000\u04bd\u04be\u0005_\u0000\u0000\u04be\u04bf"+
		"\u0005\u0006\u0000\u0000\u04bf\u04c0\u0003\u00d6k\u0000\u04c0\u04c1\u0005"+
		"\b\u0000\u0000\u04c1\u0569\u0001\u0000\u0000\u0000\u04c2\u04c3\u0005`"+
		"\u0000\u0000\u04c3\u04c4\u0005\u0006\u0000\u0000\u04c4\u04c5\u0003\u00d6"+
		"k\u0000\u04c5\u04c6\u00058\u0000\u0000\u04c6\u04c7\u0003\u00d6k\u0000"+
		"\u04c7\u04c8\u0005\b\u0000\u0000\u04c8\u0569\u0001\u0000\u0000\u0000\u04c9"+
		"\u04ca\u0005a\u0000\u0000\u04ca\u04cb\u0005\u0006\u0000\u0000\u04cb\u04cc"+
		"\u0003\u00d6k\u0000\u04cc\u04cd\u00058\u0000\u0000\u04cd\u04ce\u0003\u00d6"+
		"k\u0000\u04ce\u04cf\u0005\b\u0000\u0000\u04cf\u0569\u0001\u0000\u0000"+
		"\u0000\u04d0\u04d1\u0005b\u0000\u0000\u04d1\u04d2\u0005\u0006\u0000\u0000"+
		"\u04d2\u04d3\u0003\u00d6k\u0000\u04d3\u04d4\u00058\u0000\u0000\u04d4\u04d5"+
		"\u0003\u00d6k\u0000\u04d5\u04d6\u0005\b\u0000\u0000\u04d6\u0569\u0001"+
		"\u0000\u0000\u0000\u04d7\u04d8\u0005c\u0000\u0000\u04d8\u04d9\u0005\u0006"+
		"\u0000\u0000\u04d9\u04da\u0003\u00d6k\u0000\u04da\u04db\u00058\u0000\u0000"+
		"\u04db\u04dc\u0003\u00d6k\u0000\u04dc\u04dd\u0005\b\u0000\u0000\u04dd"+
		"\u0569\u0001\u0000\u0000\u0000\u04de\u04df\u0005d\u0000\u0000\u04df\u04e0"+
		"\u0005\u0006\u0000\u0000\u04e0\u04e1\u0003\u00d6k\u0000\u04e1\u04e2\u0005"+
		"8\u0000\u0000\u04e2\u04e3\u0003\u00d6k\u0000\u04e3\u04e4\u0005\b\u0000"+
		"\u0000\u04e4\u0569\u0001\u0000\u0000\u0000\u04e5\u04e6\u0005e\u0000\u0000"+
		"\u04e6\u04e7\u0005\u0006\u0000\u0000\u04e7\u04e8\u0003\u00d6k\u0000\u04e8"+
		"\u04e9\u0005\b\u0000\u0000\u04e9\u0569\u0001\u0000\u0000\u0000\u04ea\u04eb"+
		"\u0005f\u0000\u0000\u04eb\u04ec\u0005\u0006\u0000\u0000\u04ec\u04ed\u0003"+
		"\u00d6k\u0000\u04ed\u04ee\u0005\b\u0000\u0000\u04ee\u0569\u0001\u0000"+
		"\u0000\u0000\u04ef\u04f0\u0005g\u0000\u0000\u04f0\u04f1\u0005\u0006\u0000"+
		"\u0000\u04f1\u04f2\u0003\u00d6k\u0000\u04f2\u04f3\u0005\b\u0000\u0000"+
		"\u04f3\u0569\u0001\u0000\u0000\u0000\u04f4\u04f5\u0005h\u0000\u0000\u04f5"+
		"\u04f6\u0005\u0006\u0000\u0000\u04f6\u04f7\u0003\u00d6k\u0000\u04f7\u04f8"+
		"\u0005\b\u0000\u0000\u04f8\u0569\u0001\u0000\u0000\u0000\u04f9\u04fa\u0005"+
		"i\u0000\u0000\u04fa\u04fb\u0005\u0006\u0000\u0000\u04fb\u04fc\u0003\u00d6"+
		"k\u0000\u04fc\u04fd\u0005\b\u0000\u0000\u04fd\u0569\u0001\u0000\u0000"+
		"\u0000\u04fe\u04ff\u0005j\u0000\u0000\u04ff\u0500\u0005\u0006\u0000\u0000"+
		"\u0500\u0501\u0003\u00d6k\u0000\u0501\u0502\u0005\b\u0000\u0000\u0502"+
		"\u0569\u0001\u0000\u0000\u0000\u0503\u0504\u0005k\u0000\u0000\u0504\u0505"+
		"\u0005\u0006\u0000\u0000\u0505\u0506\u0003\u00d6k\u0000\u0506\u0507\u0005"+
		"\b\u0000\u0000\u0507\u0569\u0001\u0000\u0000\u0000\u0508\u0509\u0005l"+
		"\u0000\u0000\u0509\u050a\u0005\u0006\u0000\u0000\u050a\u050b\u0003\u00d6"+
		"k\u0000\u050b\u050c\u0005\b\u0000\u0000\u050c\u0569\u0001\u0000\u0000"+
		"\u0000\u050d\u050e\u0005m\u0000\u0000\u050e\u0569\u0005\u00a6\u0000\u0000"+
		"\u050f\u0510\u0005n\u0000\u0000\u0510\u0569\u0005\u00a6\u0000\u0000\u0511"+
		"\u0512\u0005o\u0000\u0000\u0512\u0569\u0005\u00a6\u0000\u0000\u0513\u0514"+
		"\u0005p\u0000\u0000\u0514\u0515\u0005\u0006\u0000\u0000\u0515\u0516\u0003"+
		"\u00d6k\u0000\u0516\u0517\u0005\b\u0000\u0000\u0517\u0569\u0001\u0000"+
		"\u0000\u0000\u0518\u0519\u0005q\u0000\u0000\u0519\u051a\u0005\u0006\u0000"+
		"\u0000\u051a\u051b\u0003\u00d6k\u0000\u051b\u051c\u0005\b\u0000\u0000"+
		"\u051c\u0569\u0001\u0000\u0000\u0000\u051d\u051e\u0005r\u0000\u0000\u051e"+
		"\u051f\u0005\u0006\u0000\u0000\u051f\u0520\u0003\u00d6k\u0000\u0520\u0521"+
		"\u0005\b\u0000\u0000\u0521\u0569\u0001\u0000\u0000\u0000\u0522\u0523\u0005"+
		"s\u0000\u0000\u0523\u0524\u0005\u0006\u0000\u0000\u0524\u0525\u0003\u00d6"+
		"k\u0000\u0525\u0526\u0005\b\u0000\u0000\u0526\u0569\u0001\u0000\u0000"+
		"\u0000\u0527\u0528\u0005t\u0000\u0000\u0528\u0529\u0005\u0006\u0000\u0000"+
		"\u0529\u052a\u0003\u00d6k\u0000\u052a\u052b\u0005\b\u0000\u0000\u052b"+
		"\u0569\u0001\u0000\u0000\u0000\u052c\u052d\u0005u\u0000\u0000\u052d\u0569"+
		"\u0003\u008cF\u0000\u052e\u052f\u0005v\u0000\u0000\u052f\u0530\u0005\u0006"+
		"\u0000\u0000\u0530\u0531\u0003\u00d6k\u0000\u0531\u0532\u00058\u0000\u0000"+
		"\u0532\u0533\u0003\u00d6k\u0000\u0533\u0534\u00058\u0000\u0000\u0534\u0535"+
		"\u0003\u00d6k\u0000\u0535\u0536\u0005\b\u0000\u0000\u0536\u0569\u0001"+
		"\u0000\u0000\u0000\u0537\u0538\u0005w\u0000\u0000\u0538\u0539\u0005\u0006"+
		"\u0000\u0000\u0539\u053a\u0003\u00d6k\u0000\u053a\u053b\u00058\u0000\u0000"+
		"\u053b\u053c\u0003\u00d6k\u0000\u053c\u053d\u0005\b\u0000\u0000\u053d"+
		"\u0569\u0001\u0000\u0000\u0000\u053e\u053f\u0005x\u0000\u0000\u053f\u0540"+
		"\u0005\u0006\u0000\u0000\u0540\u0541\u0003\u00d6k\u0000\u0541\u0542\u0005"+
		"8\u0000\u0000\u0542\u0543\u0003\u00d6k\u0000\u0543\u0544\u0005\b\u0000"+
		"\u0000\u0544\u0569\u0001\u0000\u0000\u0000\u0545\u0546\u0005y\u0000\u0000"+
		"\u0546\u0547\u0005\u0006\u0000\u0000\u0547\u0548\u0003\u00d6k\u0000\u0548"+
		"\u0549\u00058\u0000\u0000\u0549\u054a\u0003\u00d6k\u0000\u054a\u054b\u0005"+
		"\b\u0000\u0000\u054b\u0569\u0001\u0000\u0000\u0000\u054c\u054d\u0005z"+
		"\u0000\u0000\u054d\u054e\u0005\u0006\u0000\u0000\u054e\u054f\u0003\u00d6"+
		"k\u0000\u054f\u0550\u0005\b\u0000\u0000\u0550\u0569\u0001\u0000\u0000"+
		"\u0000\u0551\u0552\u0005{\u0000\u0000\u0552\u0553\u0005\u0006\u0000\u0000"+
		"\u0553\u0554\u0003\u00d6k\u0000\u0554\u0555\u0005\b\u0000\u0000\u0555"+
		"\u0569\u0001\u0000\u0000\u0000\u0556\u0557\u0005|\u0000\u0000\u0557\u0558"+
		"\u0005\u0006\u0000\u0000\u0558\u0559\u0003\u00d6k\u0000\u0559\u055a\u0005"+
		"\b\u0000\u0000\u055a\u0569\u0001\u0000\u0000\u0000\u055b\u055c\u0005}"+
		"\u0000\u0000\u055c\u055d\u0005\u0006\u0000\u0000\u055d\u055e\u0003\u00d6"+
		"k\u0000\u055e\u055f\u0005\b\u0000\u0000\u055f\u0569\u0001\u0000\u0000"+
		"\u0000\u0560\u0561\u0005~\u0000\u0000\u0561\u0562\u0005\u0006\u0000\u0000"+
		"\u0562\u0563\u0003\u00d6k\u0000\u0563\u0564\u0005\b\u0000\u0000\u0564"+
		"\u0569\u0001\u0000\u0000\u0000\u0565\u0569\u0003\u00eew\u0000\u0566\u0569"+
		"\u0003\u00f4z\u0000\u0567\u0569\u0003\u00f6{\u0000\u0568\u0466\u0001\u0000"+
		"\u0000\u0000\u0568\u0467\u0001\u0000\u0000\u0000\u0568\u046c\u0001\u0000"+
		"\u0000\u0000\u0568\u0471\u0001\u0000\u0000\u0000\u0568\u0478\u0001\u0000"+
		"\u0000\u0000\u0568\u047d\u0001\u0000\u0000\u0000\u0568\u0482\u0001\u0000"+
		"\u0000\u0000\u0568\u0487\u0001\u0000\u0000\u0000\u0568\u048c\u0001\u0000"+
		"\u0000\u0000\u0568\u0494\u0001\u0000\u0000\u0000\u0568\u0496\u0001\u0000"+
		"\u0000\u0000\u0568\u049b\u0001\u0000\u0000\u0000\u0568\u04a0\u0001\u0000"+
		"\u0000\u0000\u0568\u04a5\u0001\u0000\u0000\u0000\u0568\u04aa\u0001\u0000"+
		"\u0000\u0000\u0568\u04ac\u0001\u0000\u0000\u0000\u0568\u04ad\u0001\u0000"+
		"\u0000\u0000\u0568\u04b2\u0001\u0000\u0000\u0000\u0568\u04b3\u0001\u0000"+
		"\u0000\u0000\u0568\u04b8\u0001\u0000\u0000\u0000\u0568\u04bd\u0001\u0000"+
		"\u0000\u0000\u0568\u04c2\u0001\u0000\u0000\u0000\u0568\u04c9\u0001\u0000"+
		"\u0000\u0000\u0568\u04d0\u0001\u0000\u0000\u0000\u0568\u04d7\u0001\u0000"+
		"\u0000\u0000\u0568\u04de\u0001\u0000\u0000\u0000\u0568\u04e5\u0001\u0000"+
		"\u0000\u0000\u0568\u04ea\u0001\u0000\u0000\u0000\u0568\u04ef\u0001\u0000"+
		"\u0000\u0000\u0568\u04f4\u0001\u0000\u0000\u0000\u0568\u04f9\u0001\u0000"+
		"\u0000\u0000\u0568\u04fe\u0001\u0000\u0000\u0000\u0568\u0503\u0001\u0000"+
		"\u0000\u0000\u0568\u0508\u0001\u0000\u0000\u0000\u0568\u050d\u0001\u0000"+
		"\u0000\u0000\u0568\u050f\u0001\u0000\u0000\u0000\u0568\u0511\u0001\u0000"+
		"\u0000\u0000\u0568\u0513\u0001\u0000\u0000\u0000\u0568\u0518\u0001\u0000"+
		"\u0000\u0000\u0568\u051d\u0001\u0000\u0000\u0000\u0568\u0522\u0001\u0000"+
		"\u0000\u0000\u0568\u0527\u0001\u0000\u0000\u0000\u0568\u052c\u0001\u0000"+
		"\u0000\u0000\u0568\u052e\u0001\u0000\u0000\u0000\u0568\u0537\u0001\u0000"+
		"\u0000\u0000\u0568\u053e\u0001\u0000\u0000\u0000\u0568\u0545\u0001\u0000"+
		"\u0000\u0000\u0568\u054c\u0001\u0000\u0000\u0000\u0568\u0551\u0001\u0000"+
		"\u0000\u0000\u0568\u0556\u0001\u0000\u0000\u0000\u0568\u055b\u0001\u0000"+
		"\u0000\u0000\u0568\u0560\u0001\u0000\u0000\u0000\u0568\u0565\u0001\u0000"+
		"\u0000\u0000\u0568\u0566\u0001\u0000\u0000\u0000\u0568\u0567\u0001\u0000"+
		"\u0000\u0000\u0569\u00ed\u0001\u0000\u0000\u0000\u056a\u056b\u0005\u007f"+
		"\u0000\u0000\u056b\u056c\u0005\u0006\u0000\u0000\u056c\u056d\u0003\u00d6"+
		"k\u0000\u056d\u056e\u00058\u0000\u0000\u056e\u0571\u0003\u00d6k\u0000"+
		"\u056f\u0570\u00058\u0000\u0000\u0570\u0572\u0003\u00d6k\u0000\u0571\u056f"+
		"\u0001\u0000\u0000\u0000\u0571\u0572\u0001\u0000\u0000\u0000\u0572\u0573"+
		"\u0001\u0000\u0000\u0000\u0573\u0574\u0005\b\u0000\u0000\u0574\u00ef\u0001"+
		"\u0000\u0000\u0000\u0575\u0576\u0005\u0080\u0000\u0000\u0576\u0577\u0005"+
		"\u0006\u0000\u0000\u0577\u0578\u0003\u00d6k\u0000\u0578\u0579\u00058\u0000"+
		"\u0000\u0579\u057c\u0003\u00d6k\u0000\u057a\u057b\u00058\u0000\u0000\u057b"+
		"\u057d\u0003\u00d6k\u0000\u057c\u057a\u0001\u0000\u0000\u0000\u057c\u057d"+
		"\u0001\u0000\u0000\u0000\u057d\u057e\u0001\u0000\u0000\u0000\u057e\u057f"+
		"\u0005\b\u0000\u0000\u057f\u00f1\u0001\u0000\u0000\u0000\u0580\u0581\u0005"+
		"\u0081\u0000\u0000\u0581\u0582\u0005\u0006\u0000\u0000\u0582\u0583\u0003"+
		"\u00d6k\u0000\u0583\u0584\u00058\u0000\u0000\u0584\u0585\u0003\u00d6k"+
		"\u0000\u0585\u0586\u00058\u0000\u0000\u0586\u0589\u0003\u00d6k\u0000\u0587"+
		"\u0588\u00058\u0000\u0000\u0588\u058a\u0003\u00d6k\u0000\u0589\u0587\u0001"+
		"\u0000\u0000\u0000\u0589\u058a\u0001\u0000\u0000\u0000\u058a\u058b\u0001"+
		"\u0000\u0000\u0000\u058b\u058c\u0005\b\u0000\u0000\u058c\u00f3\u0001\u0000"+
		"\u0000\u0000\u058d\u058e\u0005\u0082\u0000\u0000\u058e\u058f\u0003f3\u0000"+
		"\u058f\u00f5\u0001\u0000\u0000\u0000\u0590\u0591\u0005L\u0000\u0000\u0591"+
		"\u0592\u0005\u0082\u0000\u0000\u0592\u0593\u0003f3\u0000\u0593\u00f7\u0001"+
		"\u0000\u0000\u0000\u0594\u0595\u0005\u0083\u0000\u0000\u0595\u0597\u0005"+
		"\u0006\u0000\u0000\u0596\u0598\u0005\u0004\u0000\u0000\u0597\u0596\u0001"+
		"\u0000\u0000\u0000\u0597\u0598\u0001\u0000\u0000\u0000\u0598\u059b\u0001"+
		"\u0000\u0000\u0000\u0599\u059c\u0005\t\u0000\u0000\u059a\u059c\u0003\u00d6"+
		"k\u0000\u059b\u0599\u0001\u0000\u0000\u0000\u059b\u059a\u0001\u0000\u0000"+
		"\u0000\u059c\u059d\u0001\u0000\u0000\u0000\u059d\u05d5\u0005\b\u0000\u0000"+
		"\u059e\u059f\u0005\u0084\u0000\u0000\u059f\u05a1\u0005\u0006\u0000\u0000"+
		"\u05a0\u05a2\u0005\u0004\u0000\u0000\u05a1\u05a0\u0001\u0000\u0000\u0000"+
		"\u05a1\u05a2\u0001\u0000\u0000\u0000\u05a2\u05a3\u0001\u0000\u0000\u0000"+
		"\u05a3\u05a4\u0003\u00d6k\u0000\u05a4\u05a5\u0005\b\u0000\u0000\u05a5"+
		"\u05d5\u0001\u0000\u0000\u0000\u05a6\u05a7\u0005\u0085\u0000\u0000\u05a7"+
		"\u05a9\u0005\u0006\u0000\u0000\u05a8\u05aa\u0005\u0004\u0000\u0000\u05a9"+
		"\u05a8\u0001\u0000\u0000\u0000\u05a9\u05aa\u0001\u0000\u0000\u0000\u05aa"+
		"\u05ab\u0001\u0000\u0000\u0000\u05ab\u05ac\u0003\u00d6k\u0000\u05ac\u05ad"+
		"\u0005\b\u0000\u0000\u05ad\u05d5\u0001\u0000\u0000\u0000\u05ae\u05af\u0005"+
		"\u0086\u0000\u0000\u05af\u05b1\u0005\u0006\u0000\u0000\u05b0\u05b2\u0005"+
		"\u0004\u0000\u0000\u05b1\u05b0\u0001\u0000\u0000\u0000\u05b1\u05b2\u0001"+
		"\u0000\u0000\u0000\u05b2\u05b3\u0001\u0000\u0000\u0000\u05b3\u05b4\u0003"+
		"\u00d6k\u0000\u05b4\u05b5\u0005\b\u0000\u0000\u05b5\u05d5\u0001\u0000"+
		"\u0000\u0000\u05b6\u05b7\u0005\u0087\u0000\u0000\u05b7\u05b9\u0005\u0006"+
		"\u0000\u0000\u05b8\u05ba\u0005\u0004\u0000\u0000\u05b9\u05b8\u0001\u0000"+
		"\u0000\u0000\u05b9\u05ba\u0001\u0000\u0000\u0000\u05ba\u05bb\u0001\u0000"+
		"\u0000\u0000\u05bb\u05bc\u0003\u00d6k\u0000\u05bc\u05bd\u0005\b\u0000"+
		"\u0000\u05bd\u05d5\u0001\u0000\u0000\u0000\u05be\u05bf\u0005\u0088\u0000"+
		"\u0000\u05bf\u05c1\u0005\u0006\u0000\u0000\u05c0\u05c2\u0005\u0004\u0000"+
		"\u0000\u05c1\u05c0\u0001\u0000\u0000\u0000\u05c1\u05c2\u0001\u0000\u0000"+
		"\u0000\u05c2\u05c3\u0001\u0000\u0000\u0000\u05c3\u05c4\u0003\u00d6k\u0000"+
		"\u05c4\u05c5\u0005\b\u0000\u0000\u05c5\u05d5\u0001\u0000\u0000\u0000\u05c6"+
		"\u05c7\u0005\u0089\u0000\u0000\u05c7\u05c9\u0005\u0006\u0000\u0000\u05c8"+
		"\u05ca\u0005\u0004\u0000\u0000\u05c9\u05c8\u0001\u0000\u0000\u0000\u05c9"+
		"\u05ca\u0001\u0000\u0000\u0000\u05ca\u05cb\u0001\u0000\u0000\u0000\u05cb"+
		"\u05d0\u0003\u00d6k\u0000\u05cc\u05cd\u0005\u001b\u0000\u0000\u05cd\u05ce"+
		"\u0005\u008a\u0000\u0000\u05ce\u05cf\u0005E\u0000\u0000\u05cf\u05d1\u0003"+
		"\u0108\u0084\u0000\u05d0\u05cc\u0001\u0000\u0000\u0000\u05d0\u05d1\u0001"+
		"\u0000\u0000\u0000\u05d1\u05d2\u0001\u0000\u0000\u0000\u05d2\u05d3\u0005"+
		"\b\u0000\u0000\u05d3\u05d5\u0001\u0000\u0000\u0000\u05d4\u0594\u0001\u0000"+
		"\u0000\u0000\u05d4\u059e\u0001\u0000\u0000\u0000\u05d4\u05a6\u0001\u0000"+
		"\u0000\u0000\u05d4\u05ae\u0001\u0000\u0000\u0000\u05d4\u05b6\u0001\u0000"+
		"\u0000\u0000\u05d4\u05be\u0001\u0000\u0000\u0000\u05d4\u05c6\u0001\u0000"+
		"\u0000\u0000\u05d5\u00f9\u0001\u0000\u0000\u0000\u05d6\u05d8\u0003\u010a"+
		"\u0085\u0000\u05d7\u05d9\u0003\u008aE\u0000\u05d8\u05d7\u0001\u0000\u0000"+
		"\u0000\u05d8\u05d9\u0001\u0000\u0000\u0000\u05d9\u00fb\u0001\u0000\u0000"+
		"\u0000\u05da\u05de\u0003\u0108\u0084\u0000\u05db\u05df\u0005\u0096\u0000"+
		"\u0000\u05dc\u05dd\u0005\u008b\u0000\u0000\u05dd\u05df\u0003\u010a\u0085"+
		"\u0000\u05de\u05db\u0001\u0000\u0000\u0000\u05de\u05dc\u0001\u0000\u0000"+
		"\u0000\u05de\u05df\u0001\u0000\u0000\u0000\u05df\u00fd\u0001\u0000\u0000"+
		"\u0000\u05e0\u05e4\u0003\u0100\u0080\u0000\u05e1\u05e4\u0003\u0102\u0081"+
		"\u0000\u05e2\u05e4\u0003\u0104\u0082\u0000\u05e3\u05e0\u0001\u0000\u0000"+
		"\u0000\u05e3\u05e1\u0001\u0000\u0000\u0000\u05e3\u05e2\u0001\u0000\u0000"+
		"\u0000\u05e4\u00ff\u0001\u0000\u0000\u0000\u05e5\u05e6\u0007\u0004\u0000"+
		"\u0000\u05e6\u0101\u0001\u0000\u0000\u0000\u05e7\u05e8\u0007\u0005\u0000"+
		"\u0000\u05e8\u0103\u0001\u0000\u0000\u0000\u05e9\u05ea\u0007\u0006\u0000"+
		"\u0000\u05ea\u0105\u0001\u0000\u0000\u0000\u05eb\u05ec\u0007\u0007\u0000"+
		"\u0000\u05ec\u0107\u0001\u0000\u0000\u0000\u05ed\u05ee\u0007\b\u0000\u0000"+
		"\u05ee\u0109\u0001\u0000\u0000\u0000\u05ef\u05f2\u0005\u0090\u0000\u0000"+
		"\u05f0\u05f2\u0003\u010c\u0086\u0000\u05f1\u05ef\u0001\u0000\u0000\u0000"+
		"\u05f1\u05f0\u0001\u0000\u0000\u0000\u05f2\u010b\u0001\u0000\u0000\u0000"+
		"\u05f3\u05f4\u0007\t\u0000\u0000\u05f4\u010d\u0001\u0000\u0000\u0000\u05f5"+
		"\u05f6\u0007\n\u0000\u0000\u05f6\u010f\u0001\u0000\u0000\u0000\u0096\u0112"+
		"\u0119\u011f\u0121\u012f\u013c\u0145\u0147\u014a\u0151\u015a\u0160\u0164"+
		"\u016a\u016d\u0172\u0176\u017e\u0187\u0191\u0196\u0199\u019c\u019f\u01a6"+
		"\u01ae\u01b3\u01b9\u01c2\u01c8\u01ca\u01ce\u01d2\u01d4\u01de\u01e4\u01e6"+
		"\u01f3\u01f7\u01fc\u0200\u0206\u020c\u0212\u021a\u0222\u0233\u0237\u023a"+
		"\u023f\u024f\u0253\u0256\u025f\u026a\u026e\u0271\u0275\u027f\u0281\u0286"+
		"\u028b\u028f\u0292\u0296\u029c\u029e\u02a8\u02b3\u02c4\u02cb\u02d5\u02d9"+
		"\u02e0\u02e5\u02e7\u02f1\u02fb\u0304\u030c\u0313\u0318\u0321\u0326\u032a"+
		"\u0331\u0333\u033b\u033e\u0346\u034a\u0350\u0357\u0362\u0365\u0369\u036f"+
		"\u0373\u0377\u0383\u038f\u0397\u039c\u03a1\u03ae\u03b7\u03ba\u03bd\u03c6"+
		"\u03c8\u03cc\u03d4\u03de\u03e6\u03ec\u03f0\u03f4\u03f8\u0403\u040c\u0414"+
		"\u042b\u0436\u043c\u043e\u0441\u0443\u044b\u044d\u0457\u0460\u0492\u0568"+
		"\u0571\u057c\u0589\u0597\u059b\u05a1\u05a9\u05b1\u05b9\u05c1\u05c9\u05d0"+
		"\u05d4\u05d8\u05de\u05e3\u05f1";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}