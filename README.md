- SPARQL grammar was retrieved from https://github.com/tonicebrian/sparql-parser/tree/master/src/main/antlr4 and adapted (cf https://gitlab.com/odameron/sparqlGrammar)


Resources:
- https://faun.pub/introduction-to-antlr-python-af8a3c603d23
- https://jayconrod.com/posts/37/a-simple-interpreter-from-scratch-in-python-part-1


Similar works:
- https://semsys.ifs.tuwien.ac.at/visualizing-sparql-graph-pattern/
- https://gist.github.com/pebbie/5aafdbb14921cadbad53011ef5f61165
    - reviews mention http://aidanhogan.com/docs/rdf_explorer_demo.pdf


Todo:
- re-engineer the visitor class
- create a class for node elements
- support for nested graphs 
    - requires a stack of graphs?
    - handle graph-specific duplicates of nodes
        - additional attributes?
        - in the class for node elements?
- support for OPTIONAL
    - create a subgraph
