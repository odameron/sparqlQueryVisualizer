grammar Test;

@parser::members {public static boolean allowsBlankNodes = true;}

query: selectClause whereClause;

selectClause: 'SELECT' ((var)+ | '*');

var: '?' VARNAME;

whereClause: 'WHERE'? groupGraphPattern;

groupGraphPattern: triple ( '.' triple? )*;

triple: VARNAME VARNAME VARNAME;

VARNAME: PN_CHARS_BASE+;

PN_CHARS_BASE
  :  [A-Z]
  | [a-z]
;


