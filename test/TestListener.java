// Generated from Test.g4 by ANTLR 4.10.1
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link TestParser}.
 */
public interface TestListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link TestParser#query}.
	 * @param ctx the parse tree
	 */
	void enterQuery(TestParser.QueryContext ctx);
	/**
	 * Exit a parse tree produced by {@link TestParser#query}.
	 * @param ctx the parse tree
	 */
	void exitQuery(TestParser.QueryContext ctx);
	/**
	 * Enter a parse tree produced by {@link TestParser#selectClause}.
	 * @param ctx the parse tree
	 */
	void enterSelectClause(TestParser.SelectClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link TestParser#selectClause}.
	 * @param ctx the parse tree
	 */
	void exitSelectClause(TestParser.SelectClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link TestParser#var}.
	 * @param ctx the parse tree
	 */
	void enterVar(TestParser.VarContext ctx);
	/**
	 * Exit a parse tree produced by {@link TestParser#var}.
	 * @param ctx the parse tree
	 */
	void exitVar(TestParser.VarContext ctx);
	/**
	 * Enter a parse tree produced by {@link TestParser#whereClause}.
	 * @param ctx the parse tree
	 */
	void enterWhereClause(TestParser.WhereClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link TestParser#whereClause}.
	 * @param ctx the parse tree
	 */
	void exitWhereClause(TestParser.WhereClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link TestParser#groupGraphPattern}.
	 * @param ctx the parse tree
	 */
	void enterGroupGraphPattern(TestParser.GroupGraphPatternContext ctx);
	/**
	 * Exit a parse tree produced by {@link TestParser#groupGraphPattern}.
	 * @param ctx the parse tree
	 */
	void exitGroupGraphPattern(TestParser.GroupGraphPatternContext ctx);
	/**
	 * Enter a parse tree produced by {@link TestParser#triple}.
	 * @param ctx the parse tree
	 */
	void enterTriple(TestParser.TripleContext ctx);
	/**
	 * Exit a parse tree produced by {@link TestParser#triple}.
	 * @param ctx the parse tree
	 */
	void exitTriple(TestParser.TripleContext ctx);
}